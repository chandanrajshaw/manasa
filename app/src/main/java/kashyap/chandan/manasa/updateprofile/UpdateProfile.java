package kashyap.chandan.manasa.updateprofile;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.ApiError;
import kashyap.chandan.manasa.MainActivity;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.RealPathUtil;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.driver.DriverManagement;
import kashyap.chandan.manasa.login.Login;
import kashyap.chandan.manasa.vehicle.MyImageAdapter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class UpdateProfile extends AppCompatActivity {
EditText etName,etPhone,etEmail;
CircleImageView profileImage;
TextView updateProfile,toolsave,toolHeader;
SharedPreferences sharedPreferences;
    Dialog cameradialog;
    private String picturePath;
    Bitmap converetdImage;
    File image=null;
    ImageView goBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        init();
        toolHeader.setText("Update Profile");
        toolsave.setVisibility(View.GONE);
        Picasso.get().load(ApiClient.IMAGEURL+sharedPreferences.getString("image",null)).placeholder(R.drawable.loading)
                .error(R.drawable.terms).into(profileImage);
        etName.setText(sharedPreferences.getString("name",null));
        etPhone.setText(sharedPreferences.getString("mobile",null));
        etEmail.setText(sharedPreferences.getString("userName",null));
        profileImage.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(UpdateProfile.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
                        ContextCompat.checkSelfPermission(UpdateProfile.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                        ||ContextCompat.checkSelfPermission(UpdateProfile.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {

                    askPermission();
                }
                else {

                    ImageView camera, folder;
                    cameradialog = new Dialog(UpdateProfile.this);
                    cameradialog.setContentView(R.layout.dialogboxcamera);
                    DisplayMetrics metrics=getResources().getDisplayMetrics();
                    int width=metrics.widthPixels;
                    cameradialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                    cameradialog.show();
                    cameradialog.setCancelable(true);
                    cameradialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Window window = cameradialog.getWindow();
                    window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                    camera = cameradialog.findViewById(R.id.camera);
                    folder = cameradialog.findViewById(R.id.gallery);
                    folder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, 100);
                            cameradialog.dismiss();
                        }
                    });
                    camera.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, 101);
                            cameradialog.dismiss();
                        }
                    });
                }
            }
        });
        updateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               String name=etName.getText().toString();
               String useremail=etEmail.getText().toString();
               String phone=etPhone.getText().toString();
               if (name.isEmpty()&&phone.isEmpty()&&phone.length()!=10)
                   Toast.makeText(UpdateProfile.this, "Enter all Fields", Toast.LENGTH_SHORT).show();
               else if (name.isEmpty())
                   Toast.makeText(UpdateProfile.this, "Enter Name", Toast.LENGTH_SHORT).show();
               else if (phone.isEmpty()||phone.length()!=10)
                   Toast.makeText(UpdateProfile.this, "Enter Valid Phone Number", Toast.LENGTH_SHORT).show();
               else
               {
                   RequestBody dname = RequestBody.create(MediaType.parse("multipart/form-data"),name);
                   RequestBody demail = RequestBody.create(MediaType.parse("multipart/form-data"),useremail);
                   RequestBody dphone = RequestBody.create(MediaType.parse("multipart/form-data"),phone);
                   MultipartBody.Part profileImg=null;
                   if (image!=null)
                   {
                       RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                       profileImg = MultipartBody.Part.createFormData("image", image.getName(), requestFile);
                   }
                   else {
                       RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                       profileImg = MultipartBody.Part.createFormData("image", "", requestFile);
                   }
                   final Dialog progressDialog=new Dialog(UpdateProfile.this);
                   progressDialog.setContentView(R.layout.loadingdialog);
                   progressDialog.setCancelable(false);
                   progressDialog.show();
                   UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                   Call<UpdateProfileResponse>call=userInterface.updateProfile(dname,demail,dphone,profileImg);
                   call.enqueue(new Callback<UpdateProfileResponse>() {
                       @Override
                       public void onResponse(Call<UpdateProfileResponse> call, Response<UpdateProfileResponse> response) {
                           if (response.code()==200)
                           {
                               UpdateProfileResponse.AdminDetailBean adminDetail=response.body().getAdmin_detail();
                               progressDialog.dismiss();
                               Toast.makeText(UpdateProfile.this, "Profile Updated", Toast.LENGTH_SHORT).show();
                               SharedPreferences.Editor editor=sharedPreferences.edit();
                               editor.putString("userName",adminDetail.getEmail());
                               editor.putString("name",adminDetail.getUsername());
                               editor.putString("mobile",adminDetail.getMobile());
                               editor.putString("image",adminDetail.getImage());
                               editor.commit();
                               Intent intent=new Intent(UpdateProfile.this, MainActivity.class);
                               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                               startActivity(intent);
                               finish();
                           }
                           else
                               {
                                   progressDialog.dismiss();
                                   Converter<ResponseBody, ApiError> converter =
                                           ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                   ApiError error;
                                   try {
                                       error = converter.convert(response.errorBody());
                                       ApiError.StatusBean status=error.getStatus();
                                       Toast.makeText(UpdateProfile.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                   } catch (IOException e) { e.printStackTrace(); }
                               }
                       }

                       @Override
                       public void onFailure(Call<UpdateProfileResponse> call, Throwable t) {
                           progressDialog.dismiss();
                           Toast.makeText(UpdateProfile.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                       }
                   });

               }
            }
        });
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
    }
    private void init()
    {
        goBack=findViewById(R.id.toolgoback);
        toolHeader=findViewById(R.id.toolgenheader);
        toolsave=findViewById(R.id.toolsubmit);
        profileImage=findViewById(R.id.profileImage);
        etEmail=findViewById(R.id.etEmail);
        etName=findViewById(R.id.etName);
        etPhone=findViewById(R.id.etPhone);
        updateProfile=findViewById(R.id.updateProfile);
        sharedPreferences=getSharedPreferences("Login",MODE_PRIVATE);
    }
    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (resultCode == RESULT_OK) {

            if (requestCode == 100  && resultData != null) {

//the image URI
                Uri selectedImage = resultData.getData();

                //     imagepath=selectedImage.getPath();

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();


                if (picturePath != null && !picturePath.equals("")) {
                    image = new File(picturePath);
                }

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    converetdImage = getResizedBitmap(bitmap, 500);
                    profileImage.setImageBitmap(converetdImage);
                    profileImage.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else if (requestCode == 101 ) {
                Bitmap converetdImage = (Bitmap) resultData.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                profileImage.setImageBitmap(converetdImage);
                profileImage.setVisibility(View.VISIBLE);
                image = new File(Environment.getExternalStorageDirectory(), "AdminProfileImage.jpg");
                FileOutputStream fo;
                try {
                    fo = new FileOutputStream(image);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void askPermission() {
        if (ContextCompat.checkSelfPermission(UpdateProfile.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(UpdateProfile.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                ||ContextCompat.checkSelfPermission(UpdateProfile.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},100);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int length=0;
        switch (requestCode)
        {
            case 100:
                 length= grantResults.length;
                if (grantResults.length>0)
                {
                    for (int i=0;i<length;i++)
                    {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            // Granted. Start getting the location information
                        }
                        else if(grantResults[i]==PackageManager.PERMISSION_DENIED) {
                            {
                                final AlertDialog.Builder builder=new AlertDialog.Builder(UpdateProfile.this,R.style.AlertDialogTheme);
                                builder.setTitle("Notice");
                                builder.setMessage("You Had Denied the Necessary Permissions.Please Go to app Setting and give All the permissions");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent=new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                        intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                                        startActivity(intent);
                                    }
                                });
                                builder.show();
                                break;
                            }

                        }

                    }

                }
                break;
        }
    }
}
