package kashyap.chandan.manasa.branch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.adapters.BranchAdapter;
import kashyap.chandan.manasa.valuespart.DropDownBranchResponse;
import kashyap.chandan.manasa.vehicle.VehicleManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BranchManagement extends AppCompatActivity {
TextView tvtoolheader,submit,tvBranch;
ImageView goBack;
String branchcode="";
RelativeLayout branchlay;
RecyclerView branchRecycler;
TextInputEditText etBranchAdmin,etBranchPhone,etBranchEmail,etBranchAddress,etBranchPassword,etConfirmPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_management);
        init();
        tvtoolheader.setText("Add Branch");
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        branchlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final  Dialog progress=new Dialog(BranchManagement.this);
                progress.setContentView(R.layout.loadingdialog);
                progress.setCancelable(false);
                progress.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<DropDownBranchResponse> call=userInterface.dropDownBranchList();
                call.enqueue(new Callback<DropDownBranchResponse>() {
                    @Override
                    public void onResponse(Call<DropDownBranchResponse> call, Response<DropDownBranchResponse> response) {
                        if (response.code()==200)
                        {
                            progress.dismiss();
                            final Dialog branchDialog=new Dialog(BranchManagement.this);
                            branchDialog.setContentView(R.layout.recyclerdialog);
                            branchDialog.setCancelable(true);
                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                            int width = metrics.widthPixels;
                            branchRecycler=branchDialog.findViewById(R.id.recycleroption);
                            branchDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            List<DropDownBranchResponse.DataBean> branch=new ArrayList<>();
                            branch=response.body().getData();
                            branchRecycler.setLayoutManager(new LinearLayoutManager(BranchManagement.this,LinearLayoutManager.VERTICAL,false));
                            branchRecycler.setAdapter(new BranchAdapter(BranchManagement.this,branch,branchDialog,new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, int position, String value) {
                                    tvBranch.setText(value);
                                    branchcode=String.valueOf(position);
                                }
                            }));
                            branchDialog.show();
                        }
                        else
                        {
                            progress.dismiss();
                            Snackbar.make(BranchManagement.this.getWindow().getDecorView().findViewById(android.R.id.content),"Branch Not available",Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DropDownBranchResponse> call, Throwable t) {
                        progress.dismiss();
                        Toast.makeText(BranchManagement.this, ""+t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String branchName=branchcode;
                String branchAdmin=etBranchAdmin.getText().toString();
                String branchPhone=etBranchPhone.getText().toString();
                String branchEmail=etBranchEmail.getText().toString();
                String branchAddress=etBranchAddress.getText().toString();
                String branchPassword=etBranchPassword.getText().toString().trim();
                String confirmPassword=etConfirmPassword.getText().toString().trim();
                if (branchName.equalsIgnoreCase("Branch Name")&&branchAdmin.isEmpty()&&branchPhone.isEmpty()&&branchPhone.length()!=10&&branchEmail.isEmpty()
                &&branchAddress.isEmpty()&&branchPassword.isEmpty()&&confirmPassword.isEmpty())
                    Toast.makeText(BranchManagement.this, "Please Fill All the Fields", Toast.LENGTH_SHORT).show();
                else if (branchName.equalsIgnoreCase(""))
                    Toast.makeText(BranchManagement.this, "Choose Branch Name", Toast.LENGTH_SHORT).show();
                else if (branchAdmin.isEmpty())
                    Toast.makeText(BranchManagement.this, "Enter Admin Name", Toast.LENGTH_SHORT).show();
                else if (branchPhone.isEmpty()||branchPhone.length()!=10)
                    Toast.makeText(BranchManagement.this, "Enter Valid Phone Number", Toast.LENGTH_SHORT).show();
                else if (branchEmail.isEmpty())
                    Toast.makeText(BranchManagement.this, "Enter Email", Toast.LENGTH_SHORT).show();
                else if (branchAddress.isEmpty())
                    Toast.makeText(BranchManagement.this, "Enter Address", Toast.LENGTH_SHORT).show();
                else if (branchPassword.isEmpty())
                    Toast.makeText(BranchManagement.this, "Enter Password", Toast.LENGTH_SHORT).show();
                else if (confirmPassword.isEmpty())
                    Toast.makeText(BranchManagement.this, "Confirm Password", Toast.LENGTH_SHORT).show();
                else if (!branchPassword.equalsIgnoreCase(confirmPassword))
                    Toast.makeText(BranchManagement.this, "Password mismatch in Confirm Password Field", Toast.LENGTH_SHORT).show();
           else {
                    final Dialog progressDialog=new Dialog(BranchManagement.this);
                    progressDialog.setContentView(R.layout.loadingdialog);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
Call<AddBranchResponse>call=userInterface.addBranch(branchName,branchAdmin,branchPhone,branchEmail,branchAddress,branchPassword,confirmPassword);
call.enqueue(new Callback<AddBranchResponse>() {
    @Override
    public void onResponse(Call<AddBranchResponse> call, Response<AddBranchResponse> response) {
        if (response.code()==200)
        {
            progressDialog.dismiss();
            overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
            Toast.makeText(BranchManagement.this, "Branch Added", Toast.LENGTH_SHORT).show();
            finish();
        }
        else {
            progressDialog.dismiss();
            Toast.makeText(BranchManagement.this, "Branch Not added! Try Again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Call<AddBranchResponse> call, Throwable t) {
        progressDialog.dismiss();
        Toast.makeText(BranchManagement.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
    }
});
                }
            }
        });
    }
    void init()
    {
        tvBranch=findViewById(R.id.branch);
      tvtoolheader=findViewById(R.id.toolgenheader);
      goBack=findViewById(R.id.toolgoback);
      submit=findViewById(R.id.toolsubmit);
        branchlay=findViewById(R.id.branchlay);
        etBranchAdmin=findViewById(R.id.etBranchAdmin);
        etBranchPhone=findViewById(R.id.etBranchPhone);
        etBranchEmail=findViewById(R.id.etBranchEmail);
        etBranchAddress=findViewById(R.id.etBranchAddress);
        etBranchPassword=findViewById(R.id.etBranchPassword);
        etConfirmPassword=findViewById(R.id.etConfirmPassword);
    }
}
