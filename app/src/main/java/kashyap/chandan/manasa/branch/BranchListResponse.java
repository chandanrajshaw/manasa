package kashyap.chandan.manasa.branch;

import java.io.Serializable;
import java.util.List;

public class BranchListResponse implements Serializable {

    /**
     * status : {"code":200,"message":"Branch list"}
     * data : [{"id":"1","branch_id":"2","branch_aname":"vishal","phone_no":"9089567847","email":"vishal@gmail.com","address":"<p><strong>ranchi jharkhnad<\/strong><\/p>\r\n","branches_name":"Hydrabad"},{"id":"2","branch_id":"2","branch_aname":"vjhvghfh","phone_no":"444","email":"vishal.sharma822@gmail.com","address":"<h2 style=\"font-style:italic;\"><em><u><strong>sdsdsd<\/strong><\/u><\/em><\/h2>\r\n","branches_name":"Hydrabad"},{"id":"3","branch_id":"1","branch_aname":"admin 3","phone_no":"00000000","email":"admin@3","address":"sindur","branches_name":"New-delhi"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : Branch list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * branch_id : 2
         * branch_aname : vishal
         * phone_no : 9089567847
         * email : vishal@gmail.com
         * address : <p><strong>ranchi jharkhnad</strong></p>
         * branches_name : Hydrabad
         */

        private String id;
        private String branch_id;
        private String branch_aname;
        private String phone_no;
        private String email;
        private String address;
        private String branches_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBranch_id() {
            return branch_id;
        }

        public void setBranch_id(String branch_id) {
            this.branch_id = branch_id;
        }

        public String getBranch_aname() {
            return branch_aname;
        }

        public void setBranch_aname(String branch_aname) {
            this.branch_aname = branch_aname;
        }

        public String getPhone_no() {
            return phone_no;
        }

        public void setPhone_no(String phone_no) {
            this.phone_no = phone_no;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getBranches_name() {
            return branches_name;
        }

        public void setBranches_name(String branches_name) {
            this.branches_name = branches_name;
        }
    }
}
