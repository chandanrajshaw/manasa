package kashyap.chandan.manasa.branch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BranchList extends AppCompatActivity {
    RecyclerView branchlist;
    TextView toolHeader,submit;
    ImageView goBack;
    FloatingActionButton addBranch;
    List<BranchListResponse.DataBean> branches=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_branch_list);

        init();
        final Dialog progressDialog=new Dialog(BranchList.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        submit.setVisibility(View.GONE);
        toolHeader.setText("Branch");
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        addBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(BranchList.this, BranchManagement.class);
                startActivity(intent);
            }
        });
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<BranchListResponse> call=userInterface.branchList();
        call.enqueue(new Callback<BranchListResponse>() {
            @Override
            public void onResponse(Call<BranchListResponse> call, Response<BranchListResponse> response) {
                if (response.code()==200)
                {
                    branches=response.body().getData();
                    branchlist.setLayoutManager(new LinearLayoutManager(BranchList.this,LinearLayoutManager.VERTICAL,false));
                    branchlist.setAdapter(new BranchListAdapter(BranchList.this,branches));
                    progressDialog.dismiss();
                }
                else
                {
                    progressDialog.dismiss();
                    Snackbar.make(BranchList.this.getWindow().getDecorView().findViewById(android.R.id.content),"Driver List Not available",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<BranchListResponse> call, Throwable t) {

            }
        });
    }
    void init()
    {
        branchlist=findViewById(R.id.branchList);
        goBack=findViewById(R.id.toolgoback);
        addBranch=findViewById(R.id.addBranch);
        toolHeader=findViewById(R.id.toolgenheader);
        submit=findViewById(R.id.toolsubmit);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Dialog progressDialog=new Dialog(BranchList.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        submit.setVisibility(View.GONE);
        toolHeader.setText("Branch");
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        addBranch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(BranchList.this, BranchManagement.class);
                startActivity(intent);
            }
        });
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<BranchListResponse> call=userInterface.branchList();
        call.enqueue(new Callback<BranchListResponse>() {
            @Override
            public void onResponse(Call<BranchListResponse> call, Response<BranchListResponse> response) {
                if (response.code()==200)
                {
                    branches=response.body().getData();
                    branchlist.setLayoutManager(new LinearLayoutManager(BranchList.this,LinearLayoutManager.VERTICAL,false));
                    branchlist.setAdapter(new BranchListAdapter(BranchList.this,branches));
                    progressDialog.dismiss();
                }
                else
                {
                    progressDialog.dismiss();
                    Snackbar.make(BranchList.this.getWindow().getDecorView().findViewById(android.R.id.content),"Driver List Not available",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<BranchListResponse> call, Throwable t) {

            }
        });
    }
}
