package kashyap.chandan.manasa.branch;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.manasa.R;

class BranchListAdapter extends RecyclerView.Adapter<BranchListAdapter.MyViewHolder> {
    Context context;
    List<BranchListResponse.DataBean> branches;
    public BranchListAdapter(Context context, List<BranchListResponse.DataBean> branches) {
        this.context=context;
        this.branches=branches;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.branchlist,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
holder.branchname.setText(branches.get(position).getBranches_name());
holder.admin.setText(branches.get(position).getBranch_aname());
holder.phone.setText(branches.get(position).getPhone_no());
holder.mail.setText(branches.get(position).getEmail());
holder.address.setText(branches.get(position).getAddress());
holder.makeCall.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        String mob=branches.get(pos).getPhone_no();
        Uri u = Uri.fromParts("tel",mob,null);
        Intent i = new Intent(Intent.ACTION_DIAL, u);
        context.startActivity(i);
    }
});
    }

    @Override
    public int getItemCount() {
        return branches.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView makeCall;
        TextView branchname,admin,phone,mail,address;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            branchname=itemView.findViewById(R.id.branchname);
            admin=itemView.findViewById(R.id.admin);
            phone=itemView.findViewById(R.id.phone);
            mail=itemView.findViewById(R.id.mail);
            address=itemView.findViewById(R.id.account_No);
            makeCall=itemView.findViewById(R.id.makeCall);
        }
    }
}
