package kashyap.chandan.manasa.vehicle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.StatusAdapter;
import kashyap.chandan.manasa.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditVehicle extends AppCompatActivity {
ArrayList<String>status=new ArrayList<>();
RelativeLayout statuslay;
Dialog statusDialog;
TextView tvstatus,toolgenheader,vehicleBranch,vehicleType, vehicleNo,vehicleCode,vehicleCompany,date,vehicleCapacity,tvEdit;
RecyclerView recyclerstatus;
ImageView goBack;
VehicleListResponse.DataBean vehicle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_edit_vehicle);
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("bundle");
        vehicle= (VehicleListResponse.DataBean) bundle.getSerializable("vehicle");
        init();
        status.add("Inactive");status.add("Active");
        toolgenheader.setText("Vehicle Detail");
        vehicleNo.setText(vehicle.getVehicle_number());
        vehicleType.setText(vehicle.getVehicletype_name());
        vehicleCapacity.setText(vehicle.getCapacity());
        vehicleCode.setText(vehicle.getVehicle_code());
vehicleBranch.setText(vehicle.getBranches_name());
date.setText(vehicle.getPurchased_date());
vehicleCompany.setText(vehicle.getVehiclecompany_name());
int vehiclestatus= Integer.parseInt(vehicle.getStatus());
if (vehiclestatus==1)tvstatus.setText("Active");
else tvstatus.setText("Inactive");
        statuslay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statusDialog.setContentView(R.layout.recyclerdialog);
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                int width = metrics.widthPixels;
                statusDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                recyclerstatus = statusDialog.findViewById(R.id.recycleroption);
                recyclerstatus.setLayoutManager(new LinearLayoutManager(EditVehicle.this, LinearLayoutManager.VERTICAL, false));
                recyclerstatus.setAdapter(new StatusAdapter(EditVehicle.this,status, statusDialog,new CustomItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position, String value) {
tvstatus.setText(value);
                    }
                }));
                statusDialog.show();
            }
        });
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String status=tvstatus.getText().toString().equalsIgnoreCase("Active")?"1":"0";
                String id=vehicle.getId();
                if (tvstatus.getText().toString().equalsIgnoreCase("Status"))
                    Toast.makeText(EditVehicle.this, "Select Status", Toast.LENGTH_SHORT).show();
                else {
                    final Dialog progressDialog=new Dialog(EditVehicle.this);
                    progressDialog.setContentView(R.layout.loadingdialog);
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<EditVehicleResponse>call=userInterface.editVehicle(id,status);
                    call.enqueue(new Callback<EditVehicleResponse>() {
                        @Override
                        public void onResponse(Call<EditVehicleResponse> call, Response<EditVehicleResponse> response) {
                            if (response.code()==200)
                            {
                            progressDialog.dismiss();
                                Toast.makeText(EditVehicle.this, "Update SuccessFul", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                            else {
                                progressDialog.dismiss();
                                Toast.makeText(EditVehicle.this, " Update UnSuccessFull!", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<EditVehicleResponse> call, Throwable t) {

                        }
                    });
                }

            }
        });
    }
    public void init()
    {
        tvEdit=findViewById(R.id.toolsubmit);
        vehicleBranch=findViewById(R.id.vehicleBranch);
        vehicleType=findViewById(R.id.vehicleType);
        vehicleNo =findViewById(R.id.vehicleNo);
                vehicleCode=findViewById(R.id.vehicleCode);
                vehicleCompany=findViewById(R.id.vehicleCompany);
                date=findViewById(R.id.date);
                vehicleCapacity=findViewById(R.id.vehicleCapacity);
        statuslay=findViewById(R.id.statuslay);
        statusDialog=new Dialog(EditVehicle.this);
        tvstatus=findViewById(R.id.status);
        goBack=findViewById(R.id.toolgoback);
        toolgenheader=findViewById(R.id.toolgenheader);

    }
}
