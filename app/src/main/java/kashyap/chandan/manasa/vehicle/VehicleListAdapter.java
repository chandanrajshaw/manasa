package kashyap.chandan.manasa.vehicle;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.manasa.R;

class VehicleListAdapter extends RecyclerView.Adapter<VehicleListAdapter.MyViewHolder> {
    Context context;
    List<VehicleListResponse.DataBean> vehicles;
    public VehicleListAdapter(Context context, List<VehicleListResponse.DataBean> vehicles) {
        this.context=context;
        this.vehicles=vehicles;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.vehiclelist,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
holder.detail.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(context,EditVehicle.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("vehicle",vehicles.get(position));
        intent.putExtra("bundle",bundle);
        context.startActivity(intent);
    }
});
holder.branch.setText(vehicles.get(position).getBranches_name());
holder.date.setText(vehicles.get(position).getPurchased_date());
holder.vehType.setText(vehicles.get(position).getVehicletype_name());
holder.vehName.setText(vehicles.get(position).getVehicle_number());
    }

    @Override
    public int getItemCount() {
        return vehicles.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout detail;
        TextView vehName,vehType,date,branch;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            vehName=itemView.findViewById(R.id.vehiclename);
                    vehType=itemView.findViewById(R.id.vehType);
                    date=itemView.findViewById(R.id.date);
                    branch=itemView.findViewById(R.id.branch);
                    detail=itemView.findViewById(R.id.detail);
        }
    }
}
