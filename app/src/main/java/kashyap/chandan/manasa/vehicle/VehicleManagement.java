package kashyap.chandan.manasa.vehicle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.ConnectionDetector;
import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.RealPathUtil;
import kashyap.chandan.manasa.StatusAdapter;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.adapters.BranchAdapter;
import kashyap.chandan.manasa.adapters.VehicleCompanyAdapter;
import kashyap.chandan.manasa.adapters.VehicleTypeAdapter;
import kashyap.chandan.manasa.valuespart.DropDownBranchResponse;
import kashyap.chandan.manasa.valuespart.DropDownVehicleCompanyResponse;
import kashyap.chandan.manasa.valuespart.DropDownVehicleTypeResponse;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleManagement extends AppCompatActivity {
    MultipartBody.Part pdfPart;
    ImageView goBack;
RadioButton pdf,image;
RecyclerView imageRecycler,vehicleTypeRecycler,branchRecycler;
RelativeLayout datelayout,statuslay,vehtypelay,vehCompanyLay,branchlay;
RadioGroup filegrp;
LinearLayout pdffile,imagefile;
TextView choosePdf,chooseImage,pdfpath,date,toolgenheader,toolsubmit, tvVehicleType,tvVehicleCompany,tvBranch;
    ArrayList<Uri>arrayList=new ArrayList<>();
    ArrayList<String>status=new ArrayList<>();
    private int REQUEST_CODE_READ_STORAGE=101;
    DatePickerDialog.OnDateSetListener dateDialog;
    Dialog statusDialog;
    TextView tvstatus;
    RecyclerView recyclerstatus;
    Calendar myCalendar;
    List<MultipartBody.Part> parts = new ArrayList<>();
    MultipartBody.Part pdfParts;
    TextInputEditText etCapacity, etVehicleNumber,etVehicleCode;
    ConnectionDetector connectionDetector;
    String statuscode,typecode,companycode,branchcode,pathPdf ;
    Uri pdfUri ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_vehicle_management);
        status.add("Inactive");status.add("Active");
       init();
       toolgenheader.setText("Vehicle Registration");
filegrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (pdf.getId()==i) {
            parts.clear();
            arrayList.clear();
            imageRecycler.removeAllViews();
pdffile.setVisibility(View.VISIBLE);
imagefile.setVisibility(View.GONE);
        }
        else if (image.getId()==i) {
            pdfParts=null;
            pdfUri=null;
            pdfpath.setText("");
            pdffile.setVisibility(View.GONE);
            imagefile.setVisibility(View.VISIBLE);


        }
    }
});
chooseImage.setOnClickListener(new View.OnClickListener() {
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        parts.clear();
        if (ContextCompat.checkSelfPermission(VehicleManagement.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED&&
                ContextCompat.checkSelfPermission(VehicleManagement.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED) {
            askPermission();
        }
        else {
            showChooser();
        }
    }
});
choosePdf.setOnClickListener(new View.OnClickListener() {
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        pdfParts=null;
        if (ContextCompat.checkSelfPermission(VehicleManagement.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED&&
                ContextCompat.checkSelfPermission(VehicleManagement.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED) {
            askPermission();
        }
        else {

       showPdfChooser();
        }
    }
});
        datelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(VehicleManagement.this,R.style.TimePickerTheme,dateDialog, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        dateDialog=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        toolsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String vehicleNo= etVehicleNumber.getText().toString();
                String code=etVehicleCode.getText().toString();
                String cap=etCapacity.getText().toString();
                String type=tvVehicleType.getText().toString();
                String branch=tvBranch.getText().toString();
                String company=tvVehicleCompany.getText().toString();
                String vDate=date.getText().toString();
                String stat=tvstatus.getText().toString();
                 if (vehicleNo.isEmpty()&&code.isEmpty()&&cap.isEmpty()&&type.equalsIgnoreCase("Vehicle Type")
                         &&company.equalsIgnoreCase("Vehicle Company")
                         &&branch.equalsIgnoreCase("Branch")&&
                         vDate.equalsIgnoreCase("Date")&&stat.equalsIgnoreCase("Status")){
                     Toast.makeText(VehicleManagement.this, "Fill all the details", Toast.LENGTH_SHORT).show();
                 }
                 else if(vehicleNo.isEmpty())
                 {
                     Toast.makeText(VehicleManagement.this, "Enter Name Of Vehicle", Toast.LENGTH_SHORT).show();
                 }
                 else if (type.equalsIgnoreCase("Vehicle Type")){                     Toast.makeText(VehicleManagement.this, "Choose Vehicle Type", Toast.LENGTH_SHORT).show();
                 }
                 else if (code.isEmpty()){                     Toast.makeText(VehicleManagement.this, "Enter Code Of Vehicle", Toast.LENGTH_SHORT).show();
                 }
                 else if (company.equalsIgnoreCase("Vehicle Company")){                     Toast.makeText(VehicleManagement.this, "Choose Vehicle Company", Toast.LENGTH_SHORT).show();
                 }
                 else if (vDate.equalsIgnoreCase("Date"))
                 {                     Toast.makeText(VehicleManagement.this, "Choose Date", Toast.LENGTH_SHORT).show();
                 }
                 else if (cap.isEmpty()){                     Toast.makeText(VehicleManagement.this, "Enter Capacity", Toast.LENGTH_SHORT).show();
                 }

                 else if (branch.equalsIgnoreCase("Branch"))
                 {                     Toast.makeText(VehicleManagement.this, "Choose Branch", Toast.LENGTH_SHORT).show();
                 }


                 else if (stat.equalsIgnoreCase("Status")){   Toast.makeText(VehicleManagement.this, "Choose Status", Toast.LENGTH_SHORT).show();
                 }
                 else {

                     UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
                     RequestBody vname = RequestBody.create(MediaType.parse("multipart/form-data"), vehicleNo);
                     RequestBody vcode = RequestBody.create(MediaType.parse("multipart/form-data"),code);
                     RequestBody vcap = RequestBody.create(MediaType.parse("multipart/form-data"), cap);
                     RequestBody vtype = RequestBody.create(MediaType.parse("multipart/form-data"), typecode);
                     RequestBody vbranch = RequestBody.create(MediaType.parse("multipart/form-data"), branchcode);
                     RequestBody vdate = RequestBody.create(MediaType.parse("multipart/form-data"), vDate);
                     RequestBody vstatus = RequestBody.create(MediaType.parse("multipart/form-data"),statuscode);
                     RequestBody vcompany = RequestBody.create(MediaType.parse("multipart/form-data"),companycode);
                    int ide=filegrp.getCheckedRadioButtonId();
                     if (ide==R.id.pdf)
                     {
                         MultipartBody.Part body=null;
                         if (pdfUri == null)
                         { Toast.makeText(VehicleManagement.this, "File Not Available for Upload", Toast.LENGTH_SHORT).show();
                         }
                         else {
                             if (pdfUri !=null){
                                     pdfParts=prepareFilePart("vehicledoc", pdfUri);
                                 RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                                 body = MultipartBody.Part.createFormData("vehicle_papers[]", "", requestFile);
                                 parts.add(body);
                             }
                             final Dialog progressDialog=new Dialog(VehicleManagement.this);
                             progressDialog.setContentView(R.layout.loadingdialog);
                             progressDialog.setCancelable(false);
                             progressDialog.show();
                             Call<AddVehicleResponse>call=userInterface.addVehicle(vname,vcode,vcap,vtype,vbranch,vdate,vstatus,vcompany,pdfParts,parts);
                             call.enqueue(new Callback<AddVehicleResponse>() {
                                 @Override
                                 public void onResponse(Call<AddVehicleResponse> call, Response<AddVehicleResponse> response) {
                                     if (response.code()==200)
                                     {
                                         Toast.makeText(VehicleManagement.this, "Vehicle Added", Toast.LENGTH_SHORT).show();
                                         finish();
                                     }
                                     else{
                                         progressDialog.dismiss();
                                         Toast.makeText(VehicleManagement.this, "Vehicle Not Added", Toast.LENGTH_SHORT).show();
                                     }
                                 }

                                 @Override
                                 public void onFailure(Call<AddVehicleResponse> call, Throwable t) {
                                     progressDialog.dismiss();
                                     Toast.makeText(VehicleManagement.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                 }
                             });
                         }

                     }
                     else if (ide==R.id.image)
                     {
                         pdfUri=null;
                         if (arrayList.isEmpty())
                         {
                             Toast.makeText(VehicleManagement.this, "Files Not Found", Toast.LENGTH_SHORT).show();
                         }
                         else
                         {
                             MultipartBody.Part body=null;
                             if (arrayList != null) {
                                 for (int i = 0; i < arrayList.size(); i++) {
                                     parts.add(prepareFilePart("vehicle_papers[]", arrayList.get(i)));    //here image is
                                 }
                                 RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                                  body = MultipartBody.Part.createFormData("vehicledoc", "", requestFile);
                             }
                             final Dialog progressDialog=new Dialog(VehicleManagement.this);
                             progressDialog.setContentView(R.layout.loadingdialog);
                             progressDialog.setCancelable(false);
                             progressDialog.show();

                             Call<AddVehicleResponse>call=userInterface.addVehicle(vname,vcode,vcap,vtype,vbranch,vdate,vstatus,vcompany,body,parts);
                             call.enqueue(new Callback<AddVehicleResponse>() {
                                 @Override
                                 public void onResponse(Call<AddVehicleResponse> call, Response<AddVehicleResponse> response) {
                                     if (response.code()==200)
                                     {
                                         progressDialog.dismiss();
                                         Toast.makeText(VehicleManagement.this, "Vehicle Added", Toast.LENGTH_SHORT).show();
                                         Intent intent=new Intent(VehicleManagement.this,VehicleList.class);
                                         intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                         finish();
                                     }
                                     else {
                                         progressDialog.dismiss();
                                         Toast.makeText(VehicleManagement.this, "Vehicle Not Added", Toast.LENGTH_SHORT).show();
                                     }
                                 }
                                 @Override
                                 public void onFailure(Call<AddVehicleResponse> call, Throwable t) {
                                     progressDialog.dismiss();
                                     Toast.makeText(VehicleManagement.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                 }
                             });
                         }
                     }
                     else {
                         Toast.makeText(VehicleManagement.this, "Choose Document Type to upload", Toast.LENGTH_SHORT).show();
                     }
                 }

            }
        });
        statuslay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statusDialog.setContentView(R.layout.recyclerdialog);
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                int width = metrics.widthPixels;
                statusDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                recyclerstatus = statusDialog.findViewById(R.id.recycleroption);
                recyclerstatus.setLayoutManager(new LinearLayoutManager(VehicleManagement.this, LinearLayoutManager.VERTICAL, false));
                recyclerstatus.setAdapter(new StatusAdapter(VehicleManagement.this,status, statusDialog,new CustomItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position, String value) {
                        tvstatus.setText(value);
                        statuscode=String.valueOf(position);
                    }
                }));
                statusDialog.show();
            }
        });
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        vehtypelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final  Dialog progress=new Dialog(VehicleManagement.this);
                progress.setContentView(R.layout.loadingdialog);
                progress.setCancelable(false);
                progress.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<DropDownVehicleTypeResponse> call=userInterface.dropDownvehicleTypeList();
                call.enqueue(new Callback<DropDownVehicleTypeResponse>() {
                    @Override
                    public void onResponse(Call<DropDownVehicleTypeResponse> call, Response<DropDownVehicleTypeResponse> response) {
                        if (response.code()==200)
                        {
                            progress.dismiss();
                            final Dialog vehicleTypeDialog=new Dialog(VehicleManagement.this);
                            vehicleTypeDialog.setContentView(R.layout.recyclerdialog);
                            vehicleTypeDialog.setCancelable(true);
                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                            int width = metrics.widthPixels;
                            vehicleTypeRecycler=vehicleTypeDialog.findViewById(R.id.recycleroption);
                            vehicleTypeDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            List<DropDownVehicleTypeResponse.DataBean> vehicleType=new ArrayList<>();
                            vehicleType=response.body().getData();
                            vehicleTypeRecycler.setLayoutManager(new LinearLayoutManager(VehicleManagement.this,LinearLayoutManager.VERTICAL,false));
                            vehicleTypeRecycler.setAdapter(new VehicleTypeAdapter(VehicleManagement.this,vehicleType,vehicleTypeDialog,new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, int position, String value) {
                                tvVehicleType.setText(value);
                                typecode=String.valueOf(position);
                                }
                            }));
                            vehicleTypeDialog.show();
                        }
                        else
                        {
                            progress.dismiss();
                            Snackbar.make(VehicleManagement.this.getWindow().getDecorView().findViewById(android.R.id.content),"Vehicle Type Not available",Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DropDownVehicleTypeResponse> call, Throwable t) {
                        progress.dismiss();
                        Snackbar.make(VehicleManagement.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();

                    }
                });

            }
        });
        vehCompanyLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final  Dialog progress=new Dialog(VehicleManagement.this);
                progress.setContentView(R.layout.loadingdialog);
                progress.setCancelable(false);
                progress.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<DropDownVehicleCompanyResponse>call=userInterface.dropDownvehicleCompanyList();
                call.enqueue(new Callback<DropDownVehicleCompanyResponse>() {
                    @Override
                    public void onResponse(Call<DropDownVehicleCompanyResponse> call, Response<DropDownVehicleCompanyResponse> response) {
                        if (response.code()==200)
                        {
                            progress.dismiss();
                            final Dialog vehicleCompanyDialog=new Dialog(VehicleManagement.this);
                            vehicleCompanyDialog.setContentView(R.layout.recyclerdialog);
                            vehicleCompanyDialog.setCancelable(true);
                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                            int width = metrics.widthPixels;
                            vehicleTypeRecycler=vehicleCompanyDialog.findViewById(R.id.recycleroption);
                            vehicleCompanyDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            List<DropDownVehicleCompanyResponse.DataBean> vehicleCompany=new ArrayList<>();
                            vehicleCompany=response.body().getData();
                            vehicleTypeRecycler.setLayoutManager(new LinearLayoutManager(VehicleManagement.this,LinearLayoutManager.VERTICAL,false));
                            vehicleTypeRecycler.setAdapter(new VehicleCompanyAdapter(VehicleManagement.this,vehicleCompany,vehicleCompanyDialog,new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, int position, String value) {
                                    tvVehicleCompany.setText(value);
                                    companycode=String.valueOf(position);
                                }
                            }));
                            vehicleCompanyDialog.show();
                        }
                        else
                        {
                            progress.dismiss();
                            Snackbar.make(VehicleManagement.this.getWindow().getDecorView().findViewById(android.R.id.content),"Vehicle Company Not available",Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DropDownVehicleCompanyResponse> call, Throwable t) {
                        progress.dismiss();
                        Snackbar.make(VehicleManagement.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();

                    }
                });
            }
        });
        branchlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final  Dialog progress=new Dialog(VehicleManagement.this);
                progress.setContentView(R.layout.loadingdialog);
                progress.setCancelable(false);
                progress.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<DropDownBranchResponse>call=userInterface.dropDownBranchList();
                call.enqueue(new Callback<DropDownBranchResponse>() {
                    @Override
                    public void onResponse(Call<DropDownBranchResponse> call, Response<DropDownBranchResponse> response) {
                        if (response.code()==200)
                        {
                            progress.dismiss();
                            final Dialog branchDialog=new Dialog(VehicleManagement.this);
                            branchDialog.setContentView(R.layout.recyclerdialog);
                            branchDialog.setCancelable(true);
                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                            int width = metrics.widthPixels;
                            branchRecycler=branchDialog.findViewById(R.id.recycleroption);
                            branchDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            List<DropDownBranchResponse.DataBean> branch=new ArrayList<>();
                            branch=response.body().getData();
                            branchRecycler.setLayoutManager(new LinearLayoutManager(VehicleManagement.this,LinearLayoutManager.VERTICAL,false));
                            branchRecycler.setAdapter(new BranchAdapter(VehicleManagement.this,branch,branchDialog,new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, int position, String value) {
                                    tvBranch.setText(value);
                                    branchcode=String.valueOf(position);
                                }
                            }));
                            branchDialog.show();
                        }
                        else
                        {
                            progress.dismiss();
                            Snackbar.make(VehicleManagement.this.getWindow().getDecorView().findViewById(android.R.id.content),"Branch Not available",Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DropDownBranchResponse> call, Throwable t) {
                        Toast.makeText(VehicleManagement.this, ""+t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }//ONCREATE METHOD

/*--------------------------------METHODS-----------------------------------*/
    private void init() {
        connectionDetector=new ConnectionDetector(VehicleManagement.this);
        etCapacity=findViewById(R.id.etCapacity);
        etVehicleNumber =findViewById(R.id.etVehicleNumber);
        etVehicleCode=findViewById(R.id.etVehicleCode);
        branchlay=findViewById(R.id.branchlay);
        tvBranch=findViewById(R.id.branch);
        vehtypelay=findViewById(R.id.vehtypelay);
    pdffile=findViewById(R.id.pdffile);
    imagefile=findViewById(R.id.imagefile);
    pdf=findViewById(R.id.pdf);
    pdfpath=findViewById(R.id.aadharpdfpath);
    image=findViewById(R.id.image);
    imageRecycler=findViewById(R.id.aadharimage);
    filegrp=findViewById(R.id.filegrp);
    chooseImage=findViewById(R.id.chooseAadharImage);
    choosePdf=findViewById(R.id.chooseAadharpdf);
    datelayout=findViewById(R.id.datelay);
    date=findViewById(R.id.date);
    myCalendar = Calendar.getInstance();
    toolgenheader=findViewById(R.id.toolgenheader);
    toolsubmit=findViewById(R.id.toolsubmit);
    statuslay=findViewById(R.id.statuslay);
        tvstatus=findViewById(R.id.status);
        statusDialog=new Dialog(VehicleManagement.this);
        goBack=findViewById(R.id.toolgoback);
        tvVehicleType =findViewById(R.id.vehicle);
        vehCompanyLay=findViewById(R.id.vehcomlay);
        tvVehicleCompany=findViewById(R.id.vehcompany);
}
    private void showChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CODE_READ_STORAGE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_READ_STORAGE) {
                if (resultData != null) {
                    if (resultData.getClipData() != null) {
                        int count = resultData.getClipData().getItemCount();
                        int currentItem = 0;
                        while (currentItem < 3) {
                            Uri imageUri = resultData.getClipData().getItemAt(currentItem).getUri();
                            currentItem = currentItem + 1;
                            Log.d("Uri Selected", imageUri.toString());
                            try {
                                arrayList.add(imageUri);
                                imageRecycler.setLayoutManager(new LinearLayoutManager(VehicleManagement.this,LinearLayoutManager.HORIZONTAL,false));
                                imageRecycler.setAdapter(new MyImageAdapter(VehicleManagement.this,arrayList));
                            } catch (Exception e) {
                                Log.e("TAG", "File select error", e);
                            }
                        }
                    }
                    else if (resultData.getData() != null) {
                        final Uri uri = resultData.getData();
                        Log.i("TAG", "Uri = " + uri.toString());
                        try {
                            arrayList.add(uri);
                            imageRecycler.setLayoutManager(new LinearLayoutManager(VehicleManagement.this,LinearLayoutManager.HORIZONTAL,false));
                            imageRecycler.setAdapter(new MyImageAdapter(VehicleManagement.this,arrayList));

                        } catch (Exception e) {
                            Log.e("TAG", "File select error", e);
                        }
                    }
                }
            }
            if (requestCode == 1) {
                // Get the Uri of the selected file
                Uri uri = resultData.getData();
//                String [] proj={MediaStore.Images.Media.DATA};
//                Cursor cursor = getContentResolver().query(uri, proj,  null, null, null);
//                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                cursor.moveToFirst();
//                String path = cursor.getString(column_index);
//                cursor.close();
                pdfUri=uri;
              String path= RealPathUtil.getRealPath(VehicleManagement.this,uri);
              pdfpath.setText(path);
            }
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void askPermission() {
        if (ContextCompat.checkSelfPermission(VehicleManagement.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED&&
                ContextCompat.checkSelfPermission(VehicleManagement.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED )
        {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},100);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case 100:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Granted. Start getting the location information
                }
                break;
        }
    }
    public  void showPdfChooser() {
    Intent intent = new Intent();
    intent.setAction(Intent.ACTION_GET_CONTENT);
    intent.setType("application/pdf");
    startActivityForResult(intent,1);
}
    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri)  {
        String path =RealPathUtil.getRealPath(VehicleManagement.this,fileUri);
        File file = new File(path);
        RequestBody requestFile = RequestBody.create (MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(partName, file.getName(),requestFile);
    }
    private void updateLabel() {

        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        date.setText(sdf.format(myCalendar.getTime()));
    }
}
