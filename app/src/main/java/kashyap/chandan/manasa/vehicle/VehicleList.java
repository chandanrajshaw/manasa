package kashyap.chandan.manasa.vehicle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicleList extends AppCompatActivity {
    RecyclerView vehicleList;
    TextView toolHeader,submit;
    ImageView goBack;
    FloatingActionButton addVehicle;
    List<VehicleListResponse.DataBean> vehicles=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_vehicle_list);
        init();
        final Dialog progressDialog=new Dialog(VehicleList.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        submit.setVisibility(View.GONE);
        toolHeader.setText("Vehicles");
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        addVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(VehicleList.this, VehicleManagement.class);
                startActivity(intent);
            }
        });
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<VehicleListResponse> call=userInterface.vehicleList();
        call.enqueue(new Callback<VehicleListResponse>() {
            @Override
            public void onResponse(Call<VehicleListResponse> call, Response<VehicleListResponse> response) {
                if (response.code()==200)
                {
                    vehicles=response.body().getData();
                    vehicleList.setLayoutManager(new LinearLayoutManager(VehicleList.this,LinearLayoutManager.VERTICAL,false));
                    vehicleList.setAdapter(new VehicleListAdapter(VehicleList.this,vehicles));
                    progressDialog.dismiss();
                }
                else
                {
                    progressDialog.dismiss();
                    Snackbar.make(VehicleList.this.getWindow().getDecorView().findViewById(android.R.id.content),"No Vehicle available",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<VehicleListResponse> call, Throwable t) {

            }
        });
    }
    void init()
    {
        vehicleList=findViewById(R.id.vehicleList);
        goBack=findViewById(R.id.toolgoback);
        addVehicle=findViewById(R.id.addVehicle);
        toolHeader=findViewById(R.id.toolgenheader);
        submit=findViewById(R.id.toolsubmit);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Dialog progressDialog=new Dialog(VehicleList.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        submit.setVisibility(View.GONE);
        toolHeader.setText("Vehicles");
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        addVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(VehicleList.this, VehicleManagement.class);
                startActivity(intent);
            }
        });
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<VehicleListResponse> call=userInterface.vehicleList();
        call.enqueue(new Callback<VehicleListResponse>() {
            @Override
            public void onResponse(Call<VehicleListResponse> call, Response<VehicleListResponse> response) {
                if (response.code()==200)
                {
                    vehicles=response.body().getData();
                    vehicleList.setLayoutManager(new LinearLayoutManager(VehicleList.this,LinearLayoutManager.VERTICAL,false));
                    vehicleList.setAdapter(new VehicleListAdapter(VehicleList.this,vehicles));
                    progressDialog.dismiss();
                }
                else
                {
                    progressDialog.dismiss();
                    Snackbar.make(VehicleList.this.getWindow().getDecorView().findViewById(android.R.id.content),"Vehicle List Not available",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<VehicleListResponse> call, Throwable t) {

            }
        });
    }
}
