package kashyap.chandan.manasa.vehicle;

import java.io.Serializable;
import java.util.List;

public class VehicleListResponse implements Serializable {


    /**
     * status : {"code":200,"message":"Added vehicle list"}
     * data : [{"id":"1","vehicletype_id":"2","vehicle_number":"TN 02 2007","vehicle_code":"T20","vehiclecompany_id":"3","vehicle_papers":"","vehicledoc":"upload/776432762advance_java.pdf","purchased_date":"29/07/2020","capacity":"200cc","branch_id":"2","status":"1","vehicletype_name":"Bus","vehiclecompany_name":"Bharat-Benz","branches_name":"Hydrabad"},{"id":"2","vehicletype_id":"1","vehicle_number":"TN 02GH 2007","vehicle_code":"vv","vehiclecompany_id":"1","vehicle_papers":"","vehicledoc":"upload/18694882703.pdf","purchased_date":"29-07-2020","capacity":"vv","branch_id":"1","status":"1","vehicletype_name":"Van","vehiclecompany_name":"Tata","branches_name":"New-delhi"},{"id":"3","vehicletype_id":"5","vehicle_number":"TN 3JH 3476","vehicle_code":"1200","vehiclecompany_id":"1","vehicle_papers":"","vehicledoc":"upload/248299422advance_java.pdf","purchased_date":"30/07/2020","capacity":"100cc","branch_id":"2","status":"1","vehicletype_name":"Auto","vehiclecompany_name":"Tata","branches_name":"Hydrabad"},{"id":"4","vehicletype_id":"1","vehicle_number":"TN 33GH 7689","vehicle_code":"bal","vehiclecompany_id":"3","vehicle_papers":"","vehicledoc":"upload/1665258712advance_java.pdf","purchased_date":"29/07/2020","capacity":"800cc","branch_id":"2","status":"1","vehicletype_name":"Van","vehiclecompany_name":"Bharat-Benz","branches_name":"Hydrabad"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : Added vehicle list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * vehicletype_id : 2
         * vehicle_number : TN 02 2007
         * vehicle_code : T20
         * vehiclecompany_id : 3
         * vehicle_papers :
         * vehicledoc : upload/776432762advance_java.pdf
         * purchased_date : 29/07/2020
         * capacity : 200cc
         * branch_id : 2
         * status : 1
         * vehicletype_name : Bus
         * vehiclecompany_name : Bharat-Benz
         * branches_name : Hydrabad
         */

        private String id;
        private String vehicletype_id;
        private String vehicle_number;
        private String vehicle_code;
        private String vehiclecompany_id;
        private String vehicle_papers;
        private String vehicledoc;
        private String purchased_date;
        private String capacity;
        private String branch_id;
        private String status;
        private String vehicletype_name;
        private String vehiclecompany_name;
        private String branches_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVehicletype_id() {
            return vehicletype_id;
        }

        public void setVehicletype_id(String vehicletype_id) {
            this.vehicletype_id = vehicletype_id;
        }

        public String getVehicle_number() {
            return vehicle_number;
        }

        public void setVehicle_number(String vehicle_number) {
            this.vehicle_number = vehicle_number;
        }

        public String getVehicle_code() {
            return vehicle_code;
        }

        public void setVehicle_code(String vehicle_code) {
            this.vehicle_code = vehicle_code;
        }

        public String getVehiclecompany_id() {
            return vehiclecompany_id;
        }

        public void setVehiclecompany_id(String vehiclecompany_id) {
            this.vehiclecompany_id = vehiclecompany_id;
        }

        public String getVehicle_papers() {
            return vehicle_papers;
        }

        public void setVehicle_papers(String vehicle_papers) {
            this.vehicle_papers = vehicle_papers;
        }

        public String getVehicledoc() {
            return vehicledoc;
        }

        public void setVehicledoc(String vehicledoc) {
            this.vehicledoc = vehicledoc;
        }

        public String getPurchased_date() {
            return purchased_date;
        }

        public void setPurchased_date(String purchased_date) {
            this.purchased_date = purchased_date;
        }

        public String getCapacity() {
            return capacity;
        }

        public void setCapacity(String capacity) {
            this.capacity = capacity;
        }

        public String getBranch_id() {
            return branch_id;
        }

        public void setBranch_id(String branch_id) {
            this.branch_id = branch_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getVehicletype_name() {
            return vehicletype_name;
        }

        public void setVehicletype_name(String vehicletype_name) {
            this.vehicletype_name = vehicletype_name;
        }

        public String getVehiclecompany_name() {
            return vehiclecompany_name;
        }

        public void setVehiclecompany_name(String vehiclecompany_name) {
            this.vehiclecompany_name = vehiclecompany_name;
        }

        public String getBranches_name() {
            return branches_name;
        }

        public void setBranches_name(String branches_name) {
            this.branches_name = branches_name;
        }
    }
}
