package kashyap.chandan.manasa.valuespart;

import java.io.Serializable;
import java.util.List;

public class DropDownBranchResponse implements Serializable {
    /**
     * status : {"code":200,"message":"Branch list"}
     * data : [{"id":"1","branches_name":"New-delhi","status":"1"},{"id":"2","branches_name":"Hydrabad","status":"1"},{"id":"3","branches_name":"Ranchi","status":"1"},{"id":"4","branches_name":"Tatanagar","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable{
        /**
         * code : 200
         * message : Branch list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable{
        /**
         * id : 1
         * branches_name : New-delhi
         * status : 1
         */

        private String id;
        private String branches_name;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBranches_name() {
            return branches_name;
        }

        public void setBranches_name(String branches_name) {
            this.branches_name = branches_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
