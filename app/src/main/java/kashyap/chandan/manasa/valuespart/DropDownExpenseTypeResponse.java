package kashyap.chandan.manasa.valuespart;

import java.io.Serializable;
import java.util.List;

public class DropDownExpenseTypeResponse implements Serializable {
    /**
     * status : {"code":200,"message":"expensetype list"}
     * data : [{"id":"1","expensetype_name":"bus tickets","status":"1"},{"id":"2","expensetype_name":"taxi receipts","status":"1"},{"id":"3","expensetype_name":"train and plane tickets","status":"1"},{"id":"4","expensetype_name":"Fuel","status":"1"},{"id":"5","expensetype_name":"repair","status":"0"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : expensetype list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * expensetype_name : bus tickets
         * status : 1
         */

        private String id;
        private String expensetype_name;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getExpensetype_name() {
            return expensetype_name;
        }

        public void setExpensetype_name(String expensetype_name) {
            this.expensetype_name = expensetype_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
