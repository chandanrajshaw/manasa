package kashyap.chandan.manasa.changepin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.ApiError;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.invoice.AddParticular;
import kashyap.chandan.manasa.login.Login;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ChangePassword extends AppCompatActivity {
TextView changePassword,toolSave,toolHeader;
TextInputEditText etOldPassword,etNewPassword,etConfirmPassword;
SharedPreferences sharedPreferences;
ImageView goBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        init();
        toolHeader.setText("Change Password");
        sharedPreferences=getSharedPreferences("Login",MODE_PRIVATE);
toolSave.setVisibility(View.GONE);
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email= sharedPreferences.getString("userName",null);
                String oldPassword=etOldPassword.getText().toString().trim();
                String newPassword=etNewPassword.getText().toString().trim();
                String confirmPassword=etConfirmPassword.getText().toString().trim();
                if (oldPassword.isEmpty()&&newPassword.isEmpty()&&confirmPassword.isEmpty())
                    Toast.makeText(ChangePassword.this, "Enter All Fields", Toast.LENGTH_SHORT).show();
                else if (oldPassword.isEmpty())
                    Toast.makeText(ChangePassword.this, "Enter Old Password", Toast.LENGTH_SHORT).show();
                else if (newPassword.isEmpty())
                    Toast.makeText(ChangePassword.this, "Enter New Password", Toast.LENGTH_SHORT).show();
                else if (confirmPassword.isEmpty())
                    Toast.makeText(ChangePassword.this, "Confirm Password", Toast.LENGTH_SHORT).show();
                else if (!newPassword.equals(confirmPassword))
                    Toast.makeText(ChangePassword.this, "Confirm Password Mismatch", Toast.LENGTH_SHORT).show();
                else {
                    final Dialog progressdialog=new Dialog(ChangePassword.this);
                    progressdialog.setContentView(R.layout.loadingdialog);
                    progressdialog.setCancelable(false);
                    progressdialog.show();
                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<ChangePasswordResponse>call=userInterface.changePassword(email,oldPassword,confirmPassword);
                    call.enqueue(new Callback<ChangePasswordResponse>() {
                        @Override
                        public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                            if (response.code()==200)
                            {
                                SharedPreferences.Editor editor=sharedPreferences.edit();
                                editor.clear();
                                editor.commit();
                                progressdialog.dismiss();
                                Toast.makeText(ChangePassword.this, "Password Changed Successfully", Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(ChangePassword.this,Login.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                            else {
                                progressdialog.dismiss();
                                Converter<ResponseBody, ApiError> converter =
                                        ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                ApiError error;
                                try {
                                    error = converter.convert(response.errorBody());
                                    ApiError.StatusBean status=error.getStatus();
                                    Toast.makeText(ChangePassword.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                } catch (IOException e) { e.printStackTrace(); }
                            }
                        }

                        @Override
                        public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
progressdialog.dismiss();
                            Toast.makeText(ChangePassword.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        });
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
    }
   private void init()
    {
        toolHeader=findViewById(R.id.toolgenheader);
        toolSave=findViewById(R.id.toolsubmit);
        goBack=findViewById(R.id.toolgoback);
        changePassword=findViewById(R.id.changePin);
        etConfirmPassword=findViewById(R.id.etConfirmPassword);
        etOldPassword=findViewById(R.id.etOldPassword);
        etNewPassword=findViewById(R.id.etNewPassword);

    }
}
