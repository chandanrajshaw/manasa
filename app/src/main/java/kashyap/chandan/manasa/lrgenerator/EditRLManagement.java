package kashyap.chandan.manasa.lrgenerator;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.StatusAdapter;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.adapters.VehicleTypeAdapter;
import kashyap.chandan.manasa.valuespart.DropDownVehicleTypeResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditRLManagement extends AppCompatActivity {
    RelativeLayout paymentlay,datelayout,vehiclelay;
TextView tvtoolheader,payment,date,tvvehicletype,submit;
RecyclerView recPayment,vehicleTypeRecycler;
ArrayList<String>payType;
Dialog paymentDialog;
    DatePickerDialog.OnDateSetListener dateDialog;
    Calendar myCalendar;
    LRListResponse.DataBean lrDetail;
ImageView goBack;
    String typecode,paymentCode;
TextInputEditText etVehNo,etLrNo,etconsigneer,etconsignee,etfrom,etto,etdesc,etmtype,etinvoice,etweigh,etcharge,etRate,etvalue,etRemark;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_r_l_management);
        final Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("bundle");
        lrDetail= (LRListResponse.DataBean) bundle.getSerializable("lrDetail");
        init();
        payType.add("Cash");payType.add("Card"); payType.add("Online");payType.add("Pay Later");
        tvtoolheader.setText("Edit L-R");
        etVehNo.setText(lrDetail.getVehicle_no());
        etLrNo.setText(lrDetail.getLr_no());
        tvvehicletype.setText(lrDetail.getVehicletype_name());
        etconsigneer.setText(lrDetail.getConsignor());
        etconsignee.setText(lrDetail.getConsignee());
        date.setText(lrDetail.getLrgen_date());
        etdesc.setText(lrDetail.getMaterial_desc());
        etfrom.setText(lrDetail.getLr_from());
        etto.setText(lrDetail.getLr_to());
        etmtype.setText(lrDetail.getPackageX());
        etinvoice.setText(lrDetail.getParty_inv_no());
        etweigh.setText(lrDetail.getActual_wt());
        etcharge.setText(lrDetail.getCharge_wt());
        etRate.setText(lrDetail.getRate());
        etvalue.setText(lrDetail.getValue_good());
        typecode=lrDetail.getVehicletype_id();
        paymentCode=lrDetail.getPayment_mode();
        if (paymentCode.equalsIgnoreCase("0"))
            payment.setText("Cash");
        else if (paymentCode.equalsIgnoreCase("1"))
            payment.setText("Card");
        else if (paymentCode.equalsIgnoreCase("2"))
            payment.setText("Online");
        else if (paymentCode.equalsIgnoreCase("3"))
            payment.setText("Pay Later");
        else
            payment.setText("Payment Type");
        paymentlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentDialog.setContentView(R.layout.recyclerdialog);
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                int width = metrics.widthPixels;
                paymentDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                recPayment = paymentDialog.findViewById(R.id.recycleroption);
                recPayment.setLayoutManager(new LinearLayoutManager(EditRLManagement.this, LinearLayoutManager.VERTICAL, false));
                recPayment.setAdapter(new StatusAdapter(EditRLManagement.this,payType, paymentDialog,new CustomItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position, String value) {
                        payment.setText(value);
                        paymentCode=String.valueOf(position);
                    }
                }));
                paymentDialog.show();
            }
        });
        vehiclelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final  Dialog progress=new Dialog(EditRLManagement.this);
                progress.setContentView(R.layout.loadingdialog);
                progress.setCancelable(false);
                progress.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<DropDownVehicleTypeResponse> call=userInterface.dropDownvehicleTypeList();
                call.enqueue(new Callback<DropDownVehicleTypeResponse>() {
                    @Override
                    public void onResponse(Call<DropDownVehicleTypeResponse> call, Response<DropDownVehicleTypeResponse> response) {
                        if (response.code()==200)
                        {
                            progress.dismiss();
                            final Dialog vehicleTypeDialog=new Dialog(EditRLManagement.this);
                            vehicleTypeDialog.setContentView(R.layout.recyclerdialog);
                            vehicleTypeDialog.setCancelable(true);
                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                            int width = metrics.widthPixels;
                            vehicleTypeRecycler=vehicleTypeDialog.findViewById(R.id.recycleroption);
                            vehicleTypeDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            List<DropDownVehicleTypeResponse.DataBean> vehicleType=new ArrayList<>();
                            vehicleType=response.body().getData();
                            vehicleTypeRecycler.setLayoutManager(new LinearLayoutManager(EditRLManagement.this,LinearLayoutManager.VERTICAL,false));
                            vehicleTypeRecycler.setAdapter(new VehicleTypeAdapter(EditRLManagement.this,vehicleType,vehicleTypeDialog,new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, int position, String value) {
                                    tvvehicletype.setText(value);
                                    typecode=String.valueOf(position);
                                }
                            }));
                            vehicleTypeDialog.show();
                        }
                        else
                        {
                            progress.dismiss();
                            Snackbar.make(EditRLManagement.this.getWindow().getDecorView().findViewById(android.R.id.content),"Vehicle Type Not available",Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DropDownVehicleTypeResponse> call, Throwable t) {
                        progress.dismiss();
                        Snackbar.make(EditRLManagement.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();

                    }
                });
            }
        });
        datelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(EditRLManagement.this,R.style.TimePickerTheme,dateDialog, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        dateDialog=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lr_no=etLrNo.getText().toString().trim();
                String Date=date.getText().toString();
                String vehNo=etVehNo.getText().toString();
                String consigneer=etconsigneer.getText().toString();
                String consignee=etconsignee.getText().toString();
                String from=etfrom.getText().toString();
                String to=etto.getText().toString();
                String desc=etdesc.getText().toString();
                String mType=etmtype.getText().toString();
                String invoice=etinvoice.getText().toString();
                String weight=etweigh.getText().toString();
                String charge=etcharge.getText().toString();
                String rate=etRate.getText().toString();
                String value=etvalue.getText().toString();
                String remark=etRemark.getText().toString();
                String vType=tvvehicletype.getText().toString();
                String pType=paymentCode;
                if (lr_no.isEmpty()&&Date.equalsIgnoreCase("Date") &&typecode==null&&vehNo.isEmpty()&&consigneer.isEmpty()&&consignee.isEmpty()&&from.isEmpty()&&to.isEmpty()&&desc.isEmpty()&&mType.isEmpty()&&
                        invoice.isEmpty()&&weight.isEmpty()&&charge.isEmpty()&&rate.isEmpty()&&value.isEmpty()
                        &&paymentCode==null)
                    Toast.makeText(EditRLManagement.this, "Fill All Details", Toast.LENGTH_SHORT).show();
                else if (lr_no.isEmpty())
                    Toast.makeText(EditRLManagement.this, "Enter LR Number", Toast.LENGTH_SHORT).show();
                else if (Date.equalsIgnoreCase("Date"))
                    Toast.makeText(EditRLManagement.this, "Choose Date", Toast.LENGTH_SHORT).show();
                else if (typecode==null)
                    Toast.makeText(EditRLManagement.this, "Choose Vehicle Type", Toast.LENGTH_SHORT).show();
                else if (vehNo.isEmpty()) Toast.makeText(EditRLManagement.this, "Enter Vehicle No", Toast.LENGTH_SHORT).show();

                else if (consigneer.isEmpty()) Toast.makeText(EditRLManagement.this, "Enter Consigneer Address", Toast.LENGTH_SHORT).show();
                else if (consignee.isEmpty()) Toast.makeText(EditRLManagement.this, "Enter Consignee Address", Toast.LENGTH_SHORT).show();
                else if (from.isEmpty())                   Toast.makeText(EditRLManagement.this, "Enter Source Point", Toast.LENGTH_SHORT).show();
                else if (to.isEmpty())                   Toast.makeText(EditRLManagement.this, "Enter Destination", Toast.LENGTH_SHORT).show();
                else if (desc.isEmpty())                   Toast.makeText(EditRLManagement.this, "Enter Material Description", Toast.LENGTH_SHORT).show();
                else if (mType.isEmpty())                   Toast.makeText(EditRLManagement.this, "Enter Material Type", Toast.LENGTH_SHORT).show();
                else if (invoice.isEmpty())                   Toast.makeText(EditRLManagement.this, "Enter Invoice Number", Toast.LENGTH_SHORT).show();
                else if (weight.isEmpty())                   Toast.makeText(EditRLManagement.this, "Enter Weight in Kg", Toast.LENGTH_SHORT).show();
                else if (charge.isEmpty())                   Toast.makeText(EditRLManagement.this, "Enter Charge", Toast.LENGTH_SHORT).show();
                else if (rate.isEmpty())                   Toast.makeText(EditRLManagement.this, "Enter Rate/Kg", Toast.LENGTH_SHORT).show();
                else if (value.isEmpty())                   Toast.makeText(EditRLManagement.this, "Enter Value of Goods", Toast.LENGTH_SHORT).show();
                else if (paymentCode==null) Toast.makeText(EditRLManagement.this, "Choose Payment Type", Toast.LENGTH_SHORT).show();
                else {
                    final  Dialog progress=new Dialog(EditRLManagement.this);
                    progress.setContentView(R.layout.loadingdialog);
                    progress.setCancelable(false);
                    progress.show();
                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                  Call<EditLrResponse>call=userInterface.editLR(lr_no,lrDetail.getId(),Date,typecode,consigneer,consignee
                          ,from,to,desc,mType,invoice,weight,charge,rate,value,paymentCode,remark,vehNo);
                    call.enqueue(new Callback<EditLrResponse>() {
                        @Override
                        public void onResponse(Call<EditLrResponse> call, Response<EditLrResponse> response) {
                            if (response.code()==200)
                            { progress.dismiss();
                                Toast.makeText(EditRLManagement.this, "L-R Updated", Toast.LENGTH_SHORT).show();
                                Intent intent1=new Intent(EditRLManagement.this,LrList.class);
                                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent1);
                                finish(); }
                            else { progress.dismiss();
                                Toast.makeText(EditRLManagement.this, "L-R Not Updated", Toast.LENGTH_SHORT).show(); }
                        }
                        @Override
                        public void onFailure(Call<EditLrResponse> call, Throwable t) {
                            Toast.makeText(EditRLManagement.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }
        });
    }

    void init()
    {
        etVehNo=findViewById(R.id.etVehNo);
        etLrNo=findViewById(R.id.etLrNo);
        etRemark=findViewById(R.id.etRemark);
        submit=findViewById(R.id.toolsubmit);
        vehiclelay=findViewById(R.id.vehiclelay);
        paymentDialog=new Dialog(EditRLManagement.this);
        etvalue=findViewById(R.id.etvalue);
        etRate=findViewById(R.id.etRate);
        etcharge=findViewById(R.id.etcharge);
        etweigh=findViewById(R.id.etweigh);
        etinvoice=findViewById(R.id.etinvoice);
        etmtype=findViewById(R.id.etmtype);
        etdesc=findViewById(R.id.etdesc);
        etto=findViewById(R.id.etto);
        etfrom=findViewById(R.id.etfrom);
        etconsignee=findViewById(R.id.etconsignee);
        myCalendar=Calendar.getInstance();
        tvvehicletype=findViewById(R.id.tvvehicletype);
        etconsigneer=findViewById(R.id.etconsigneer);
        payType=new ArrayList<>();
        tvtoolheader=findViewById(R.id.toolgenheader);
        paymentlay=findViewById(R.id.paymentlay);
        payment=findViewById(R.id.payment);
        date=findViewById(R.id.date);
        datelayout=findViewById(R.id.datelay);
        goBack=findViewById(R.id.toolgoback);
    }
    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        date.setText(sdf.format(myCalendar.getTime()));
    }
}
