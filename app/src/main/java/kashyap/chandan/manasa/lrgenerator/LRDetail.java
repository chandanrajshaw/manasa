package kashyap.chandan.manasa.lrgenerator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import kashyap.chandan.manasa.R;

public class LRDetail extends AppCompatActivity {
TextView vehicleNo,lrNumber,editLR,toolHeader,vehicleType,consigner,consignee,date,from,to,desc,mtype,invoiceno,weight,charge,rate,value,pay;
ImageView goBack;
LRListResponse.DataBean lrDetail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_l_r_detail);
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("bundle");
        lrDetail= (LRListResponse.DataBean) bundle.getSerializable("lrDetail");
        init();
        lrNumber.setText(lrDetail.getLr_no());
        editLR.setText("Edit");
        toolHeader.setText("L-R Details");
        vehicleType.setText(lrDetail.getVehicletype_name());
        consigner.setText(lrDetail.getConsignor());
        consignee.setText(lrDetail.getConsignee());
        date.setText(lrDetail.getLrgen_date());
        desc.setText(lrDetail.getMaterial_desc());
        from.setText(lrDetail.getLr_from());
        to.setText(lrDetail.getLr_to());
        mtype.setText(lrDetail.getPackageX());
        invoiceno.setText(lrDetail.getParty_inv_no());
        weight.setText(lrDetail.getActual_wt());
        charge.setText(lrDetail.getCharge_wt());
        rate.setText(lrDetail.getRate());
        value.setText(lrDetail.getValue_good());
        vehicleNo.setText(lrDetail.getVehicle_no());
        int paymentMode= Integer.parseInt(lrDetail.getPayment_mode());
        if (paymentMode==0)
        {
            pay.setText("Cash");
        }
        else if (paymentMode==1)
            pay.setText("Card");
        else if (paymentMode==2)
            pay.setText("Online");
        else if (paymentMode==3)
            pay.setText("Pay Later");
        else
            pay.setText("Not Available");
        editLR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LRDetail.this,EditRLManagement.class);
                Bundle bundle=new Bundle();
                bundle.putSerializable("lrDetail",lrDetail);
                intent.putExtra("bundle",bundle);
                startActivity(intent);
            }
        });
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
    }
    void init()
    {
        vehicleNo=findViewById(R.id.vehicleNo);
        lrNumber=findViewById(R.id.lrNumber);
        pay=findViewById(R.id.pay);
        value=findViewById(R.id.value);
        rate=findViewById(R.id.rate);
        charge=findViewById(R.id.charge);
        weight=findViewById(R.id.weight);
        invoiceno=findViewById(R.id.invoiceno);
        mtype=findViewById(R.id.mtype);
        desc=findViewById(R.id.desc);
        to=findViewById(R.id.to);
        from=findViewById(R.id.from);
        date=findViewById(R.id.date);
        consignee=findViewById(R.id.consignee);
        consigner=findViewById(R.id.consigner);
        vehicleType=findViewById(R.id.vehicleType);
        goBack=findViewById(R.id.toolgoback);
        editLR=findViewById(R.id.toolsubmit);
        toolHeader=findViewById(R.id.toolgenheader);
    }
}
