package kashyap.chandan.manasa.lrgenerator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LrList extends AppCompatActivity {
    RecyclerView LrList;
    TextView toolHeader,submit;
    ImageView goBack;
    FloatingActionButton addLR;
    List<LRListResponse.DataBean> lr=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_lr_list);
        init();
        final Dialog progressDialog=new Dialog(LrList.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        submit.setVisibility(View.GONE);
        toolHeader.setText("LR List");
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        addLR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LrList.this, RLManagement.class);
                startActivity(intent);
            }
        });

        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<LRListResponse> call=userInterface.lrList();
        call.enqueue(new Callback<LRListResponse>() {
            @Override
            public void onResponse(Call<LRListResponse> call, Response<LRListResponse> response) {
                if (response.code()==200)
                {
                    lr=response.body().getData();
                    LrList.setLayoutManager(new LinearLayoutManager(LrList.this,LinearLayoutManager.VERTICAL,false));
                    LrList.setAdapter(new LRListAdapter(LrList.this,lr));
                    progressDialog.dismiss();
                }
                else
                {
                    progressDialog.dismiss();
                    Snackbar.make(LrList.this.getWindow().getDecorView().findViewById(android.R.id.content),"LR List Not available",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LRListResponse> call, Throwable t) {
                progressDialog.dismiss();
                Snackbar.make(LrList.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();

            }
        });
    }

    void init()
    {
        LrList =findViewById(R.id.lrList);
        goBack=findViewById(R.id.toolgoback);
        addLR =findViewById(R.id.addLr);
        toolHeader=findViewById(R.id.toolgenheader);
        submit=findViewById(R.id.toolsubmit);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Dialog progressDialog=new Dialog(LrList.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<LRListResponse> call=userInterface.lrList();
        call.enqueue(new Callback<LRListResponse>() {
            @Override
            public void onResponse(Call<LRListResponse> call, Response<LRListResponse> response) {
                if (response.code()==200)
                {
                    lr=response.body().getData();
                    LrList.setLayoutManager(new LinearLayoutManager(LrList.this,LinearLayoutManager.VERTICAL,false));
                    LrList.setAdapter(new LRListAdapter(LrList.this,lr));
                    progressDialog.dismiss();
                }
                else
                {
                    progressDialog.dismiss();
                    Snackbar.make(LrList.this.getWindow().getDecorView().findViewById(android.R.id.content),"LR List Not available",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LRListResponse> call, Throwable t) {
                progressDialog.dismiss();
                Snackbar.make(LrList.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();

            }
        });
    }
}
