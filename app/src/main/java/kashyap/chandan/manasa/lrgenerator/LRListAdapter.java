package kashyap.chandan.manasa.lrgenerator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.manasa.R;

class LRListAdapter extends RecyclerView.Adapter<LRListAdapter.MyViewHolder> {
    Context context;
    List<LRListResponse.DataBean> lr;
    public LRListAdapter(Context context, List<LRListResponse.DataBean> lr) {
        this.context=context;
        this.lr=lr;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.lrlist,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.from.setText(lr.get(position).getLr_from());
        holder.to.setText(lr.get(position).getLr_to());
        holder.date.setText(lr.get(position).getLrgen_date());
        holder.vehNo.setText(lr.get(position).getVehicletype_name());
        holder.detail.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(context,LRDetail.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("lrDetail",lr.get(position));
        intent.putExtra("bundle",bundle);
        context.startActivity(intent);
    }
});
    }

    @Override
    public int getItemCount() {
        return lr.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout detail;
        TextView vehNo,from,to,date;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            detail=itemView.findViewById(R.id.detail);
            date=itemView.findViewById(R.id.date);
            vehNo=itemView.findViewById(R.id.vehNo);
            from=itemView.findViewById(R.id.from);
            to=itemView.findViewById(R.id.to);
        }
    }
}
