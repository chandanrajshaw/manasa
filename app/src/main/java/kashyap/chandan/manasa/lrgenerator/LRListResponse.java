package kashyap.chandan.manasa.lrgenerator;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class LRListResponse implements Serializable {

    /**
     * status : {"code":200,"message":"Added lrgenerator list"}
     * data : [{"id":"2","lrgen_date":"12-10-2020","lr_no":"22222","vehicle_no":"FDF 55 66","vehicletype_id":"1","consignor":"ppp","consignee":"sss","lr_from":"p, reddy","lr_to":"s. reddy","material_desc":"<p>goods<\/p>\r\n","package":"1kg","party_inv_no":"1","actual_wt":"3","charge_wt":"3","rate":"3","value_good":"200","payment_mode":"0","remark":"<p>good value<\/p>\r\n","vehicletype_name":"Van"},{"id":"4","lrgen_date":"28/05/2020","lr_no":"3232","vehicle_no":"TN 67 6734","vehicletype_id":"1","consignor":"Ichak","consignee":"HZB","lr_from":"Karma","lr_to":"Ranchi","material_desc":"Abcd","package":"Solid","party_inv_no":"1234","actual_wt":"120","charge_wt":"10","rate":"12","value_good":"100000","payment_mode":"2","remark":"","vehicletype_name":"Van"},{"id":"6","lrgen_date":"06/30/2020","lr_no":"3443","vehicle_no":"TN 05 6784","vehicletype_id":"1","consignor":"dssd","consignee":"sdsd","lr_from":"dsd","lr_to":"dsdsd","material_desc":"<p>dsd<\/p>\r\n","package":"dsd","party_inv_no":"dsd","actual_wt":"4","charge_wt":"5","rate":"55","value_good":"gfdgdf","payment_mode":"2","remark":"<p>fdhgfdhgfdg<\/p>\r\n","vehicletype_name":"Van"},{"id":"1","lrgen_date":"28/05/2020","lr_no":"23","vehicle_no":"TN 09 7895","vehicletype_id":"2","consignor":"fff","consignee":"fff","lr_from":"ff","lr_to":"sfsdaf","material_desc":"<p>sfsfdf<\/p>\r\n","package":"ff","party_inv_no":"fff","actual_wt":"5","charge_wt":"3","rate":"5","value_good":"fff","payment_mode":"1","remark":"","vehicletype_name":"Bus"},{"id":"7","lrgen_date":"07/29/2020","lr_no":"34","vehicle_no":"GFDG 545 656","vehicletype_id":"2","consignor":"vishal","consignee":"vishal","lr_from":"vishal","lr_to":"vishal","material_desc":"<p>vishal<\/p>\r\n","package":"vishal234","party_inv_no":"234","actual_wt":"3","charge_wt":"4","rate":"4","value_good":"vishal","payment_mode":"0","remark":"<p>vishal<\/p>\r\n","vehicletype_name":"Bus"},{"id":"8","lrgen_date":"01/08/2020","lr_no":"SL 1001","vehicle_no":"JH 02 5678","vehicletype_id":"2","consignor":"me","consignee":"you","lr_from":"Kanzipet","lr_to":"Ameerpet","material_desc":"Glass","package":"glass","party_inv_no":"1501","actual_wt":"200","charge_wt":"80","rate":"60","value_good":"6000","payment_mode":"3","remark":"Deliver safely","vehicletype_name":"Bus"},{"id":"9","lrgen_date":"08/04/2020","lr_no":"999","vehicle_no":"TN 09 9999","vehicletype_id":"3","consignor":"TEN","consignee":"CD","lr_from":"DDS","lr_to":"DSD","material_desc":"<p>CDCSDV VFDVF FD<\/p>\r\n","package":"FEF","party_inv_no":"434","actual_wt":"2","charge_wt":"2","rate":"22","value_good":"22","payment_mode":"cash","remark":"<p>EWFWEFEW<\/p>\r\n","vehicletype_name":"Scooter"},{"id":"5","lrgen_date":"29/07/2020","lr_no":"434","vehicle_no":"DSDD 3345 RR 44","vehicletype_id":"4","consignor":"me","consignee":"you","lr_from":"Hzb","lr_to":"Rnc","material_desc":"glass","package":"glass","party_inv_no":"12","actual_wt":"12","charge_wt":"20","rate":"20","value_good":"abcd","payment_mode":"3","remark":"ok","vehicletype_name":"Trucks"},{"id":"3","lrgen_date":"30/05/2020","lr_no":"4554","vehicle_no":"KJ 444 RR 44","vehicletype_id":"5","consignor":"Ichak","consignee":"HZB","lr_from":"Karma","lr_to":"Ranchi","material_desc":"<p>Abcd<\/p>\r\n","package":"Solid","party_inv_no":"1234","actual_wt":"120","charge_wt":"10","rate":"12","value_good":"100000","payment_mode":"2","remark":"<p>abcd<\/p>\r\n","vehicletype_name":"Auto"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : Added lrgenerator list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 2
         * lrgen_date : 12-10-2020
         * lr_no : 22222
         * vehicle_no : FDF 55 66
         * vehicletype_id : 1
         * consignor : ppp
         * consignee : sss
         * lr_from : p, reddy
         * lr_to : s. reddy
         * material_desc : <p>goods</p>
         * package : 1kg
         * party_inv_no : 1
         * actual_wt : 3
         * charge_wt : 3
         * rate : 3
         * value_good : 200
         * payment_mode : 0
         * remark : <p>good value</p>
         * vehicletype_name : Van
         */

        private String id;
        private String lrgen_date;
        private String lr_no;
        private String vehicle_no;
        private String vehicletype_id;
        private String consignor;
        private String consignee;
        private String lr_from;
        private String lr_to;
        private String material_desc;
        @SerializedName("package")
        private String packageX;
        private String party_inv_no;
        private String actual_wt;
        private String charge_wt;
        private String rate;
        private String value_good;
        private String payment_mode;
        private String remark;
        private String vehicletype_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLrgen_date() {
            return lrgen_date;
        }

        public void setLrgen_date(String lrgen_date) {
            this.lrgen_date = lrgen_date;
        }

        public String getLr_no() {
            return lr_no;
        }

        public void setLr_no(String lr_no) {
            this.lr_no = lr_no;
        }

        public String getVehicle_no() {
            return vehicle_no;
        }

        public void setVehicle_no(String vehicle_no) {
            this.vehicle_no = vehicle_no;
        }

        public String getVehicletype_id() {
            return vehicletype_id;
        }

        public void setVehicletype_id(String vehicletype_id) {
            this.vehicletype_id = vehicletype_id;
        }

        public String getConsignor() {
            return consignor;
        }

        public void setConsignor(String consignor) {
            this.consignor = consignor;
        }

        public String getConsignee() {
            return consignee;
        }

        public void setConsignee(String consignee) {
            this.consignee = consignee;
        }

        public String getLr_from() {
            return lr_from;
        }

        public void setLr_from(String lr_from) {
            this.lr_from = lr_from;
        }

        public String getLr_to() {
            return lr_to;
        }

        public void setLr_to(String lr_to) {
            this.lr_to = lr_to;
        }

        public String getMaterial_desc() {
            return material_desc;
        }

        public void setMaterial_desc(String material_desc) {
            this.material_desc = material_desc;
        }

        public String getPackageX() {
            return packageX;
        }

        public void setPackageX(String packageX) {
            this.packageX = packageX;
        }

        public String getParty_inv_no() {
            return party_inv_no;
        }

        public void setParty_inv_no(String party_inv_no) {
            this.party_inv_no = party_inv_no;
        }

        public String getActual_wt() {
            return actual_wt;
        }

        public void setActual_wt(String actual_wt) {
            this.actual_wt = actual_wt;
        }

        public String getCharge_wt() {
            return charge_wt;
        }

        public void setCharge_wt(String charge_wt) {
            this.charge_wt = charge_wt;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getValue_good() {
            return value_good;
        }

        public void setValue_good(String value_good) {
            this.value_good = value_good;
        }

        public String getPayment_mode() {
            return payment_mode;
        }

        public void setPayment_mode(String payment_mode) {
            this.payment_mode = payment_mode;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getVehicletype_name() {
            return vehicletype_name;
        }

        public void setVehicletype_name(String vehicletype_name) {
            this.vehicletype_name = vehicletype_name;
        }
    }
}
