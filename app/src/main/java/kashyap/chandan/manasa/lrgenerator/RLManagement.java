package kashyap.chandan.manasa.lrgenerator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.AbstractSequentialList;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.StatusAdapter;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.adapters.VehicleTypeAdapter;
import kashyap.chandan.manasa.valuespart.DropDownVehicleTypeResponse;
import kashyap.chandan.manasa.vehicle.VehicleManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RLManagement extends AppCompatActivity {
    RelativeLayout paymentlay,datelayout,vehiclelay;
    TextView tvtoolheader,payment,date,tvvehicletype,submit;
    RecyclerView recPayment,vehicleTypeRecycler;
    ArrayList<String>payType;
    Dialog paymentDialog;
    DatePickerDialog.OnDateSetListener dateDialog;
    Calendar myCalendar;
    String typecode,paymentCode;
ImageView goBack;
TextInputEditText etVehNo,etLrNo,etRemark,etvalue,etRate,etcharge,etweigh,etinvoice,etmtype,etdesc,etto,etfrom,etconsignee,etconsigneer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_r_l_management);
        init();
        payType.add("Cash");payType.add("Card"); payType.add("Online");payType.add("Pay Later");
        tvtoolheader.setText("Add LR");
        paymentlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentDialog=new Dialog(RLManagement.this);
                paymentDialog.setContentView(R.layout.recyclerdialog);
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                int width = metrics.widthPixels;
                paymentDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                recPayment = paymentDialog.findViewById(R.id.recycleroption);
                recPayment.setLayoutManager(new LinearLayoutManager(RLManagement.this, LinearLayoutManager.VERTICAL, false));
                recPayment.setAdapter(new StatusAdapter(RLManagement.this,payType,paymentDialog,new CustomItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position, String value) {
                        payment.setText(value);
                        paymentCode=String.valueOf(position);
                    }
                }));
                paymentDialog.show();
    }

});
        vehiclelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final  Dialog progress=new Dialog(RLManagement.this);
                progress.setContentView(R.layout.loadingdialog);
                progress.setCancelable(false);
                progress.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<DropDownVehicleTypeResponse> call=userInterface.dropDownvehicleTypeList();
                call.enqueue(new Callback<DropDownVehicleTypeResponse>() {
                    @Override
                    public void onResponse(Call<DropDownVehicleTypeResponse> call, Response<DropDownVehicleTypeResponse> response) {
                        if (response.code()==200)
                        {
                            progress.dismiss();
                            final Dialog vehicleTypeDialog=new Dialog(RLManagement.this);
                            vehicleTypeDialog.setContentView(R.layout.recyclerdialog);
                            vehicleTypeDialog.setCancelable(true);
                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                            int width = metrics.widthPixels;
                            vehicleTypeRecycler=vehicleTypeDialog.findViewById(R.id.recycleroption);
                            vehicleTypeDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            List<DropDownVehicleTypeResponse.DataBean> vehicleType=new ArrayList<>();
                            vehicleType=response.body().getData();
                            vehicleTypeRecycler.setLayoutManager(new LinearLayoutManager(RLManagement.this,LinearLayoutManager.VERTICAL,false));
                            vehicleTypeRecycler.setAdapter(new VehicleTypeAdapter(RLManagement.this,vehicleType,vehicleTypeDialog,new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, int position, String value) {
                                    tvvehicletype.setText(value);
                                    typecode=String.valueOf(position);
                                }
                            }));
                            vehicleTypeDialog.show();
                        }
                        else
                        {
                            progress.dismiss();
                            Snackbar.make(RLManagement.this.getWindow().getDecorView().findViewById(android.R.id.content),"Vehicle Type Not available",Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DropDownVehicleTypeResponse> call, Throwable t) {
                        progress.dismiss();
                        Snackbar.make(RLManagement.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();

                    }
                });
            }
        });
        datelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(RLManagement.this,R.style.TimePickerTheme,dateDialog, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        dateDialog=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
       submit.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
//etRemark,etvalue,etRate,etcharge,etweigh,etinvoice,etmtype,etdesc,etto,etfrom,etconsignee,etconsigneer
               String lr_No=etLrNo.getText().toString().trim();
               String Date=date.getText().toString();
               String vehNo=etVehNo.getText().toString();
               String consigneer=etconsigneer.getText().toString();
               String consignee=etconsignee.getText().toString();
               String from=etfrom.getText().toString();
               String to=etto.getText().toString();
               String desc=etdesc.getText().toString();
               String mType=etmtype.getText().toString();
               String invoice=etinvoice.getText().toString();
               String weight=etweigh.getText().toString();
               String charge=etcharge.getText().toString();
               String rate=etRate.getText().toString();
               String value=etvalue.getText().toString();
               String remark=etRemark.getText().toString();
               String vType=tvvehicletype.getText().toString();
               String pType=payment.getText().toString();
               if (lr_No.isEmpty()&&Date.equalsIgnoreCase("Date") &&vType.equalsIgnoreCase("Vehicle Type")&&vehNo.isEmpty()&&consigneer.isEmpty()&&consignee.isEmpty()&&from.isEmpty()&&to.isEmpty()&&desc.isEmpty()&&mType.isEmpty()&&
               invoice.isEmpty()&&weight.isEmpty()&&charge.isEmpty()&&rate.isEmpty()&&value.isEmpty()
                      &&pType.equalsIgnoreCase("Payment Type"))
                   Toast.makeText(RLManagement.this, "Fill All Details", Toast.LENGTH_SHORT).show();
               else if (lr_No.isEmpty())
                   Toast.makeText(RLManagement.this, "Enter LR Number", Toast.LENGTH_SHORT).show();
               else if (Date.equalsIgnoreCase("Date"))
                   Toast.makeText(RLManagement.this, "Choose Date", Toast.LENGTH_SHORT).show();
               else if (vType.equalsIgnoreCase("Vehicle Type"))
                   Toast.makeText(RLManagement.this, "Choose Vehicle Type", Toast.LENGTH_SHORT).show();
               else if (vehNo.isEmpty())
                   Toast.makeText(RLManagement.this, "Enter Vehicle No", Toast.LENGTH_SHORT).show();
               else if (consigneer.isEmpty()) Toast.makeText(RLManagement.this, "Enter Consigneer Address", Toast.LENGTH_SHORT).show();
               else if (consignee.isEmpty()) Toast.makeText(RLManagement.this, "Enter Consignee Address", Toast.LENGTH_SHORT).show();
        else if (from.isEmpty())                   Toast.makeText(RLManagement.this, "Enter Source Point", Toast.LENGTH_SHORT).show();
else if (to.isEmpty())                   Toast.makeText(RLManagement.this, "Enter Destination", Toast.LENGTH_SHORT).show();
else if (desc.isEmpty())                   Toast.makeText(RLManagement.this, "Enter Material Description", Toast.LENGTH_SHORT).show();
else if (mType.isEmpty())                   Toast.makeText(RLManagement.this, "Enter Material Type", Toast.LENGTH_SHORT).show();
else if (invoice.isEmpty())                   Toast.makeText(RLManagement.this, "Enter Invoice Number", Toast.LENGTH_SHORT).show();
else if (weight.isEmpty())                   Toast.makeText(RLManagement.this, "Enter Weight in Kg", Toast.LENGTH_SHORT).show();
else if (charge.isEmpty())                   Toast.makeText(RLManagement.this, "Enter Charge", Toast.LENGTH_SHORT).show();
else if (rate.isEmpty())                   Toast.makeText(RLManagement.this, "Enter Rate/Kg", Toast.LENGTH_SHORT).show();
else if (value.isEmpty())                   Toast.makeText(RLManagement.this, "Enter Value of Goods", Toast.LENGTH_SHORT).show();
else if (pType.equalsIgnoreCase("Payment Type")) Toast.makeText(RLManagement.this, "Choose Payment Type", Toast.LENGTH_SHORT).show();
       else {
                   final  Dialog progress=new Dialog(RLManagement.this);
                   progress.setContentView(R.layout.loadingdialog);
                   progress.setCancelable(false);
                   progress.show();
                   UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                   Call<AddLRResponse>call=userInterface.addLR(lr_No,Date,typecode,consigneer,consignee
                   ,from,to,desc,mType,invoice,weight,charge,rate,value,paymentCode,remark,vehNo);
                   call.enqueue(new Callback<AddLRResponse>() {
                       @Override
                       public void onResponse(Call<AddLRResponse> call, Response<AddLRResponse> response) {
                           if (response.code()==200)
                           { progress.dismiss();
                               Toast.makeText(RLManagement.this, "L-R Added", Toast.LENGTH_SHORT).show();
                               finish(); }
                           else { progress.dismiss();
                               Toast.makeText(RLManagement.this, "L-R Not Added", Toast.LENGTH_SHORT).show(); }
                       }
                       @Override
                       public void onFailure(Call<AddLRResponse> call, Throwable t) {
                           Toast.makeText(RLManagement.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                       }
                   });

       }
           }
       });
    }
    void init()
    {
        etVehNo=findViewById(R.id.etVehNo);
        etLrNo=findViewById(R.id.etLrNo);
        submit=findViewById(R.id.toolsubmit);
        etconsigneer=findViewById(R.id.etconsigneer);
        etconsignee=findViewById(R.id.etconsignee);
        etfrom=findViewById(R.id.etfrom);
        etto=findViewById(R.id.etto);
        etdesc=findViewById(R.id.etdesc);
        etmtype=findViewById(R.id.etmtype);
        etinvoice=findViewById(R.id.etinvoice);
        etweigh=findViewById(R.id.etweigh);
        etcharge=findViewById(R.id.etcharge);
        etRate=findViewById(R.id.etRate);
        etvalue=findViewById(R.id.etvalue);
        etRemark=findViewById(R.id.etRemark);
        tvvehicletype=findViewById(R.id.tvvehicletype);
        myCalendar=Calendar.getInstance();
        payType=new ArrayList<>();
        vehiclelay=findViewById(R.id.vehiclelay);
        tvtoolheader=findViewById(R.id.toolgenheader);
        paymentlay=findViewById(R.id.paymentlay);
        payment=findViewById(R.id.payment);
        date=findViewById(R.id.date);
        datelayout=findViewById(R.id.datelay);
        goBack=findViewById(R.id.toolgoback);
    }
    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        date.setText(sdf.format(myCalendar.getTime()));
    }
}