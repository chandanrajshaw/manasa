package kashyap.chandan.manasa.miscellaneous;

import java.io.Serializable;
import java.util.List;

public class VehicleTypeListResponse implements Serializable {

    /**
     * status : {"code":200,"message":"vehicletype list"}
     * data : [{"id":"1","vehicletype_name":"Van","status":"1"},{"id":"2","vehicletype_name":"Bus","status":"1"},{"id":"3","vehicletype_name":"Scooter","status":"1"},{"id":"4","vehicletype_name":"Trucks","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : vehicletype list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * vehicletype_name : Van
         * status : 1
         */

        private String id;
        private String vehicletype_name;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVehicletype_name() {
            return vehicletype_name;
        }

        public void setVehicletype_name(String vehicletype_name) {
            this.vehicletype_name = vehicletype_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
