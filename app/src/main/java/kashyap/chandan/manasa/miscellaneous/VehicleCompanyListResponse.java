package kashyap.chandan.manasa.miscellaneous;

import java.io.Serializable;
import java.util.List;

public class VehicleCompanyListResponse implements Serializable {
    /**
     * status : {"code":200,"message":"vehiclecompany list"}
     * data : [{"id":"1","vehiclecompany_name":"Tata","status":"1"},{"id":"2","vehiclecompany_name":"Bajaj","status":"1"},{"id":"3","vehiclecompany_name":"Bharat-Benz","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable{
        /**
         * code : 200
         * message : vehiclecompany list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable{
        /**
         * id : 1
         * vehiclecompany_name : Tata
         * status : 1
         */

        private String id;
        private String vehiclecompany_name;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getVehiclecompany_name() {
            return vehiclecompany_name;
        }

        public void setVehiclecompany_name(String vehiclecompany_name) {
            this.vehiclecompany_name = vehiclecompany_name;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
