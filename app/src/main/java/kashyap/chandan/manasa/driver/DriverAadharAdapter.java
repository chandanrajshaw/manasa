package kashyap.chandan.manasa.driver;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.R;

public class DriverAadharAdapter extends RecyclerView.Adapter<DriverAadharAdapter.MyViewHolder> {
    Context context;
    String[] images;
    public DriverAadharAdapter(Context context, String[] images) {
        this.context=context;
        this.images = images;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.rec_image,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        Picasso.get().load(ApiClient.IMAGEURL + images[position]).placeholder(R.drawable.loading).into(holder.image);
holder.image.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        int pos=holder.getAdapterPosition();
        final Dialog dialog=new Dialog(context);
        Activity activity=(Activity)context;
        if(!activity.isFinishing()) {
//show dialog
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
            dialog.setContentView(R.layout.imagedialog);
            dialog.setCancelable(false);
            ImageView image=dialog.findViewById(R.id.dialogImage);
            ImageView close=dialog.findViewById(R.id.cancel);
            Picasso.get().load(ApiClient.IMAGEURL+ images[pos]).placeholder(R.drawable.loading).into(image);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }
});
    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.imageView);
        }
    }
}
