package kashyap.chandan.manasa.driver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.R;

class DriverListAdapter extends RecyclerView.Adapter<DriverListAdapter.MyViewHolder> {
    Context context;
    List<DriverListResponse.DataBean> drivers;
    public DriverListAdapter(Context context, List<DriverListResponse.DataBean> drivers) {
        this.context=context;
        this.drivers=drivers;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.driverlist,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
holder.detail.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(context,EditDriver.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("Driver",drivers.get(position));
        intent.putExtra("bundle",bundle);
        context.startActivity(intent);
    }
});
        Picasso.get().load(ApiClient.IMAGEURL +drivers.get(position).getPimage())
                .placeholder(R.drawable.loading).error(R.drawable.terms).into(holder.driverimage);
holder.tvname.setText(drivers.get(position).getDriver_name());
holder.date.setText(drivers.get(position).getJoin_date());
holder.phone.setText(drivers.get(position).getPhone_no());
    }

    @Override
    public int getItemCount() {
        return drivers.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout detail;
        CircleImageView driverimage;
        TextView tvname,date, phone;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            driverimage=itemView.findViewById(R.id.driverimage);
            tvname=itemView.findViewById(R.id.tvname);
            date=itemView.findViewById(R.id.date);
            detail=itemView.findViewById(R.id.detail);
            phone =itemView.findViewById(R.id.phone);
        }
    }
}
