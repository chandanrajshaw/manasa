package kashyap.chandan.manasa.driver;

import java.io.Serializable;
import java.util.List;

public class DriverListResponse implements Serializable {


    /**
     * status : {"code":200,"message":"Added driver list"}
     * data : [{"id":"1","driver_name":"Driver 1","phone_no":"1234567890","limage":"upload/1448120608B.jpg","account_no":"2147483647","ifsc_code":"<p>Driver 1<\/p>\r\n","join_date":"29-07-2020","branch_id":"1","comment":"<p>Driver 1<\/p>\r\n","pimage":"upload/14169790141622618021srikanth_sir.jpg","aimage":"upload/8582971691.jpg","status":"1","branches_name":"New-delhi"},{"id":"25","driver_name":"vishal driver","phone_no":"34343434","limage":"upload/1493235647i.jpg","account_no":"34434","ifsc_code":"4343434","join_date":"18-08-2020","branch_id":"1","comment":"<p>4343434<\/p>\r\n","pimage":"upload/2027492347images_(2).jpg","aimage":"upload/993455433i.jpg","status":"1","branches_name":"New-delhi"},{"id":"26","driver_name":"vvvvvvv1111","phone_no":"vvvvvvv1111","limage":"upload/477739145images_(2).jpg","account_no":"1111","ifsc_code":"vvvvvv","join_date":"12-10-2020","branch_id":"1","comment":"vsdfsdf","pimage":"upload/1803530276images_(2).jpg","aimage":"upload/534786983images_(3).jpg","status":"1","branches_name":"New-delhi"},{"id":"2","driver_name":"chandan","phone_no":"7209794982","limage":"upload/1303532402r.jpg","account_no":"2147483647","ifsc_code":"abcdtest","join_date":"29/07/2020","branch_id":"2","comment":"test","pimage":"upload/1993342905IMG_20200621_234217.jpg","aimage":"upload/1534730444MyImage.jpg","status":"1","branches_name":"Hydrabad"},{"id":"3","driver_name":"driver 3","phone_no":"4343434","limage":"upload/395856508g.jpg","account_no":"2147483647","ifsc_code":"<p>driver 3<\/p>\r\n","join_date":"29-07-2020","branch_id":"2","comment":"<p>driver 3<\/p>\r\n","pimage":"upload/360092344images_(2).jpg","aimage":"upload/2027119802driverPic.jpg","status":"1","branches_name":"Hydrabad"},{"id":"4","driver_name":"Chandan","phone_no":"1234567988","limage":"upload/405200337d.jpg","account_no":"2147483647","ifsc_code":"test driver","join_date":"31/07/2020","branch_id":"2","comment":"test","pimage":"upload/2027119802driverPic.jpg","aimage":"upload/1094141582I.jpg","status":"1","branches_name":"Hydrabad"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : Added driver list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * driver_name : Driver 1
         * phone_no : 1234567890
         * limage : upload/1448120608B.jpg
         * account_no : 2147483647
         * ifsc_code : <p>Driver 1</p>
         * join_date : 29-07-2020
         * branch_id : 1
         * comment : <p>Driver 1</p>
         * pimage : upload/14169790141622618021srikanth_sir.jpg
         * aimage : upload/8582971691.jpg
         * status : 1
         * branches_name : New-delhi
         */

        private String id;
        private String driver_name;
        private String phone_no;
        private String limage;
        private String account_no;
        private String ifsc_code;
        private String join_date;
        private String branch_id;
        private String comment;
        private String pimage;
        private String aimage;
        private String status;
        private String branches_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDriver_name() {
            return driver_name;
        }

        public void setDriver_name(String driver_name) {
            this.driver_name = driver_name;
        }

        public String getPhone_no() {
            return phone_no;
        }

        public void setPhone_no(String phone_no) {
            this.phone_no = phone_no;
        }

        public String getLimage() {
            return limage;
        }

        public void setLimage(String limage) {
            this.limage = limage;
        }

        public String getAccount_no() {
            return account_no;
        }

        public void setAccount_no(String account_no) {
            this.account_no = account_no;
        }

        public String getIfsc_code() {
            return ifsc_code;
        }

        public void setIfsc_code(String ifsc_code) {
            this.ifsc_code = ifsc_code;
        }

        public String getJoin_date() {
            return join_date;
        }

        public void setJoin_date(String join_date) {
            this.join_date = join_date;
        }

        public String getBranch_id() {
            return branch_id;
        }

        public void setBranch_id(String branch_id) {
            this.branch_id = branch_id;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getPimage() {
            return pimage;
        }

        public void setPimage(String pimage) {
            this.pimage = pimage;
        }

        public String getAimage() {
            return aimage;
        }

        public void setAimage(String aimage) {
            this.aimage = aimage;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getBranches_name() {
            return branches_name;
        }

        public void setBranches_name(String branches_name) {
            this.branches_name = branches_name;
        }
    }
}
