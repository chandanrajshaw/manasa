package kashyap.chandan.manasa.driver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.UserInterface.*;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverList extends AppCompatActivity {
    RecyclerView driverList;
    TextView toolHeader,submit;
    ImageView goBack;
    FloatingActionButton addDriver;
    List<DriverListResponse.DataBean> drivers=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_driver_list);
        init();
        final Dialog progressDialog=new Dialog(DriverList.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        submit.setVisibility(View.GONE);
        toolHeader.setText("Drivers");
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        addDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(DriverList.this, DriverManagement.class);
                startActivity(intent);
            }
        });
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<DriverListResponse>call=userInterface.driverList();
        call.enqueue(new Callback<DriverListResponse>() {
            @Override
            public void onResponse(Call<DriverListResponse> call, Response<DriverListResponse> response) {
                if (response.code()==200)
                {
                   drivers=response.body().getData();
                    driverList.setLayoutManager(new LinearLayoutManager(DriverList.this,LinearLayoutManager.VERTICAL,false));
                    driverList.setAdapter(new DriverListAdapter(DriverList.this,drivers));
                    progressDialog.dismiss();
                }
                else
                    {
                    progressDialog.dismiss();
                        Snackbar.make(DriverList.this.getWindow().getDecorView().findViewById(android.R.id.content),"No Driver available",Snackbar.LENGTH_LONG).show();
                    }
            }

            @Override
            public void onFailure(Call<DriverListResponse> call, Throwable t) {

            }
        });

    }
    void init()
    {
        driverList =findViewById(R.id.driverList);
        goBack=findViewById(R.id.toolgoback);
        addDriver =findViewById(R.id.addDriver);
        toolHeader=findViewById(R.id.toolgenheader);
        submit=findViewById(R.id.toolsubmit);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Dialog progressDialog=new Dialog(DriverList.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<DriverListResponse>call=userInterface.driverList();
        call.enqueue(new Callback<DriverListResponse>() {
            @Override
            public void onResponse(Call<DriverListResponse> call, Response<DriverListResponse> response) {
                if (response.code()==200)
                {
                    drivers=response.body().getData();
                    driverList.setLayoutManager(new LinearLayoutManager(DriverList.this,LinearLayoutManager.VERTICAL,false));
                    driverList.setAdapter(new DriverListAdapter(DriverList.this,drivers));
                    progressDialog.dismiss();
                }
                else
                {
                    progressDialog.dismiss();
                    Snackbar.make(DriverList.this.getWindow().getDecorView().findViewById(android.R.id.content),"Driver List Not available",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<DriverListResponse> call, Throwable t) {

            }
        });

    }
}
