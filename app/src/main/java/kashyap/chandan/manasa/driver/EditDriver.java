package kashyap.chandan.manasa.driver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.StatusAdapter;
import kashyap.chandan.manasa.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditDriver extends AppCompatActivity {
    ArrayList<String> status=new ArrayList<>();
    RelativeLayout statuslay;
    Dialog statusDialog;
    TextView liscense,ifsc_Code,tvstatus,toolgenheader,driverName,branchName,mobileNumber,aadharNumber,date, account_No,submit;
    RecyclerView recyclerstatus,recAadharImage,recLiscenseImage;
    ImageView goBack, showAadharImage, showLiscenseImage;
    CircleImageView profilePic;
    DriverListResponse.DataBean driver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_edit_driver);
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("bundle");
        driver= (DriverListResponse.DataBean) bundle.getSerializable("Driver");
        status.add("Inactive");status.add("Active");
        init();
        toolgenheader.setText(" Edit Driver Detail");
        String aadharImg=driver.getAimage();
        String liscenseImg=driver.getLimage();
driverName.setText(driver.getDriver_name());
mobileNumber.setText(driver.getPhone_no());
branchName.setText(driver.getBranches_name());
date.setText(driver.getJoin_date());
account_No.setText(driver.getAccount_no());
ifsc_Code.setText(driver.getIfsc_code());
int varstatus= Integer.parseInt(driver.getStatus());
if (varstatus==1) tvstatus.setText("Active");
else tvstatus.setText("Inactive");
        Picasso.get().load(ApiClient.IMAGEURL+driver.getPimage())
                .placeholder(R.drawable.loading).error(R.drawable.terms).into(profilePic);
        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog=new Dialog(EditDriver.this);
                dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT);
                dialog.setContentView(R.layout.imagedialog);
                dialog.setCancelable(false);
                ImageView image=dialog.findViewById(R.id.dialogImage);
                ImageView close=dialog.findViewById(R.id.cancel);
                Picasso.get().load(ApiClient.IMAGEURL+driver.getPimage()).placeholder(R.drawable.loading).into(image);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
showAadharImage.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        final Dialog dialog=new Dialog(EditDriver.this);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        dialog.setContentView(R.layout.imagedialog);
        dialog.setCancelable(false);
        ImageView image=dialog.findViewById(R.id.dialogImage);
        ImageView close=dialog.findViewById(R.id.cancel);
        Picasso.get().load(ApiClient.IMAGEURL+driver.getAimage()).placeholder(R.drawable.loading).into(image);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
});
showLiscenseImage.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        final Dialog dialog=new Dialog(EditDriver.this);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        dialog.setContentView(R.layout.imagedialog);
        dialog.setCancelable(false);
        ImageView image=dialog.findViewById(R.id.dialogImage);
        ImageView close=dialog.findViewById(R.id.cancel);
        Picasso.get().load(ApiClient.IMAGEURL+driver.getLimage()).placeholder(R.drawable.loading).into(image);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
});

        statuslay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statusDialog.setContentView(R.layout.recyclerdialog);
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                int width = metrics.widthPixels;
                statusDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                recyclerstatus = statusDialog.findViewById(R.id.recycleroption);
                recyclerstatus.setLayoutManager(new LinearLayoutManager(EditDriver.this, LinearLayoutManager.VERTICAL, false));
                recyclerstatus.setAdapter(new StatusAdapter(EditDriver.this,status, statusDialog,new CustomItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position, String value) {
                        tvstatus.setText(value);
                    }
                }));
                statusDialog.show();
            }
        });
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
       submit.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               String status=tvstatus.getText().toString().equalsIgnoreCase("Active")?"1":"0";
               String id=driver.getId();
               if (tvstatus.getText().toString().equalsIgnoreCase("Status"))
                   Toast.makeText(EditDriver.this, "Select Status", Toast.LENGTH_SHORT).show();
               else {
                       final Dialog progressDialog=new Dialog(EditDriver.this);
                       progressDialog.setContentView(R.layout.loadingdialog);
                       progressDialog.setCancelable(false);
                       progressDialog.show();
                       UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                       Call<EditDriverResponse>call=userInterface.editDriver(id,status);
                       call.enqueue(new Callback<EditDriverResponse>() {
                           @Override
                           public void onResponse(Call<EditDriverResponse> call, Response<EditDriverResponse> response) {
                               if (response.code()==200)
                               {
                                   progressDialog.dismiss();
                                   Toast.makeText(EditDriver.this, "Edited Successfully ", Toast.LENGTH_SHORT).show();
                                   overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                                   finish();
                               }
                               else {
                                   progressDialog.dismiss();
                                   Toast.makeText(EditDriver.this, "Edit Unsuccessfull ", Toast.LENGTH_SHORT).show();
                               }
                           }

                           @Override
                           public void onFailure(Call<EditDriverResponse> call, Throwable t) {
                               progressDialog.dismiss();
                               Toast.makeText(EditDriver.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                           }
                       });
               }
           }
       });
    }
    public void init()
    {
        liscense=findViewById(R.id.liscense);
        showLiscenseImage =findViewById(R.id.showLiscenseImage);

        ifsc_Code=findViewById(R.id.ifsc_Code);
        submit=findViewById(R.id.toolsubmit);
        profilePic=findViewById(R.id.driverPic);
        driverName=findViewById(R.id.driverName);
        branchName=findViewById(R.id.branchName);
        mobileNumber=findViewById(R.id.mobileNumber);
        aadharNumber=findViewById(R.id.aadharNumber);
        date=findViewById(R.id.date);
        account_No =findViewById(R.id.account_No);
        statuslay=findViewById(R.id.statuslay);
        statusDialog=new Dialog(EditDriver.this);
        tvstatus=findViewById(R.id.status);
        goBack=findViewById(R.id.toolgoback);
        toolgenheader=findViewById(R.id.toolgenheader);
        showAadharImage=findViewById(R.id.showAadharImage);
    }
}
