package kashyap.chandan.manasa.driver;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.ActionBar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.RealPathUtil;
import kashyap.chandan.manasa.StatusAdapter;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.adapters.BranchAdapter;
import kashyap.chandan.manasa.valuespart.DropDownBranchResponse;
import kashyap.chandan.manasa.vehicle.MyImageAdapter;
import kashyap.chandan.manasa.vehicle.VehicleList;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverManagement extends AppCompatActivity {
final int CHOOSE_AADHAR_PDF=1,CHOOSE_LISCENCE_PDF=1000;
    String aadharPath,liscensePath,branchcode,statuscode;
    TextView chooseLiscenseImage,liscensepdfpath,chooseLiscencepdf,chooseAadharPdf, chooseAadharImage, aadharPdfpath,date,toolgenheader,toolsubmit,tvBranch;
    LinearLayout pdffile,imagefile;
    RelativeLayout datelayout,statuslay,branchlay;
    RadioGroup filegrp;
    ImageView goBack,driverProfilepic,Liscenceimages,aadharimage;
    FrameLayout driverImg;
    RadioButton rdopdf,rdoimage;
    RecyclerView branchRecycler;
    ArrayList<Uri> aadharUriarrayList =new ArrayList<>();
    ArrayList<Uri> LiscenseUriarrayList =new ArrayList<>();
    ArrayList<String>status=new ArrayList<>();
    List<MultipartBody.Part> aadharimageParts = new ArrayList<>();
    List<MultipartBody.Part> liscenseimageParts = new ArrayList<>();
    MultipartBody.Part aadharParts,liscenseParts;
    Uri aadharImageUri,liscenseImageUri;
    private int REQUEST_CODE_CHOOSE_AIMAGE =102,REQUEST_CODE_CHOOSE_LIMAGE =1001;
    DatePickerDialog.OnDateSetListener dateDialog;
    Dialog statusDialog,cameradialog;
    TextView tvstatus;
    RecyclerView recyclerstatus;
    Calendar myCalendar;
    /*-----------profile Image-******************/
    String picturePath,imagePic;
    Bitmap bitmap,converetdImage,aadharConvertedImage,liscenseConvertedImage;
    Uri picUri;
    File image=null;
    int permissionCounter=0;
    /*---------------EditText-----------------------*/
    TextInputEditText etfName, etAccNo,etPhone, etIFSCcode,etAddress,etComment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_driver_management);
        status.add("Inactive");status.add("Active");
        init();
        toolgenheader.setText("Driver Management");

//        filegrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup radioGroup, int i) {
//                if (rdopdf.getId()==i) {
//                    aadharimageParts.clear();
//                    liscenseimageParts.clear();
//                    aadharUriarrayList.clear();
//                    LiscenseUriarrayList.clear();
////                    aadharimage.removeAllViews();
////                    LiscenceimagesRecycler.removeAllViews();
//                    pdffile.setVisibility(View.VISIBLE);
//                    imagefile.setVisibility(View.GONE);
//                }
//                if (rdoimage.getId()==i) {
//                    aadharpdfParts =null;
//                    aadharPdfUri =null;
//                    aadharPdfpath.setText("");
//                    aadharpdfParts =null;
//                    liscensePdfUri =null;
//                    liscensepdfpath.setText("");
//                    pdffile.setVisibility(View.GONE);
//                    imagefile.setVisibility(View.VISIBLE);
//                }
//            }
//        });
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        chooseAadharImage.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(DriverManagement.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED&&
                        ContextCompat.checkSelfPermission(DriverManagement.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED) {
                    askPermission();
                }
                else {
//                    aadharimageParts.clear();
//                    aadharUriarrayList.clear();
//                    aadharimageRecycler.removeAllViews();
                    showChooser(REQUEST_CODE_CHOOSE_AIMAGE);
                }
            }
        });
//        chooseAadharPdf.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.M)
//            @Override
//            public void onClick(View view) {
//                if (ContextCompat.checkSelfPermission(DriverManagement.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED&&
//                        ContextCompat.checkSelfPermission(DriverManagement.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED) {
//                    askPermission();
//                }
//                else {
//                    aadharpdfParts =null;
//                    aadharPdfUri =null;
//                    aadharPdfpath.setText("");
//                    showPdfChooser(CHOOSE_AADHAR_PDF);
//                }
//            }
//        });
//        chooseLiscencepdf.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.M)
//            @Override
//            public void onClick(View view) {
//                if (ContextCompat.checkSelfPermission(DriverManagement.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED&&
//                        ContextCompat.checkSelfPermission(DriverManagement.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED) {
//                    askPermission();
//                }
//                else {
//
//                    liscensepdfParts =null;
//                    liscensePdfUri =null;
//                    liscensepdfpath.setText("");
//                    showPdfChooser(CHOOSE_LISCENCE_PDF);
//                }
//            }
//        });
        chooseLiscenseImage.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(DriverManagement.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED&&
                        ContextCompat.checkSelfPermission(DriverManagement.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED) {
                    askPermission();
                }
                else {
//                    liscenseimageParts.clear();
//                    LiscenseUriarrayList.clear();
//                    LiscenceimagesRecycler.removeAllViews();
                    showChooser(REQUEST_CODE_CHOOSE_LIMAGE);
                }
            }
        });
        statuslay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                statusDialog.setContentView(R.layout.recyclerdialog);
                DisplayMetrics metrics = getResources().getDisplayMetrics();
                int width = metrics.widthPixels;
                statusDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                recyclerstatus = statusDialog.findViewById(R.id.recycleroption);
                recyclerstatus.setLayoutManager(new LinearLayoutManager(DriverManagement.this, LinearLayoutManager.VERTICAL, false));
                recyclerstatus.setAdapter(new StatusAdapter(DriverManagement.this,status, statusDialog,new CustomItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position, String value) {
                        tvstatus.setText(value);
                        statuscode= String.valueOf(position);
                    }
                }));
                statusDialog.show();
            }
        });
        branchlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final  Dialog progress=new Dialog(DriverManagement.this);
                progress.setContentView(R.layout.loadingdialog);
                progress.setCancelable(false);
                progress.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<DropDownBranchResponse> call=userInterface.dropDownBranchList();
                call.enqueue(new Callback<DropDownBranchResponse>() {
                    @Override
                    public void onResponse(Call<DropDownBranchResponse> call, Response<DropDownBranchResponse> response) {
                        if (response.code()==200)
                        {
                            progress.dismiss();
                            final Dialog branchDialog=new Dialog(DriverManagement.this);
                            branchDialog.setContentView(R.layout.recyclerdialog);
                            branchDialog.setCancelable(true);
                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                            int width = metrics.widthPixels;
                            branchRecycler=branchDialog.findViewById(R.id.recycleroption);
                            branchDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            List<DropDownBranchResponse.DataBean> branch=new ArrayList<>();
                            branch=response.body().getData();
                            branchRecycler.setLayoutManager(new LinearLayoutManager(DriverManagement.this,LinearLayoutManager.VERTICAL,false));
                            branchRecycler.setAdapter(new BranchAdapter(DriverManagement.this,branch,branchDialog,new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, int position, String value) {
                                    tvBranch.setText(value);
                                    branchcode=String.valueOf(position);
                                }
                            }));
                            branchDialog.show();
                        }
                        else
                        {
                            progress.dismiss();
                            Snackbar.make(DriverManagement.this.getWindow().getDecorView().findViewById(android.R.id.content),"Branch Not available",Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DropDownBranchResponse> call, Throwable t) {
                        Toast.makeText(DriverManagement.this, ""+t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        datelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(DriverManagement.this,R.style.TimePickerTheme,dateDialog, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        dateDialog=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        driverImg.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(DriverManagement.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
                        ContextCompat.checkSelfPermission(DriverManagement.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                ||ContextCompat.checkSelfPermission(DriverManagement.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {

                    askPermission();
                }
                else {

                        ImageView camera, folder;
                    cameradialog = new Dialog(DriverManagement.this);
                    cameradialog.setContentView(R.layout.dialogboxcamera);
                        DisplayMetrics metrics=getResources().getDisplayMetrics();
                        int width=metrics.widthPixels;
                    cameradialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                    cameradialog.show();
                    cameradialog.setCancelable(true);
                    cameradialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        Window window = cameradialog.getWindow();
                        window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
                        camera = cameradialog.findViewById(R.id.camera);
                        folder = cameradialog.findViewById(R.id.gallery);
                        folder.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(i, 100);
                                cameradialog.dismiss();
                            }
                        });
                        camera.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, 101);
                                cameradialog.dismiss();
                            }
                        });
                    }
                }

        });
        toolsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fName=etfName.getText().toString();
                String accountNumber= etAccNo.getText().toString();
                String ifscCode= etIFSCcode.getText().toString();
//                String address=etAddress.getText().toString();
                String joinDate=date.getText().toString();
                String phone=etPhone.getText().toString();
                String br=branchcode;
                String status=statuscode;
                String comment=etComment.getText().toString().trim();
                System.out.println(ifscCode);
                if (image==null&&fName.isEmpty()&&accountNumber.isEmpty()&&phone.length()!=10 && phone.isEmpty()&&ifscCode.isEmpty()
//                        &&address.isEmpty()
                &&joinDate.equalsIgnoreCase("Date")&&tvBranch.getText().toString().equalsIgnoreCase("Branch")
                &&tvstatus.getText().toString().equalsIgnoreCase("status"))
                    Toast.makeText(DriverManagement.this, "Please Fill All the Fields", Toast.LENGTH_SHORT).show();
              else if (image==null)
                    Toast.makeText(DriverManagement.this, "Select Profile Pic", Toast.LENGTH_SHORT).show();
                else if (fName.isEmpty())
                    Toast.makeText(DriverManagement.this, "Enter First Name", Toast.LENGTH_SHORT).show();

                else if (phone.isEmpty()||phone.length()!=10)
                    Toast.makeText(DriverManagement.this, "Enter Valid Phone Number", Toast.LENGTH_SHORT).show();
                else if (accountNumber.isEmpty())
                    Toast.makeText(DriverManagement.this, "Enter Account Number", Toast.LENGTH_SHORT).show();
                else if (ifscCode.isEmpty())
                    Toast.makeText(DriverManagement.this, "Enter IFSC Code", Toast.LENGTH_SHORT).show();
                else if (joinDate.equalsIgnoreCase("Date"))
                    Toast.makeText(DriverManagement.this, "Select Date", Toast.LENGTH_SHORT).show();
                else if (tvBranch.getText().toString().equalsIgnoreCase("Branch"))
                    Toast.makeText(DriverManagement.this, "Select Branch", Toast.LENGTH_SHORT).show();
                else if (tvstatus.getText().toString().equalsIgnoreCase("status"))
                    Toast.makeText(DriverManagement.this, "Select Status", Toast.LENGTH_SHORT).show();
                else {
                    UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
                    RequestBody dfname = RequestBody.create(MediaType.parse("multipart/form-data"), fName);
                    RequestBody dAccountNumber = RequestBody.create(MediaType.parse("multipart/form-data"), accountNumber);
                    RequestBody dphone = RequestBody.create(MediaType.parse("multipart/form-data"),phone);
                    RequestBody dIFSCcode = RequestBody.create(MediaType.parse("multipart/form-data"), ifscCode);
                    System.out.println(ifscCode);
//                    RequestBody daddress = RequestBody.create(MediaType.parse("multipart/form-data"), address);
                    RequestBody dDate = RequestBody.create(MediaType.parse("multipart/form-data"), joinDate);
                    RequestBody dbranch = RequestBody.create(MediaType.parse("multipart/form-data"), br);
                    RequestBody dstatus = RequestBody.create(MediaType.parse("multipart/form-data"),status);
                    RequestBody dcmnt = RequestBody.create(MediaType.parse("multipart/form-data"),comment);
                    MultipartBody.Part dpic=null;
                    if (image != null) {
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), image);
                        dpic = MultipartBody.Part.createFormData("pimage", image.getName(), requestFile);
                    }
//                    int ide=filegrp.getCheckedRadioButtonId();
//                    if (ide==R.id.selectpdf)
//                    {
//                        MultipartBody.Part body=null,body1=null;
//                        if (aadharPdfUri == null ||liscensePdfUri == null)
//                        {
//                            Toast.makeText(DriverManagement.this, "File Not Available for Upload", Toast.LENGTH_SHORT).show();
//                        }
//                        else {
//                            if (aadharPdfUri !=null&&liscensePdfUri!=null){
//                                aadharpdfParts =prepareFilePart("aadhar_doc", aadharPdfUri);
//                                liscensepdfParts =prepareFilePart("liscence_doc", liscensePdfUri);
//                                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
//                                body = MultipartBody.Part.createFormData("aimage[]", "", requestFile);
//                                aadharimageParts.add(body);
//                                RequestBody requestFileliscense = RequestBody.create(MediaType.parse("multipart/form-data"), "");
//                                body1 = MultipartBody.Part.createFormData("limage[]", "", requestFileliscense);
//                                liscenseimageParts.add(body1);
//                            }
//                            final Dialog progressDialog=new Dialog(DriverManagement.this);
//                            progressDialog.setContentView(R.layout.loadingdialog);
//                            progressDialog.setCancelable(false);
//                            progressDialog.show();
//                            Call<AddDriverResponse>call=userInterface.addDriver(dfname,dAccountNumber,dphone,dIFSCcode/*,daddress*/,dDate,dbranch,
//                                    dcmnt,dstatus,dpic,aadharpdfParts,liscensepdfParts, aadharimageParts,liscenseimageParts);
//                            call.enqueue(new Callback<AddDriverResponse>() {
//                                @Override
//                                public void onResponse(Call<AddDriverResponse> call, Response<AddDriverResponse> response) {
//                                    if (response.code()==200)
//                                    {
//                                        Toast.makeText(DriverManagement.this, "Driver Added", Toast.LENGTH_SHORT).show();
//                                        finish();
//                                    }
//                                    else{
//                                        progressDialog.dismiss();
//                                        Toast.makeText(DriverManagement.this, "Driver Not Added", Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//
//                                @Override
//                                public void onFailure(Call<AddDriverResponse> call, Throwable t) {
//                                    progressDialog.dismiss();
//                                    Toast.makeText(DriverManagement.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
//                                }
//                            });
//                        }
//
//                    }
//                    else if (ide==R.id.selectimage)

                        if (aadharImageUri==null ||liscenseImageUri==null)
                        {
                            Toast.makeText(DriverManagement.this, "Files Not Found", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            aadharParts=prepareFilePart("aimage",aadharImageUri);
                            liscenseParts=prepareFilePart("limage",liscenseImageUri);
//                            if (!(aadharUriarrayList.isEmpty()&&LiscenseUriarrayList.isEmpty())) {
//                                for (int i = 0; i < aadharUriarrayList.size(); i++) {
//                                    aadharimageParts.add(prepareFilePart(" aimage[]", aadharUriarrayList.get(i)));    //here image is
//                                }
//                                for (int i = 0; i < LiscenseUriarrayList.size(); i++) {
//                                    liscenseimageParts.add(prepareFilePart(" limage[]", aadharUriarrayList.get(i)));    //here image is
//                                }
//                                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
//                                aadharpdfParts = MultipartBody.Part.createFormData("aadhar_doc", "", requestFile);
//                                RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), "");
//                                aadharpdfParts = MultipartBody.Part.createFormData("liscence_doc", "", requestFile1);
//                            }

                            final Dialog progressDialog=new Dialog(DriverManagement.this);
                            progressDialog.setContentView(R.layout.loadingdialog);
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            Call<AddDriverResponse>call=userInterface.addDriver(dfname,dAccountNumber,dphone,dIFSCcode/*,daddress*/,dDate,dbranch,
                                    dcmnt,dstatus,dpic, aadharParts,liscenseParts);
                            call.enqueue(new Callback<AddDriverResponse>() {
                                @Override
                                public void onResponse(Call<AddDriverResponse> call, Response<AddDriverResponse> response) {
                                    if (response.code()==200)
                                    {
                                        progressDialog.dismiss();
                                        Toast.makeText(DriverManagement.this, "Driver Added", Toast.LENGTH_SHORT).show();
                                        Intent intent=new Intent(DriverManagement.this, VehicleList.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        finish();
                                    }
                                    else {
                                        progressDialog.dismiss();
                                        Toast.makeText(DriverManagement.this, "Driver Not Added", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<AddDriverResponse> call, Throwable t) {
                                    progressDialog.dismiss();
                                    Toast.makeText(DriverManagement.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }

//                    else {
//                        Toast.makeText(DriverManagement.this, "Choose Document Type to upload", Toast.LENGTH_SHORT).show();
//                    }
                }
            }
        });
    }
    private void init() {
        chooseLiscenseImage=findViewById(R.id.chooseLiscenseImage);
        Liscenceimages=findViewById(R.id.Liscenceimages);
//        liscensepdfpath=findViewById(R.id.liscensepdfpath);
//        chooseLiscencepdf=findViewById(R.id.chooseLiscencepdf);
        etComment=findViewById(R.id.etComment);
//        etAddress=findViewById(R.id.etAddress);
        etfName=findViewById(R.id.etfName);
        etAccNo =findViewById(R.id.etAccNo);
        etPhone=findViewById(R.id.etPhone);
        etIFSCcode =findViewById(R.id.etIFSCcode);
        tvBranch=findViewById(R.id.branch);
        branchlay=findViewById(R.id.branchlay);
        driverImg=findViewById(R.id.driverImg);
        driverProfilepic=findViewById(R.id.driverProfilePic);
        pdffile=findViewById(R.id.pdffile);
        imagefile=findViewById(R.id.imagefile);
//        rdopdf=findViewById(R.id.selectpdf);
//        aadharPdfpath =findViewById(R.id.aadharpdfpath);
//        rdoimage=findViewById(R.id.selectimage);
        aadharimage =findViewById(R.id.aadharimage);
//        filegrp=findViewById(R.id.aadhargrp);
        chooseAadharImage =findViewById(R.id.chooseAadharImage);
//        chooseAadharPdf =findViewById(R.id.chooseAadharpdf);
        datelayout=findViewById(R.id.datelay);
        date=findViewById(R.id.date);
        myCalendar = Calendar.getInstance();
        toolgenheader=findViewById(R.id.toolgenheader);
        toolsubmit=findViewById(R.id.toolsubmit);
        statuslay=findViewById(R.id.statuslay);
        tvstatus=findViewById(R.id.status);
        statusDialog=new Dialog(DriverManagement.this);
        goBack=findViewById(R.id.toolgoback);
    }
    private void showChooser(int REQUEST_CODE) {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, REQUEST_CODE);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == REQUEST_CODE_CHOOSE_AIMAGE)
            {
                Uri selectedImage = resultData.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    aadharImageUri=selectedImage;
                    aadharConvertedImage = getResizedBitmap(bitmap, 500);
                    aadharimage.setImageBitmap(aadharConvertedImage);
                    aadharimage.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (requestCode == REQUEST_CODE_CHOOSE_LIMAGE)
            {
                Uri selectedImage = resultData.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    liscenseImageUri=selectedImage;
                    liscenseConvertedImage = getResizedBitmap(bitmap, 500);
                    Liscenceimages.setImageBitmap(liscenseConvertedImage);
                    Liscenceimages.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (requestCode == 100  && resultData != null) {

//the image URI
                Uri selectedImage = resultData.getData();

                //     imagepath=selectedImage.getPath();

                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();


                if (picturePath != null && !picturePath.equals("")) {
                    image = new File(picturePath);
                }

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    converetdImage = getResizedBitmap(bitmap, 500);
                    driverProfilepic.setImageBitmap(converetdImage);
                    driverProfilepic.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else if (requestCode == 101 ) {
                Bitmap converetdImage = (Bitmap) resultData.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                driverProfilepic.setImageBitmap(converetdImage);
                driverProfilepic.setVisibility(View.VISIBLE);
                image = new File(Environment.getExternalStorageDirectory(), "driverPic.jpg");
                FileOutputStream fo;
                try {
                    fo = new FileOutputStream(image);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
//            {
//                aadharimageParts.clear();
//                aadharUriarrayList.clear();
//                aadharimageRecycler.removeAllViews();
//                if (resultData != null) {
//                    if (resultData.getClipData() != null) {
//                        int count = resultData.getClipData().getItemCount();
//                        int currentItem = 0;
//                        while (currentItem < 2) {
//                            Uri imageUri = resultData.getClipData().getItemAt(currentItem).getUri();
//                            currentItem = currentItem + 1;
//
//                            Log.d("Uri Selected", imageUri.toString());
//
//                            try {
//                                aadharUriarrayList.add(imageUri);
//                                aadharimageRecycler.setLayoutManager(new LinearLayoutManager(DriverManagement.this,LinearLayoutManager.HORIZONTAL,false));
//                                aadharimageRecycler.setAdapter(new MyImageAdapter(DriverManagement.this, aadharUriarrayList));
//
//                            } catch (Exception e) {
//                                Log.e("TAG", "File select error", e);
//                            }
//                        }
//                    }
//                    else if (resultData.getData() != null) {
//
//                        final Uri uri = resultData.getData();
//                        Log.i("TAG", "Uri = " + uri.toString());
//
//                        try {
//                            aadharUriarrayList.add(uri);
//                            aadharimageRecycler.setLayoutManager(new LinearLayoutManager(DriverManagement.this,LinearLayoutManager.HORIZONTAL,false));
//                            aadharimageRecycler.setAdapter(new MyImageAdapter(DriverManagement.this, aadharUriarrayList));
//
//                        } catch (Exception e) {
//                            Log.e("TAG", "File select error", e);
//                        }
//                    }
//                }
//            }
//            if (requestCode==CHOOSE_AADHAR_PDF) {
//                // Get the Uri of the selected file
//                Uri uri = resultData.getData();
//                String path= RealPathUtil.getRealPath(DriverManagement.this,uri);
//                aadharPath=path;
//                aadharPdfpath.setText(path);
//                aadharPdfUri =uri;
//            }
//            if (requestCode==CHOOSE_LISCENCE_PDF) {
//                // Get the Uri of the selected file
//                Uri uri = resultData.getData();
//                String path= RealPathUtil.getRealPath(DriverManagement.this,uri);
//                liscensePath=path;
//                liscensepdfpath.setText(path);
//                liscensePdfUri =uri;
//            }

//            {
//                liscenseimageParts.clear();
//                LiscenseUriarrayList.clear();
//                LiscenceimagesRecycler.removeAllViews();
//                if (resultData != null) {
//                    if (resultData.getClipData() != null) {
//                        int count = resultData.getClipData().getItemCount();
//                        int currentItem = 0;
//                        while (currentItem < 2) {
//                            Uri imageUri = resultData.getClipData().getItemAt(currentItem).getUri();
//                            currentItem = currentItem + 1;
//
//                            Log.d("Uri Selected", imageUri.toString());
//
//                            try {
//                                LiscenseUriarrayList.add(imageUri);
//                                LiscenceimagesRecycler.setLayoutManager(new LinearLayoutManager(DriverManagement.this,LinearLayoutManager.HORIZONTAL,false));
//                                LiscenceimagesRecycler.setAdapter(new MyImageAdapter(DriverManagement.this, LiscenseUriarrayList));
//
//                            } catch (Exception e) {
//                                Log.e("TAG", "File select error", e);
//                            }
//                        }
//                    }
//                    else if (resultData.getData() != null) {
//
//                        final Uri uri = resultData.getData();
//                        Log.i("TAG", "Uri = " + uri.toString());
//
//                        try {
//                            LiscenseUriarrayList.add(uri);
//                            LiscenceimagesRecycler.setLayoutManager(new LinearLayoutManager(DriverManagement.this,LinearLayoutManager.HORIZONTAL,false));
//                            LiscenceimagesRecycler.setAdapter(new MyImageAdapter(DriverManagement.this, LiscenseUriarrayList));
//
//                        } catch (Exception e) {
//                            Log.e("TAG", "File select error", e);
//                        }
//                    }
//                }
//            }

        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void askPermission() {
        if (ContextCompat.checkSelfPermission(DriverManagement.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(DriverManagement.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
        ||ContextCompat.checkSelfPermission(DriverManagement.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},100);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case 100:
               int length= grantResults.length;
               if (grantResults.length>0)
               {
                   for (int i=0;i<length;i++)
                   {
                       if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                           // Granted. Start getting the location information
                       }
                       else if(grantResults[i]==PackageManager.PERMISSION_DENIED) {
                           {
                               final AlertDialog.Builder builder=new AlertDialog.Builder(DriverManagement.this,R.style.AlertDialogTheme);
                               builder.setTitle("Notice");
                               builder.setMessage("You Had Denied the Necessary Permissions.Please Go to app Setting and give All the permissions");
                               builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                   @Override
                                   public void onClick(DialogInterface dialogInterface, int i) {
                                       dialogInterface.dismiss();
                                       Intent intent=new Intent();
                                       intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                       intent.addCategory(Intent.CATEGORY_DEFAULT);
                                       intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                                       startActivity(intent);
                                   }
                               });
builder.show();
                               break;
                           }

                       }

                   }

               }

                break;
        }
    }
    //    @NonNull
//    private RequestBody createPartFromString(String descriptionString) {
//        return RequestBody.create(MediaType.parse(FileUtils.MIME_TYPE_TEXT), descriptionString);
//    }
    public  void showPdfChooser( int REQUEST_CODE) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        startActivityForResult(intent,REQUEST_CODE);
    }
    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri)  {
        String path = RealPathUtil.getRealPath(DriverManagement.this,fileUri);
        File file = new File(path);
        RequestBody requestFile = RequestBody.create (MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(partName, file.getName(),requestFile);
    }

    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        date.setText(sdf.format(myCalendar.getTime()));
    }
    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
}
