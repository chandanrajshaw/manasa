package kashyap.chandan.manasa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import kashyap.chandan.manasa.login.Login;

public class SplashScreen extends AppCompatActivity {
SharedPreferences sharedPreferences;
String userName,password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sharedPreferences=getSharedPreferences("Login",MODE_PRIVATE);
               userName=sharedPreferences.getString("userName",null);
               password=sharedPreferences.getString("password",null);
               if (userName!=null&&password!=null)
               {
                   Intent  intent=new Intent(SplashScreen.this, MainActivity.class);
                   startActivity(intent);
                   finish();
               }
               else {
                   Intent  intent=new Intent(SplashScreen.this, Login.class);
                   startActivity(intent);
                   finish();
               }
            }
        },2000);
    }
}
