package kashyap.chandan.manasa.login;

import java.io.Serializable;

public class LoginResponse implements Serializable {

    /**
     * status : {"code":200,"message":"welcome admin"}
     * admin_detail : {"username":"Mansa mover","email":"mansa@gmail.com","image":"upload/858008817mansa_cargo.jpeg","mobile":"9709615446"}
     */

    private StatusBean status;
    private AdminDetailBean admin_detail;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public AdminDetailBean getAdmin_detail() {
        return admin_detail;
    }

    public void setAdmin_detail(AdminDetailBean admin_detail) {
        this.admin_detail = admin_detail;
    }

    public static class StatusBean implements Serializable{
        /**
         * code : 200
         * message : welcome admin
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class AdminDetailBean implements Serializable {
        /**
         * username : Mansa mover
         * email : mansa@gmail.com
         * image : upload/858008817mansa_cargo.jpeg
         * mobile : 9709615446
         */

        private String username;
        private String email;
        private String image;
        private String mobile;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }
}
