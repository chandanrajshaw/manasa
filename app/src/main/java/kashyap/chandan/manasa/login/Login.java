package kashyap.chandan.manasa.login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.ApiError;
import kashyap.chandan.manasa.MainActivity;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.expenses.ExpenseList;
import kashyap.chandan.manasa.forgetpassword.ForgetPassword;
import kashyap.chandan.manasa.forgetpassword.SendOtpResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class Login extends AppCompatActivity {
Button login;
TextView forgetPassword;
TextInputEditText etuserName,etPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);
            init();
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String userName=etuserName.getText().toString().trim();
                    final String password=etPassword.getText().toString().trim();
                    if (userName.isEmpty()&&password.isEmpty())
                        Toast.makeText(Login.this, "Enter All Fields", Toast.LENGTH_SHORT).show();
                    else if (userName.isEmpty())
                        Toast.makeText(Login.this, "Enter Email", Toast.LENGTH_SHORT).show();
                    else if (password.isEmpty())
                        Toast.makeText(Login.this, "Enter Password", Toast.LENGTH_SHORT).show();
                    else {
                        final Dialog progressDialog=new Dialog(Login.this);
                        progressDialog.setContentView(R.layout.loadingdialog);
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);

                        Call<LoginResponse>call=userInterface.login(userName,password);
                        call.enqueue(new Callback<LoginResponse>() {
                            @Override
                            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                                if (response.code()==200){
                                    progressDialog.dismiss();
                                    LoginResponse.AdminDetailBean admin=response.body().getAdmin_detail();
                                    SharedPreferences sharedPreferences=getSharedPreferences("Login",MODE_PRIVATE);
                                    SharedPreferences.Editor editor=sharedPreferences.edit();
                                    editor.putString("userName",userName);
                                    editor.putString("password",password);
                                    editor.putString("name",admin.getUsername());
                                    editor.putString("mobile",admin.getMobile());
                                    editor.putString("image",admin.getImage());
                                    editor.commit();
                                    Intent  intent=new Intent(Login.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                else {
                                    progressDialog.dismiss();
                                    Converter<ResponseBody, ApiError> converter =
                                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
            ApiError error;
            try {
                error = converter.convert(response.errorBody());
                ApiError.StatusBean status=error.getStatus();
                Toast.makeText(Login.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
            } catch (IOException e) { e.printStackTrace(); }
                                }
                            }

                            @Override
                            public void onFailure(Call<LoginResponse> call, Throwable t) {
                                progressDialog.dismiss();
                                Toast.makeText(Login.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });

                    }

                }
            });
            forgetPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String email=etuserName.getText().toString().trim();
                    if (email.isEmpty())
                        Toast.makeText(Login.this, "Enter Registered Email", Toast.LENGTH_SHORT).show();
                    else
                        {
                            final Dialog progressDialog=new Dialog(Login.this);
                            progressDialog.setContentView(R.layout.loadingdialog);
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                            Call<SendOtpResponse>call=userInterface.sendOtp(email);
                            call.enqueue(new Callback<SendOtpResponse>() {
                                @Override
                                public void onResponse(Call<SendOtpResponse> call, Response<SendOtpResponse> response) {
                                    if (response.code()==200)
                                    {
                                        progressDialog.dismiss();
                                        Intent intent=new Intent(Login.this, ForgetPassword.class);
                                        startActivity(intent);
                                    }
                                    else
                                    {
                                        progressDialog.dismiss();
                                        Converter<ResponseBody, ApiError> converter =
                                                ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                        ApiError error;
                                        try {
                                            error = converter.convert(response.errorBody());
                                            ApiError.StatusBean status=error.getStatus();
                                            Toast.makeText(Login.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                        } catch (IOException e) { e.printStackTrace(); }
                                    }
                                }

                                @Override
                                public void onFailure(Call<SendOtpResponse> call, Throwable t) {

                                }
                            });
                        }
                }
            });
    }
    void init()
    {
        forgetPassword=findViewById(R.id.forgetPassword);
        login=findViewById(R.id.login);
        etPassword=findViewById(R.id.etPassword);
        etuserName=findViewById(R.id.etuserName);
    }
}
