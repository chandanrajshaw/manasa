package kashyap.chandan.manasa.invoice;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.client.ClientListResponse;

public class ClientAdapter extends RecyclerView.Adapter<ClientAdapter.MyViewHolder> {
    Context context;
    List<ClientListResponse.DataBean> client;
    Dialog clientDialog;
    CustomItemClickListener listener;
    private int mSelectedItem = -1;
    public ClientAdapter(Context context, List<ClientListResponse.DataBean> client, Dialog clientDialog, CustomItemClickListener listener) {
   this.context=context;
   this.client=client;
   this.clientDialog=clientDialog;
   this.listener=listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.option,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == mSelectedItem);
        holder.tvClient.setText(client.get(position).getClient_name());
        TextView ok= clientDialog.findViewById(R.id.ok);
        ok.setVisibility(View.GONE);
        TextView dialogheader=clientDialog.findViewById(R.id.dialogheader);
        dialogheader.setText("Clients");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedItem==-1)
                { Toast.makeText(context, "Select Branch", Toast.LENGTH_SHORT).show(); }
                else
                { clientDialog.dismiss(); }
            }
        });
    }


    @Override
    public int getItemCount() {
        return client.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvClient;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvClient=itemView.findViewById(R.id.item);
            radioselect=itemView.findViewById(R.id.selected);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    listener.onItemClick(v, Integer.parseInt(client.get(mSelectedItem).getId()),client.get(mSelectedItem).getClient_name());
                }

            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}
