package kashyap.chandan.manasa.invoice;

public class CreateInvoiceResponse {


    /**
     * status : {"code":200,"message":"New invoice created"}
     * invoice_id : 13
     */

    private StatusBean status;
    private int invoice_id;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public int getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(int invoice_id) {
        this.invoice_id = invoice_id;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : New invoice created
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
