package kashyap.chandan.manasa.invoice;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.ApiError;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class InvoiceDetail extends AppCompatActivity {
TextView tvtoolheader,invoicesDate, ClientName,submit,tvTotal;
ImageView goBack;
Double total=0.0;
Button btnPrintInvoice;
    List<InvoiceBillDetailsResponse.DataBean> bills=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_invoice_detail);
        init();
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("bundle");
        bills= (List<InvoiceBillDetailsResponse.DataBean>) bundle.getSerializable("bills");
        String clientName=intent.getStringExtra("clientName");
        String date=intent.getStringExtra("date");
        final String id=intent.getStringExtra("inv_id");
        ClientName.setText(clientName);
        invoicesDate.setText(date);
        submit.setText("Print");
        submit.setVisibility(View.GONE);
        btnPrintInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog progressdialog=new Dialog(InvoiceDetail.this);
                progressdialog.setContentView(R.layout.loadingdialog);
                progressdialog.setCancelable(false);
                progressdialog.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<PrintResponse>call=userInterface.printInvoice(id);
                call.enqueue(new Callback<PrintResponse>() {
                    @Override
                    public void onResponse(Call<PrintResponse> call, Response<PrintResponse> response) {
                        if (response.code()==200)
                        {
                            progressdialog.dismiss();
                            String url=response.body().getPDF_web_view();
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                            startActivity(browserIntent);
                        }
                        else
                        {
                            Converter<ResponseBody, ApiError> converter = ApiClient.getClient().
                                    responseBodyConverter(ApiError.class,new Annotation[0]);
                            ApiError error;
                            try {
                                error = converter.convert(response.errorBody());
                                ApiError.StatusBean status=error.getStatus();
                                Toast.makeText(InvoiceDetail.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                            } catch (IOException e) { e.printStackTrace(); }
                            progressdialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<PrintResponse> call, Throwable t) {
                        Toast.makeText(InvoiceDetail.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });
        for (int i=0;i<bills.size();i++)
        {
            total=total+Double.parseDouble(bills.get(i).getPrice());
        }
        tvTotal.setText(String.valueOf(total));
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        tvtoolheader.setText("Invoice Detail");
        if (bills.isEmpty())
        {
            Toast.makeText(this, "No Bills", Toast.LENGTH_SHORT).show();
        }
        else
        {
            addHeaders();
            for (int i=0;i<bills.size();i++)

            {addData(String.valueOf(i+1),bills.get(i).getParticulars(),bills.get(i).getWeight(),bills.get(i).getRate(),bills.get(i).getPrice());}
        }
    }
    public void addHeaders() {
        TableLayout tl = findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());
        tr.addView(getTextView(0, "Sl.No", ContextCompat.getColor(this, R.color.white), Typeface.BOLD, ContextCompat.getColor(this, R.color.pink)));
        tr.addView(getTextView(0, "particular",  ContextCompat.getColor(this, R.color.white), Typeface.BOLD,ContextCompat.getColor(this, R.color.pink)));
        tr.addView(getTextView(0, "Weight(Kg)", ContextCompat.getColor(this, R.color.white), Typeface.BOLD,ContextCompat.getColor(this, R.color.pink)));
        tr.addView(getTextView(0, "Rate(Rs)", ContextCompat.getColor(this, R.color.white), Typeface.BOLD,ContextCompat.getColor(this, R.color.pink)));
        tr.addView(getTextView(0, "Price(Rs)", ContextCompat.getColor(this, R.color.white), Typeface.BOLD,ContextCompat.getColor(this, R.color.pink)));

        tl.addView(tr, getTblLayoutParams());
    }
    public void addData(String sno,String particular,String weight,String rate,String total) {
        // int numCompanies = companies.length;
        TableLayout tl = findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());
        tr.addView(getTextView( 1,String.valueOf(sno), ContextCompat.getColor(this, R.color.black), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.bg_color)));
        tr.addView(getTextView(1, particular,ContextCompat.getColor(this, R.color.black), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.bg_color)));
        tr.addView(getTextView(1,weight, ContextCompat.getColor(this, R.color.black), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.bg_color)));
        tr.addView(getTextView(1, rate, ContextCompat.getColor(this, R.color.black), Typeface.NORMAL, ContextCompat.getColor(this, R.color.bg_color)));
        tr.addView(getTextView(1, total, ContextCompat.getColor(this, R.color.black), Typeface.NORMAL, ContextCompat.getColor(this, R.color.bg_color)));
        tl.addView(tr, getTblLayoutParams());


    }
    private TextView getTextView (int id, String title, int color, int typeface, int bgColor)
    {
        TextView tv = new TextView(InvoiceDetail.this);
        tv.setId(id);
        tv.setText(title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(5, 5, 5, 5);
        tv.setTypeface(Typeface.DEFAULT, typeface);
        tv.setBackgroundColor(bgColor);
        tv.setLayoutParams(getLayoutParams());

        return tv;
    }
    @NonNull
    private TableLayout.LayoutParams getTblLayoutParams () {
        return new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
    }
    private TableRow.LayoutParams getLayoutParams ()
    {
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 0, 0, 2);
        return params;
    }
    void init()
    {
        btnPrintInvoice=findViewById(R.id.btnprintInvoice);
        tvTotal=findViewById(R.id.tvTotal);
        tvtoolheader=findViewById(R.id.toolgenheader);
        goBack=findViewById(R.id.toolgoback);
        invoicesDate=findViewById(R.id.tvDate);
        ClientName =findViewById(R.id.tvClientName);
        submit=findViewById(R.id.toolsubmit);
    }
}
