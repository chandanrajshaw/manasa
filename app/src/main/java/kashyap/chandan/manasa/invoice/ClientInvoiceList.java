package kashyap.chandan.manasa.invoice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.client.ClientListResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientInvoiceList extends AppCompatActivity {
RelativeLayout clientLay;
TextView tvClient,tvSubmit,tvToolHeader,message;
RecyclerView invoiceList,clientList;
String clientCode;
ImageView goback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_client_invoice_list);
        init();
        tvSubmit.setText("CREATE");
        tvToolHeader.setText("Invoice List");
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ClientInvoiceList.this,AddInvoice.class);
                startActivity(intent);
            }
        });
        clientLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    final Dialog progress=new Dialog(ClientInvoiceList.this);
                    progress.setContentView(R.layout.loadingdialog);
                    progress.setCancelable(false);
                    progress.show();
                    final UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<ClientListResponse> call=userInterface.clientList();
                    call.enqueue(new Callback<ClientListResponse>() {
                        @Override
                        public void onResponse(Call<ClientListResponse> call, Response<ClientListResponse> response) {
                            if (response.code()==200)
                            {
                                progress.dismiss();
                                final Dialog clientDialog=new Dialog(ClientInvoiceList.this);
                                clientDialog.setContentView(R.layout.recyclerdialog);
                                clientDialog.setCancelable(true);
                                DisplayMetrics metrics = getResources().getDisplayMetrics();
                                int width = metrics.widthPixels;
                                clientList=clientDialog.findViewById(R.id.recycleroption);
                                clientDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                                List<ClientListResponse.DataBean> client=new ArrayList<>();
                                client=response.body().getData();
                                clientList.setLayoutManager(new LinearLayoutManager(ClientInvoiceList.this,LinearLayoutManager.VERTICAL,false));
                                clientList.setAdapter(new ClientAdapter(ClientInvoiceList.this,client,clientDialog,new CustomItemClickListener() {
                                    @Override
                                    public void onItemClick(View v, int position, String value) {
                                        tvClient.setText(value);
                                        clientCode=String.valueOf(position);
                                        clientDialog.dismiss();
                                        final Dialog progressdialog=new Dialog(ClientInvoiceList.this);
                                        progressdialog.setContentView(R.layout.loadingdialog);
                                        progressdialog.setCancelable(false);
                                        progressdialog.show();
                                        Call<ClientInvoicesResponse>call1=userInterface.clientInvoices(clientCode);
                                        call1.enqueue(new Callback<ClientInvoicesResponse>()
                                        {
                                            @Override
                                            public void onResponse(Call<ClientInvoicesResponse> call, Response<ClientInvoicesResponse> response)
                                            {
                                                if (response.code()==200)
                                                {

                                                    List<ClientInvoicesResponse.AvailableInvoiceBean> invoices=new ArrayList<>();
                                                    invoices=response.body().getAvailable_invoice();
                                                   progressdialog.dismiss();
                                                   invoiceList.setLayoutManager(new LinearLayoutManager(ClientInvoiceList.this,LinearLayoutManager.VERTICAL,false));
                                                   invoiceList.setAdapter(new ClientInvoiceAdapter(ClientInvoiceList.this,invoices));
                                                   invoiceList.setVisibility(View.VISIBLE);
                                                }
                                                else if (response.code()!=200){
                                                    clientDialog.dismiss();
                                                    progressdialog.dismiss();
                                                    invoiceList.setVisibility(View.GONE);
                                                    message.setText("Invoice List Not Available");
                                                    Toast.makeText(ClientInvoiceList.this, "Invoice Not Available", Toast.LENGTH_LONG).show();
                                                }

                                            }

                                            @Override
                                            public void onFailure(Call<ClientInvoicesResponse> call, Throwable t) {
                                                progressdialog.dismiss();
                                                Toast.makeText(ClientInvoiceList.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                                            }
                                        });
                                    }
                                }));
                                clientDialog.show();
                            }

                            else {
                                progress.dismiss();
                                Toast.makeText(ClientInvoiceList.this, "Client Not Available", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ClientListResponse> call, Throwable t) {
                            progress.dismiss();
                            Toast.makeText(ClientInvoiceList.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });

            }
        });
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
    }
    void init()
    {tvToolHeader=findViewById(R.id.toolgenheader);
        tvSubmit=findViewById(R.id.toolsubmit);
        clientLay=findViewById(R.id.clientLay);
        tvClient=findViewById(R.id.tvClient);
        invoiceList =findViewById(R.id.invoiceList);
        goback=findViewById(R.id.toolgoback);
        message=findViewById(R.id.message);
    }
}
