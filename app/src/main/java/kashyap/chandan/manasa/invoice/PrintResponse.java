package kashyap.chandan.manasa.invoice;

import java.io.Serializable;

public class PrintResponse implements Serializable {

    /**
     * status : {"code":200,"message":"url for printing pdf"}
     * PDF_web_view : http://www.igranddeveloper.xyz/mansamover/admin/invoice_print_cont/printbill_list/4
     */

    private StatusBean status;
    private String PDF_web_view;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public String getPDF_web_view() {
        return PDF_web_view;
    }

    public void setPDF_web_view(String PDF_web_view) {
        this.PDF_web_view = PDF_web_view;
    }

    public static class StatusBean {
        /**
         * code : 200
         * message : url for printing pdf
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
