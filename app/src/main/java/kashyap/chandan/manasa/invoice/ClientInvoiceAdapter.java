package kashyap.chandan.manasa.invoice;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class ClientInvoiceAdapter extends RecyclerView.Adapter<ClientInvoiceAdapter.MyViewHolder> {
    List<ClientInvoicesResponse.AvailableInvoiceBean> invoices;
    Context context;

    public ClientInvoiceAdapter(Context context, List<ClientInvoicesResponse.AvailableInvoiceBean> invoices) {
        this.context=context;
        this.invoices=invoices;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.invoicelist,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
    holder.clientName.setText(invoices.get(position).getClient_name());
    holder.date.setText(invoices.get(position).getInvoice_date());
    holder.detail.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final Dialog progressdialog=new Dialog(context);
            progressdialog.setContentView(R.layout.loadingdialog);
            progressdialog.setCancelable(false);
            progressdialog.show();
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            Call<InvoiceBillDetailsResponse>call=userInterface.invoiceParticular(invoices.get(position).getId());
            call.enqueue(new Callback<InvoiceBillDetailsResponse>() {
                @Override
                public void onResponse(Call<InvoiceBillDetailsResponse> call, Response<InvoiceBillDetailsResponse> response) {
                    if (response.code()==200)
                    {
                        progressdialog.dismiss();
                        List<InvoiceBillDetailsResponse.DataBean>bills=new ArrayList<>();
                        bills=response.body().getData();
                        Intent intent=new Intent(context,InvoiceDetail.class);
                        Bundle bundle=new Bundle();
                        bundle.putSerializable("bills", (Serializable) bills);
                        intent.putExtra("bundle",bundle);
                        intent.putExtra("date",invoices.get(position).getInvoice_date());
                        intent.putExtra("clientName",invoices.get(position).getClient_name());
                        intent.putExtra("inv_id",invoices.get(position).getId());
                        context.startActivity(intent);
                    }
                    else {
                        progressdialog.dismiss();
                        Toast.makeText(context, "Bills Not Available", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<InvoiceBillDetailsResponse> call, Throwable t) {
                    progressdialog.dismiss();
                    Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    });
    holder.delete.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
final Dialog deleteDialog=new Dialog(context);
deleteDialog.setContentView(R.layout.deletedialog);
            DisplayMetrics metrics=context.getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;
            deleteDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
            deleteDialog.show();
            TextView delete=deleteDialog.findViewById(R.id.btndelete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Dialog progressdialog=new Dialog(context);
                    progressdialog.setContentView(R.layout.loadingdialog);
                    progressdialog.setCancelable(false);
                    progressdialog.show();
                    UserInterface userInterface=ApiClient.getClient().create(UserInterface.class);
                    Call<DeleteInvoiceResponse>call=userInterface.deleteInvoice(invoices.get(holder.getAdapterPosition()).getId());
                    call.enqueue(new Callback<DeleteInvoiceResponse>() {
                        @Override
                        public void onResponse(Call<DeleteInvoiceResponse> call, Response<DeleteInvoiceResponse> response) {
                            if (response.code()==200)
                            {
                                progressdialog.dismiss();
                                deleteDialog.dismiss();
                                Toast.makeText(context, "Invoice Deleted", Toast.LENGTH_SHORT).show();
                                removeAt(holder.getAdapterPosition());
                            }
                            else {
                                progressdialog.dismiss();
                                Toast.makeText(context, "Invoice Not Deleted", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<DeleteInvoiceResponse> call, Throwable t) {
                            progressdialog.dismiss();
                            Toast.makeText(context, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });

        }
    });
    }
    private void removeAt(int position) {
        invoices.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, invoices.size());
    }

    @Override
    public int getItemCount() {
        return invoices.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView clientName,date;
        LinearLayout detail;
        ImageView delete;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            clientName=itemView.findViewById(R.id.clientName);
            date=itemView.findViewById(R.id.date);
            detail=itemView.findViewById(R.id.detail);
            delete=itemView.findViewById(R.id.erase);
        }
    }
}
