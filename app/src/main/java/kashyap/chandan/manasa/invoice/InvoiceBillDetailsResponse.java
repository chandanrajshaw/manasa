package kashyap.chandan.manasa.invoice;

import java.io.Serializable;
import java.util.List;

public class InvoiceBillDetailsResponse implements Serializable {

    /**
     * status : {"code":200,"message":"Added invoicebill list"}
     * data : [{"id":"1","invoice_id":"1","particulars":"sdsd","weight":"dsd","rate":"dsd","price":"10"},{"id":"2","invoice_id":"1","particulars":"22","weight":"ww","rate":"ww","price":"10"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : Added invoicebill list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * invoice_id : 1
         * particulars : sdsd
         * weight : dsd
         * rate : dsd
         * price : 10
         */

        private String id;
        private String invoice_id;
        private String particulars;
        private String weight;
        private String rate;
        private String price;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getInvoice_id() {
            return invoice_id;
        }

        public void setInvoice_id(String invoice_id) {
            this.invoice_id = invoice_id;
        }

        public String getParticulars() {
            return particulars;
        }

        public void setParticulars(String particulars) {
            this.particulars = particulars;
        }

        public String getWeight() {
            return weight;
        }

        public void setWeight(String weight) {
            this.weight = weight;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }
}
