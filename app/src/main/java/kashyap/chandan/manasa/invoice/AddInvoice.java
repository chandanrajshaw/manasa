package kashyap.chandan.manasa.invoice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.adapters.VehicleTypeAdapter;
import kashyap.chandan.manasa.client.ClientListResponse;
import kashyap.chandan.manasa.lrgenerator.RLManagement;
import kashyap.chandan.manasa.valuespart.DropDownVehicleTypeResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddInvoice extends AppCompatActivity {
RelativeLayout clientLay,dateLay;
TextView tvClient,tvDate,toolheader,submit,tvmessage;
RecyclerView clientList;
DatePickerDialog.OnDateSetListener dateDialog;
Calendar myCalendar;
String clientCode;
ImageView goBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_add_invoice);
        init();
toolheader.setText("Create Invoice");
submit.setVisibility(View.GONE);
        dateLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AddInvoice.this,R.style.TimePickerTheme,dateDialog, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        dateDialog=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        clientLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String date=tvDate.getText().toString();
                if (date.equalsIgnoreCase("Date"))
                {
                    Toast.makeText(AddInvoice.this, "Select Date First", Toast.LENGTH_SHORT).show();
                }
            else
                {
                    final Dialog progress=new Dialog(AddInvoice.this);
                    progress.setContentView(R.layout.loadingdialog);
                    progress.setCancelable(false);
                    progress.show();
                    final UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                    Call<ClientListResponse> call=userInterface.clientList();
                    call.enqueue(new Callback<ClientListResponse>() {
                        @Override
                        public void onResponse(Call<ClientListResponse> call, Response<ClientListResponse> response) {
                            if (response.code()==200)
                            {
                                progress.dismiss();
                                final Dialog clientDialog=new Dialog(AddInvoice.this);
                                clientDialog.setContentView(R.layout.recyclerdialog);
                                clientDialog.setCancelable(true);
                                DisplayMetrics metrics = getResources().getDisplayMetrics();
                                int width = metrics.widthPixels;
                                clientList=clientDialog.findViewById(R.id.recycleroption);
                                clientDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                                List<ClientListResponse.DataBean> client=new ArrayList<>();
                                client=response.body().getData();
                                clientList.setLayoutManager(new LinearLayoutManager(AddInvoice.this,LinearLayoutManager.VERTICAL,false));
                                clientList.setAdapter(new ClientAdapter(AddInvoice.this,client,clientDialog,new CustomItemClickListener() {
                                    @Override
                                    public void onItemClick(View v, int position, String value) {
                                        tvClient.setText(value);
                                        clientCode=String.valueOf(position);
                                        final Dialog progressdialog=new Dialog(AddInvoice.this);
                                        progressdialog.setContentView(R.layout.loadingdialog);
                                        progressdialog.setCancelable(false);
                                        progressdialog.show();
                                        Call<CreateInvoiceResponse>call1=userInterface.createInvoice(date,clientCode);
                                        call1.enqueue(new Callback<CreateInvoiceResponse>() {
                                            @Override
                                            public void onResponse(Call<CreateInvoiceResponse> call, Response<CreateInvoiceResponse> response)
                                            {
                                                if (response.code()==200)
                                            {
                                               progressdialog.dismiss();
                                                Toast.makeText(AddInvoice.this, "Invoice Created", Toast.LENGTH_SHORT).show();
                                                tvmessage.setVisibility(View.VISIBLE);
                                                final String invoiceId= String.valueOf(response.body().getInvoice_id());
                                                final String invoicedate =tvDate.getText().toString();
                                                new Handler().postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Intent intent=new Intent(AddInvoice.this,AddParticular.class);
                                                        intent.putExtra("clientname",tvClient.getText().toString());
                                                        intent.putExtra("clientid",clientCode);
                                                        intent.putExtra("invoiceid",invoiceId);
                                                        intent.putExtra("date",invoicedate);
                                                        startActivity(intent);
                                                    }
                                                },1000);
                                                finish();
                                            }
                                            else {
                                                    progressdialog.dismiss();
                                                    Toast.makeText(AddInvoice.this, "Invoice Not Created", Toast.LENGTH_SHORT).show();
                                                }

                                            }

                                            @Override
                                            public void onFailure(Call<CreateInvoiceResponse> call, Throwable t) {
                                                progressdialog.dismiss();
                                                Toast.makeText(AddInvoice.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                                            }
                                        });
                                    }
                                }));
                                clientDialog.show();
                            }

                            else {
                                progress.dismiss();
                                Toast.makeText(AddInvoice.this, "Client Not Available", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ClientListResponse> call, Throwable t) {
                            progress.dismiss();
                            Toast.makeText(AddInvoice.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
    }
    void  init()
    {
        tvmessage=findViewById(R.id.message);
        toolheader=findViewById(R.id.toolgenheader);
        submit=findViewById(R.id.toolsubmit);
        myCalendar=Calendar.getInstance();
        clientLay =findViewById(R.id.clientLay);
        dateLay=findViewById(R.id.datelay);
        tvClient=findViewById(R.id.tvClient);
        tvDate=findViewById(R.id.tvDate);
        goBack=findViewById(R.id.toolgoback);
    }
    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        tvDate.setText(sdf.format(myCalendar.getTime()));
    }
}
