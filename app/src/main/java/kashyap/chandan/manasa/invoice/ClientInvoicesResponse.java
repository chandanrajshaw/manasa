package kashyap.chandan.manasa.invoice;

import java.io.Serializable;
import java.util.List;

public class ClientInvoicesResponse implements Serializable {

    /**
     * status : {"code":200,"message":"All detail of Client bill"}
     * available_invoice : [{"id":"1","invoice_date":"12-12-1990","client_id":"1","client_name":"vishal123"},{"id":"3","invoice_date":"05/28/2020","client_id":"1","client_name":"vishal123"},{"id":"4","invoice_date":"28/05/2020","client_id":"1","client_name":"vishal123"}]
     */

    private StatusBean status;
    private List<AvailableInvoiceBean> available_invoice;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<AvailableInvoiceBean> getAvailable_invoice() {
        return available_invoice;
    }

    public void setAvailable_invoice(List<AvailableInvoiceBean> available_invoice) {
        this.available_invoice = available_invoice;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : All detail of Client bill
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class AvailableInvoiceBean implements Serializable {
        /**
         * id : 1
         * invoice_date : 12-12-1990
         * client_id : 1
         * client_name : vishal123
         */

        private String id;
        private String invoice_date;
        private String client_id;
        private String client_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getInvoice_date() {
            return invoice_date;
        }

        public void setInvoice_date(String invoice_date) {
            this.invoice_date = invoice_date;
        }

        public String getClient_id() {
            return client_id;
        }

        public void setClient_id(String client_id) {
            this.client_id = client_id;
        }

        public String getClient_name() {
            return client_name;
        }

        public void setClient_name(String client_name) {
            this.client_name = client_name;
        }
    }
}
