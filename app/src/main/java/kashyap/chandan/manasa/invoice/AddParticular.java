package kashyap.chandan.manasa.invoice;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddParticular extends AppCompatActivity {
    int counter=0;
    ImageView goBack;
FloatingActionButton addBill;
RelativeLayout dateLay;
    DatePickerDialog.OnDateSetListener dateDialog;
    Calendar myCalendar;
    TextView date,tvtoolheader,invoicesDate,clientName,submit;
    Dialog AddBillDialog;
    TextInputEditText etparticular, etrate, etprice, etweight;
   ArrayList<String> listParticular,listRate,listPrice,listWeight,listDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_invoice_management);
        init();
        Intent intent=getIntent();
        String client=intent.getStringExtra("clientname");
        String clientid=intent.getStringExtra("clientid");
        final String invoiceid=intent.getStringExtra("invoiceid");
        String invoiceDate=intent.getStringExtra("date");
        tvtoolheader.setText("Add Particulars");
        clientName.setText(client);
        invoicesDate.setText(invoiceDate);
        addBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AddBillDialog=new Dialog(AddParticular.this);
                AddBillDialog.setContentView(R.layout.billlayout);
                DisplayMetrics metrics = getResources().getDisplayMetrics();
//                int width = metrics.widthPixels;
                AddBillDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                AddBillDialog.setCancelable(false);
                TextView btnbill=AddBillDialog.findViewById(R.id.btnbill);
//                date=AddBillDialog.findViewById(R.id.date);
//                dateLay=AddBillDialog.findViewById(R.id.datelay);
                etparticular =AddBillDialog.findViewById(R.id.particular);
                etrate =AddBillDialog.findViewById(R.id.rate);
                etprice =AddBillDialog.findViewById(R.id.price);
                etweight =AddBillDialog.findViewById(R.id.weight);
                etprice.setEnabled(false);
                TextWatcher textWatcher=new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }
                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }
                    @Override
                    public void afterTextChanged(Editable s) {
                        if (etrate.getText().toString().isEmpty() && etweight.getText().toString().isEmpty()){
                            etprice.setText("");
                        }
                        else if (etrate.getText().toString().isEmpty())
                            etprice.setText("");
                        else if (etweight.getText().toString().isEmpty())
                            etprice.setText("");
                        else {
                            int quantity=Integer.valueOf(etweight.getText().toString());
                            long amt= Long.valueOf(s.toString());
                            long tot=quantity*amt;
                            etprice.setText(String.valueOf(tot));
                      }
                    }
                };
                etrate.addTextChangedListener(textWatcher);
                etweight.addTextChangedListener(textWatcher);
//                dateLay.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        new DatePickerDialog(AddParticular.this,R.style.TimePickerTheme,dateDialog, myCalendar
//                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
//                    }
//                });
//                dateDialog=new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
//                        myCalendar.set(Calendar.YEAR, year);
//                        myCalendar.set(Calendar.MONTH, monthOfYear);
//                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                        updateLabel();
//                    }
//                };
                btnbill.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        String bDate=date.getText().toString();
                        String particulars= etparticular.getText().toString();
                        String weights= etweight.getText().toString();
                        String prices= etprice.getText().toString();
                        String rates= etrate.getText().toString();
                        if (/*bDate.equalsIgnoreCase("Date")&&*/particulars.isEmpty()&&weights.isEmpty()&&prices.isEmpty()&&rates.isEmpty())
                            Toast.makeText(AddParticular.this, "Fill All The Fields", Toast.LENGTH_SHORT).show();
//                        else if (bDate.equalsIgnoreCase("Date"))
//                            Toast.makeText(AddParticular.this, "Select Date", Toast.LENGTH_SHORT).show();
                        else if (particulars.isEmpty())
                            Toast.makeText(AddParticular.this, "Enter Particular Name", Toast.LENGTH_SHORT).show();
                        else if (weights.isEmpty())
                            Toast.makeText(AddParticular.this, "Enter Weight", Toast.LENGTH_SHORT).show();
                        else if (rates.isEmpty())
                            Toast.makeText(AddParticular.this, "Enter Rate", Toast.LENGTH_SHORT).show();
                        else if (prices.isEmpty())
                            Toast.makeText(AddParticular.this, "Enter Total Price", Toast.LENGTH_SHORT).show();
                        else {
                                counter++;
//                                listDate.add(bDate);
                                listParticular.add(particulars);
                                listPrice.add(prices);
                                listRate.add(rates);
                                listWeight.add(weights);
                                if (listWeight.size()==1)
                                {
                                    addHeaders();
                                }
                                addData(String.valueOf(counter)/*,bDate*/,particulars,weights,rates,prices);
                            AddBillDialog.dismiss();
                        }

                    }
                });
                AddBillDialog.show();
            }
        });


goBack.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
        finish();
    }
});
submit.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        final Dialog progressdialog=new Dialog(AddParticular.this);
        progressdialog.setContentView(R.layout.loadingdialog);
        progressdialog.setCancelable(false);
        progressdialog.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<AddParticularResponse>call=userInterface.addBill(invoiceid,listParticular,listWeight,listRate,listPrice);
        call.enqueue(new Callback<AddParticularResponse>() {
            @Override
            public void onResponse(Call<AddParticularResponse> call, Response<AddParticularResponse> response) {
                if (response.code()==200)
                {
                    progressdialog.dismiss();
                    Toast.makeText(AddParticular.this, "Particulars Added", Toast.LENGTH_SHORT).show();
                    Intent intent1=new Intent(AddParticular.this,ClientInvoiceList.class);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);
                    finish();
                }
                else
                {
                    progressdialog.dismiss();
                    Toast.makeText(AddParticular.this, "Particulars Not added", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddParticularResponse> call, Throwable t) {
                progressdialog.dismiss();
                Toast.makeText(AddParticular.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
});
    }
    void init()
    {
        myCalendar=Calendar.getInstance();
        listParticular=new ArrayList<>();
        listRate=new ArrayList<>();
        listPrice=new ArrayList<>();
        listWeight=new ArrayList<>();
        listDate=new ArrayList<>();
        addBill=findViewById(R.id.addBill);
        tvtoolheader=findViewById(R.id.toolgenheader);
        goBack=findViewById(R.id.toolgoback);
        invoicesDate=findViewById(R.id.tvDate);clientName=findViewById(R.id.tvClientName);
submit=findViewById(R.id.toolsubmit);
    }
    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        date.setText(sdf.format(myCalendar.getTime()));
    }
    public void addHeaders() {
        TableLayout tl = findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());
        tr.addView(getTextView(0, "Sl.No", ContextCompat.getColor(this, R.color.white), Typeface.BOLD, ContextCompat.getColor(this, R.color.pink)));
       // tr.addView(getTextView(0, "Date", ContextCompat.getColor(this, R.color.white), Typeface.BOLD, ContextCompat.getColor(this, R.color.pink)));
        tr.addView(getTextView(0, "particular",  ContextCompat.getColor(this, R.color.white), Typeface.BOLD,ContextCompat.getColor(this, R.color.pink)));
        tr.addView(getTextView(0, "Weight(Kg)", ContextCompat.getColor(this, R.color.white), Typeface.BOLD,ContextCompat.getColor(this, R.color.pink)));
        tr.addView(getTextView(0, "Rate(Rs)", ContextCompat.getColor(this, R.color.white), Typeface.BOLD,ContextCompat.getColor(this, R.color.pink)));
        tr.addView(getTextView(0, "Price(Rs)", ContextCompat.getColor(this, R.color.white), Typeface.BOLD,ContextCompat.getColor(this, R.color.pink)));

        tl.addView(tr, getTblLayoutParams());
    }
    public void addData(String sno/*,String date*/,String particular,String weight,String rate,String total) {
        // int numCompanies = companies.length;
        TableLayout tl = findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());

        tr.addView(getTextView( 1,String.valueOf(sno), ContextCompat.getColor(this, R.color.black), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.bg_color)));
//        tr.addView(getTextView(1, date,ContextCompat.getColor(this, R.color.black), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.bg_color)));
        tr.addView(getTextView(1, particular,ContextCompat.getColor(this, R.color.black), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.bg_color)));
        tr.addView(getTextView(1,weight, ContextCompat.getColor(this, R.color.black), Typeface.NORMAL,  ContextCompat.getColor(this, R.color.bg_color)));
        tr.addView(getTextView(1, rate, ContextCompat.getColor(this, R.color.black), Typeface.NORMAL, ContextCompat.getColor(this, R.color.bg_color)));
        tr.addView(getTextView(1, total, ContextCompat.getColor(this, R.color.black), Typeface.NORMAL, ContextCompat.getColor(this, R.color.bg_color)));
        tl.addView(tr, getTblLayoutParams());


    }
    private TextView getTextView ( int id, String title,int color, int typeface, int bgColor)
    {
        TextView tv = new TextView(AddParticular.this);
        tv.setId(id);
        tv.setText(title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(5, 5, 5, 5);
        tv.setTypeface(Typeface.DEFAULT, typeface);
        tv.setBackgroundColor(bgColor);
        tv.setLayoutParams(getLayoutParams());

        return tv;
    }
    @NonNull
    private TableLayout.LayoutParams getTblLayoutParams () {
        return new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
    }
    private TableRow.LayoutParams getLayoutParams ()
    {
        TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 0, 0, 2);
        return params;
    }
}
