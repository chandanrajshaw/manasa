package kashyap.chandan.manasa.client;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.R;

public class ClientListAdapter extends RecyclerView.Adapter<ClientListAdapter.MyViewHolder> {
    Context context;
    List<ClientListResponse.DataBean> clients;
    public ClientListAdapter(Context context, List<ClientListResponse.DataBean> clients) {
        this.context=context;
        this.clients=clients;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.clientlist,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
holder.detail.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(context,ClientDetails.class);
        Bundle bundle=new Bundle();
        bundle.putSerializable("client",clients.get(position));
        intent.putExtra("bundle",bundle);
        context.startActivity(intent);
    }
});
        Picasso.get().load(ApiClient.IMAGEURL +clients.get(position).getLogo()).placeholder(R.drawable.loading)
                .error(R.drawable.terms).into(holder.clientLogo);
        holder.location.setText(clients.get(position).getAddress());
        holder.phone.setText(clients.get(position).getPhone_no());
        holder.email.setText(clients.get(position).getEmail());
        holder.clientName.setText(clients.get(position).getClient_name());
    }

    @Override
    public int getItemCount() {
        return clients.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout detail;
        CircleImageView clientLogo;
        TextView location, phone,clientName,email;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            clientLogo=itemView.findViewById(R.id.clientLogo);
            location=itemView.findViewById(R.id.phone);
            phone =itemView.findViewById(R.id.phone);
            detail=itemView.findViewById(R.id.detail);
            clientName=itemView.findViewById(R.id.clientName);
            email=itemView.findViewById(R.id.email);

        }
    }
}
