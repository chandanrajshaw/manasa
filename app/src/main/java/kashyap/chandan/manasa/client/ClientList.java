package kashyap.chandan.manasa.client;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.MainActivity;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientList extends AppCompatActivity {
RecyclerView clientList;
TextView toolHeader,submit;
ImageView goBack;
FloatingActionButton addClient;
    List<ClientListResponse.DataBean> clients=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_client_list);
        init();
        final Dialog progressDialog=new Dialog(ClientList.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        submit.setVisibility(View.GONE);
        toolHeader.setText("Clients");
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        addClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ClientList.this,ClientRegistration.class);
                startActivity(intent);
            }
        });
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<ClientListResponse> call=userInterface.clientList();
        call.enqueue(new Callback<ClientListResponse>() {
            @Override
            public void onResponse(Call<ClientListResponse> call, Response<ClientListResponse> response) {
                if (response.code()==200)
                {
                    clients=response.body().getData();
                    clientList.setLayoutManager(new LinearLayoutManager(ClientList.this,LinearLayoutManager.VERTICAL,false));
                    clientList.setAdapter(new ClientListAdapter(ClientList.this,clients));
                    progressDialog.dismiss();
                }
                else
                {
                    progressDialog.dismiss();
                    Snackbar.make(ClientList.this.getWindow().getDecorView().findViewById(android.R.id.content),"No Client available",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ClientListResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ClientList.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }
    void init()
    {
        clientList=findViewById(R.id.clientList);
        goBack=findViewById(R.id.toolgoback);
        addClient=findViewById(R.id.addClient);
        toolHeader=findViewById(R.id.toolgenheader);
        submit=findViewById(R.id.toolsubmit);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Dialog progressDialog=new Dialog(ClientList.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        submit.setVisibility(View.GONE);
        toolHeader.setText("Clients");
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        addClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ClientList.this,ClientRegistration.class);
                startActivity(intent);
            }
        });
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<ClientListResponse> call=userInterface.clientList();
        call.enqueue(new Callback<ClientListResponse>() {
            @Override
            public void onResponse(Call<ClientListResponse> call, Response<ClientListResponse> response) {
                if (response.code()==200)
                {
                    clients=response.body().getData();
                    clientList.setLayoutManager(new LinearLayoutManager(ClientList.this,LinearLayoutManager.VERTICAL,false));
                    clientList.setAdapter(new ClientListAdapter(ClientList.this,clients));
                    progressDialog.dismiss();
                }
                else
                {
                    progressDialog.dismiss();
                    Snackbar.make(ClientList.this.getWindow().getDecorView().findViewById(android.R.id.content),"Driver List Not available",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ClientListResponse> call, Throwable t) {

            }
        });
    }
}
