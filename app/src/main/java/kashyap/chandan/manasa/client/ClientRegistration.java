package kashyap.chandan.manasa.client;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.RealPathUtil;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.driver.DriverManagement;
import kashyap.chandan.manasa.vehicle.MyImageAdapter;
import kashyap.chandan.manasa.vehicle.VehicleManagement;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientRegistration extends AppCompatActivity {
    final int COM_DOC_PDF =10;final int AADHAR_PDF=20;final int PAN_PDF=30;final int GST_PDF=40;final int COM_DOC_IMG=1;final int AADHAR_IMG=2;final int PAN_IMG=3;
    final int GST_IMG=4;
    RecyclerView recComDocImg,recPanImg,recAadharImg,recGstImg;
    ArrayList<Uri> listComImg,listPanImg,listAdharImg,listGstImg;
    LinearLayout imageFile,pdfFile;
    List<MultipartBody.Part> CompanyImgPart,AadharImgPart,GstImgPart,PanImgPart;
    MultipartBody.Part panPdfPart,gstPdfPart,aadharPdfPart,comPdfPart;
    Uri panPdfUri,gstPdfUri,comPdfUri,aadharPdfUri;
/*------------------------File for Pdf Uploading--------------------------*/
    String companyDocFile,aadharFile,gstFile,pancardFile;
    ImageView companyLogo,goBack;
    RadioGroup filegrp;
    RadioButton pdfdoc,imagedoc;
    TextView submit,tvtoolHeader,btnComDocPdf,btnAdharPdf,btnPanPdf,btnGstPdf,btnComDocImg,btnAdharImg,btnPanImg,btnGstImg,comDocPath,aadharPath,panPath,gstPath;
    private String picturePath=null;
    private File image=null;
    private Bitmap converetdImage;
    Dialog cameradialog;
    private Bitmap bitmap;
/*--------------------editText --------------------------*/
    TextInputEditText etComment,etCompanyName,etOwnerName,etPhone,etEmail,etAddress,etPanNumberP,etAadharNumberP,etGstNumberP,etPanNumberI,etAadharNumberI,etGstNumberI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_client_registration);
        init();
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        tvtoolHeader.setText("Client Registration");
        /*----------------RADIO BTN WORK-------------------*/
        filegrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i==R.id.pdf)
                {
                    listComImg.clear();
                    CompanyImgPart.clear();
                    recComDocImg.removeAllViews();
                    listAdharImg.clear();
                    AadharImgPart.clear();
                    recAadharImg.removeAllViews();
                    PanImgPart.clear();
                    listGstImg.clear();
                    GstImgPart.clear();
                    listPanImg.clear();
                    imageFile.setVisibility(View.GONE);
                    pdfFile.setVisibility(View.VISIBLE);
                }
                else if (i==R.id.image)
                {panPdfUri=null;
                gstPdfUri=null;
                comPdfUri=null;
                aadharPdfUri=null;
                    panPdfPart=null;
                    gstPdfPart=null;
                    aadharPdfPart=null;
                    comPdfPart=null;
                    imageFile.setVisibility(View.VISIBLE);
                    pdfFile.setVisibility(View.GONE);
                }
            }
        });
        /*----------------------Image Btn Work------------------------*/
        btnPanImg.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(ClientRegistration.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
                        ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                        ||ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {
                    askPermission();
                }
                else {
                    showChooser(PAN_IMG);
                }
            }
        });
        btnAdharImg.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(ClientRegistration.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
                        ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                        ||ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {
                    askPermission();
                }
                else {
                    showChooser(AADHAR_IMG);
                }
            }
        });
        btnGstImg.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(ClientRegistration.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
                        ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                        ||ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {
                    askPermission();
                }
                else {
                    showChooser(GST_IMG);
                }
            }
        });
        btnComDocImg.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(ClientRegistration.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
                        ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                        ||ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {
                    askPermission();
                }
                else {
                    listComImg.clear();
                    recComDocImg.removeAllViews();
                    showChooser(COM_DOC_IMG);
                }
            }
        });
        /*-----------------------PDF BTN WORK------------------------*/
        btnComDocPdf.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(ClientRegistration.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
                        ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                        ||ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {
                    askPermission();
                }
                else {
                    showPdfChooser(COM_DOC_PDF);
                }

                }
        });
        btnPanPdf.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(ClientRegistration.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
                        ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                        ||ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {
                    askPermission();
                }
                else {
                    showPdfChooser(PAN_PDF);

                }

            }
        });
        btnAdharPdf.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(ClientRegistration.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
                        ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                        ||ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {
                    askPermission();
                }
                else {
                    showPdfChooser(AADHAR_PDF);
                }

            }
        });
        btnGstPdf.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(ClientRegistration.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
                        ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                        ||ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {
                    askPermission();
                }
                else {
                    showPdfChooser(GST_PDF);
                }

            }
        });
        /*--------------------------Image Btn Work-----------------------*/
companyLogo.setOnClickListener(new View.OnClickListener() {
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        if (ContextCompat.checkSelfPermission(ClientRegistration.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED||
                ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                ||ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED) {

            askPermission();
        }
        else {

            ImageView camera, folder;
            cameradialog = new Dialog(ClientRegistration.this);
            cameradialog.setContentView(R.layout.dialogboxcamera);
            DisplayMetrics metrics=getResources().getDisplayMetrics();
            int width=metrics.widthPixels;
            cameradialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
            cameradialog.show();
            cameradialog.setCancelable(true);
            cameradialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Window window = cameradialog.getWindow();
            window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
            camera = cameradialog.findViewById(R.id.camera);
            folder = cameradialog.findViewById(R.id.gallery);
            folder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, 100);
                    cameradialog.dismiss();
                }
            });
            camera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, 101);
                    cameradialog.dismiss();
                }
            });
        }
    }
});
submit.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        String company=etCompanyName.getText().toString();
        String owner=etOwnerName.getText().toString();
        String phone=etPhone.getText().toString();
        String email=etEmail.getText().toString();
        String address=etAddress.getText().toString();
        String cmnt=etComment.getText().toString();
        String stat="1";
        if (image==null&&company.isEmpty()&&owner.isEmpty()&&phone.isEmpty()&&phone.length()!=10&&email.isEmpty()&&address.isEmpty())
            Toast.makeText(ClientRegistration.this, "Please Fill all details", Toast.LENGTH_SHORT).show();
       else if (image==null) Toast.makeText(ClientRegistration.this, "Select Logo", Toast.LENGTH_SHORT).show();
        else if (company.isEmpty()) Toast.makeText(ClientRegistration.this, "Enter Company Name", Toast.LENGTH_SHORT).show();
    else if (owner.isEmpty()) Toast.makeText(ClientRegistration.this, "Enter Owner Name", Toast.LENGTH_SHORT).show();
   else if (phone.isEmpty()||phone.length()!=10) Toast.makeText(ClientRegistration.this, "Enter Valid Phone Number", Toast.LENGTH_SHORT).show();
    else if (email.isEmpty()) Toast.makeText(ClientRegistration.this, "Enter Email", Toast.LENGTH_SHORT).show();
    else if (address.isEmpty()) Toast.makeText(ClientRegistration.this, "Enter Address", Toast.LENGTH_SHORT).show();
    else {
            UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
            RequestBody cCompany = RequestBody.create(MediaType.parse("multipart/form-data"), company);
            RequestBody cOwner = RequestBody.create(MediaType.parse("multipart/form-data"), owner);
            RequestBody cPhone = RequestBody.create(MediaType.parse("multipart/form-data"),phone);
            RequestBody cEmail = RequestBody.create(MediaType.parse("multipart/form-data"), email);
            RequestBody cAddress = RequestBody.create(MediaType.parse("multipart/form-data"), address);
            RequestBody cComment = RequestBody.create(MediaType.parse("multipart/form-data"), cmnt);
            RequestBody status = RequestBody.create(MediaType.parse("multipart/form-data"), stat);

            MultipartBody.Part logo=null;
            RequestBody requestFilelogo = RequestBody.create(MediaType.parse("multipart/form-data"), image);
            logo = MultipartBody.Part.createFormData("logo", image.getName(), requestFilelogo);
        int ide=filegrp.getCheckedRadioButtonId();
                if (ide==R.id.pdf)
                        {
                            MultipartBody.Part companybody=null,gstbody=null,panbody=null,aadharbody;
                            String panNo=etPanNumberP.getText().toString();
                            String gstNo=etGstNumberP.getText().toString();
                            String aadharNo=etAadharNumberP.getText().toString();
                            if (panNo.isEmpty()&&aadharNo.isEmpty()&&gstNo.isEmpty())
                                Toast.makeText(ClientRegistration.this, "Enter Document Number", Toast.LENGTH_SHORT).show();
                           else if (panNo.isEmpty())
                                Toast.makeText(ClientRegistration.this, "Enter Pan Number", Toast.LENGTH_SHORT).show();
                            else if (aadharNo.isEmpty())
                                Toast.makeText(ClientRegistration.this, "Enter Aadhar Number", Toast.LENGTH_SHORT).show();
                            else if (gstNo.isEmpty())
                                Toast.makeText(ClientRegistration.this, "Enter Gst Number", Toast.LENGTH_SHORT).show();
                            else if(comPdfUri==null)
                                Toast.makeText(ClientRegistration.this, "Select Company Document", Toast.LENGTH_SHORT).show();
                            else if (panPdfUri == null)
                                Toast.makeText(ClientRegistration.this, "Select Pan Card", Toast.LENGTH_SHORT).show();
                            else if (aadharPdfUri==null)
                                Toast.makeText(ClientRegistration.this, "Select Aadhar File", Toast.LENGTH_SHORT).show();
                              else if ( gstPdfUri==null)
                                Toast.makeText(ClientRegistration.this, "Select Gst File", Toast.LENGTH_SHORT).show();
                         else {
                                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                                companybody = MultipartBody.Part.createFormData("company_doc_img[]", "", requestFile);
                                CompanyImgPart.add(companybody);
                                RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                                panbody = MultipartBody.Part.createFormData("panimage[]", "", requestFile1);
                                PanImgPart.add(panbody);
                                RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                                gstbody = MultipartBody.Part.createFormData("gstimage[]", "", requestFile2);
                                GstImgPart.add(gstbody);
                                RequestBody requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                                aadharbody = MultipartBody.Part.createFormData("aimage[]", "", requestFile3);
                                AadharImgPart.add(aadharbody);
                             comPdfPart=prepareFilePart("company_doc",comPdfUri);
                             panPdfPart=prepareFilePart("pan_doc",panPdfUri);
                             aadharPdfPart=prepareFilePart("aadhar_doc",aadharPdfUri);
                             gstPdfPart=prepareFilePart("gst_doc",gstPdfUri);
                                RequestBody pPan = RequestBody.create(MediaType.parse("multipart/form-data"), panNo);
                                RequestBody pGst = RequestBody.create(MediaType.parse("multipart/form-data"), gstNo);
                                RequestBody paadhar = RequestBody.create(MediaType.parse("multipart/form-data"),aadharNo);
                                Call<AddClientResponse>call=userInterface.addClient(cCompany,cOwner,cPhone,cEmail,cAddress
                                ,logo,comPdfPart,panPdfPart,aadharPdfPart,gstPdfPart,CompanyImgPart,PanImgPart,AadharImgPart
                                ,GstImgPart,pPan,paadhar,pGst,cComment,status);
                                final Dialog progressDialog=new Dialog(ClientRegistration.this);
                                progressDialog.setContentView(R.layout.loadingdialog);
                                progressDialog.setCancelable(false);
                                progressDialog.show();
                                call.enqueue(new Callback<AddClientResponse>() {
                                    @Override
                                    public void onResponse(Call<AddClientResponse> call, Response<AddClientResponse> response) {
                                        if (response.code()==200)
                                        {

                                            progressDialog.dismiss();
                                            Toast.makeText(ClientRegistration.this, "Client Added", Toast.LENGTH_SHORT).show();
                                    overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                                     finish();
                                        }
                                        else {
                                            progressDialog.dismiss();
                                            Toast.makeText(ClientRegistration.this, "Client Not Added", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<AddClientResponse> call, Throwable t) {
progressDialog.dismiss();
                                        Toast.makeText(ClientRegistration.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                else if (ide==R.id.image)
                        {
                            //listComImg,listPanImg,listAdharImg,listGstImg;
                            String panNo=etPanNumberI.getText().toString();
                            String gstNo=etGstNumberI.getText().toString();
                            String aadharNo=etAadharNumberI.getText().toString();
                            if (panNo.isEmpty()&&aadharNo.isEmpty()&&gstNo.isEmpty())
                                Toast.makeText(ClientRegistration.this, "Enter Document Number", Toast.LENGTH_SHORT).show();
                            else if (panNo.isEmpty())
                                Toast.makeText(ClientRegistration.this, "Enter Pan Number", Toast.LENGTH_SHORT).show();
                            else if (aadharNo.isEmpty())
                                Toast.makeText(ClientRegistration.this, "Enter Aadhar Number", Toast.LENGTH_SHORT).show();
                            else if (gstNo.isEmpty())
                                Toast.makeText(ClientRegistration.this, "Enter Gst Number", Toast.LENGTH_SHORT).show();
                            else if(listComImg.isEmpty())
                                Toast.makeText(ClientRegistration.this, "Select Company Document", Toast.LENGTH_SHORT).show();
                            else if (listPanImg.isEmpty())
                                Toast.makeText(ClientRegistration.this, "Select Pan Card", Toast.LENGTH_SHORT).show();
                            else if (listAdharImg.isEmpty())
                                Toast.makeText(ClientRegistration.this, "Select Aadhar File", Toast.LENGTH_SHORT).show();
                            else if ( listGstImg.isEmpty())
                                Toast.makeText(ClientRegistration.this, "Select Gst File", Toast.LENGTH_SHORT).show();
                            else
                            {
                                // CompanyImgPart,AadharImgPart,GstImgPart
                                RequestBody requestFiles = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                                comPdfPart = MultipartBody.Part.createFormData("company_doc", "", requestFiles);
                                RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                                panPdfPart = MultipartBody.Part.createFormData("pan_doc", "", requestFile1);
                                RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                                aadharPdfPart = MultipartBody.Part.createFormData("aadhar_doc", "", requestFile2);
                                RequestBody requestFile3 = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                                gstPdfPart = MultipartBody.Part.createFormData("gst_doc", "", requestFile3);
                                for (int i=0;i<listComImg.size();i++){ CompanyImgPart.add(prepareFilePart("company_doc_img[]",listComImg.get(i))); }
                                for (int i=0;i<listPanImg.size();i++){PanImgPart.add(prepareFilePart("panimage[]",listPanImg.get(i)));}
                                for (int i=0;i<listAdharImg.size();i++){AadharImgPart.add(prepareFilePart("aimage[]",listAdharImg.get(i)));}
                                for (int i=0;i<listGstImg.size();i++){GstImgPart.add(prepareFilePart("gstimage[]",listGstImg.get(i)));}
                                RequestBody iPan = RequestBody.create(MediaType.parse("multipart/form-data"), panNo);
                                RequestBody iGst = RequestBody.create(MediaType.parse("multipart/form-data"), gstNo);
                                RequestBody iaadhar = RequestBody.create(MediaType.parse("multipart/form-data"),aadharNo);
                                Call<AddClientResponse>call=userInterface.addClient(cCompany,cOwner,cPhone,cEmail,cAddress
                                        ,logo,comPdfPart,panPdfPart,aadharPdfPart,gstPdfPart,CompanyImgPart,PanImgPart,AadharImgPart
                                        ,GstImgPart,iPan,iaadhar,iGst,cComment,status);
                                final Dialog progressDialog=new Dialog(ClientRegistration.this);
                                progressDialog.setContentView(R.layout.loadingdialog);
                                progressDialog.setCancelable(false);
                                progressDialog.show();
                                call.enqueue(new Callback<AddClientResponse>() {
                                    @Override
                                    public void onResponse(Call<AddClientResponse> call, Response<AddClientResponse> response) {
                                        if (response.code()==200)
                                        {
                                            progressDialog.dismiss();
                                            Toast.makeText(ClientRegistration.this, "Client Added", Toast.LENGTH_SHORT).show();
                                            overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                                            finish();
                                        }
                                        else {
                                            progressDialog.dismiss();
                                            Toast.makeText(ClientRegistration.this, "Client Not Added", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<AddClientResponse> call, Throwable t) {
progressDialog.dismiss();
                                        Toast.makeText(ClientRegistration.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        }
                else Toast.makeText(ClientRegistration.this, "Select Document Type", Toast.LENGTH_SHORT).show();
        }
    }
});

    }
   private void init()
   {
       CompanyImgPart=new ArrayList<>();
       AadharImgPart=new ArrayList<>();
       GstImgPart=new ArrayList<>();
       PanImgPart=new ArrayList<>();
       etComment=findViewById(R.id.etComment);
       etCompanyName=findViewById(R.id.etCompanyName);
       etOwnerName=findViewById(R.id.etOwnerName);
       etPhone=findViewById(R.id.etPhone);
       etEmail=findViewById(R.id.etEmail);
       etAddress=findViewById(R.id.etAddress);
       etPanNumberP=findViewById(R.id.etPdfPanNumber);
       etAadharNumberP=findViewById(R.id.etpdfAdharNumber);
       etGstNumberP=findViewById(R.id.etpdfGstNumber);
       etPanNumberI=findViewById(R.id.etPanNumberImg);
       etAadharNumberI=findViewById(R.id.etAdharNumberImg);
       etGstNumberI=findViewById(R.id.etGstNumberImg);
       submit=findViewById(R.id.toolsubmit);
       goBack=findViewById(R.id.toolgoback);
       tvtoolHeader=findViewById(R.id.toolgenheader);
       companyLogo=findViewById(R.id.companyLogo);
       imageFile=findViewById(R.id.clientimageFile);
       pdfFile=findViewById(R.id.clientpdffile);
       filegrp=findViewById(R.id.filegrp);
       pdfdoc=findViewById(R.id.pdf);
       imagedoc=findViewById(R.id.image);
       /*---------------pdf btn and Path-----------------*/
       btnComDocPdf=findViewById(R.id.btncomdocpdf);
       btnAdharPdf=findViewById(R.id.btnAdharPdf);
       btnPanPdf=findViewById(R.id.btnPanPdf);
       btnGstPdf=findViewById(R.id.btnGstPdf);
       comDocPath=findViewById(R.id.comDocpath);
       aadharPath=findViewById(R.id.adharPath);
       panPath=findViewById(R.id.panPath);
       gstPath=findViewById(R.id.gstPath);
       /*---------------Image btn-----------------*/
       btnComDocImg=findViewById(R.id.btnComDocImg);
       btnAdharImg=findViewById(R.id.btnAdharImg);
       btnPanImg=findViewById(R.id.btnPanImg);
       btnGstImg=findViewById(R.id.btnGstImg);
       /*----------------recyxler View--------------------*/
       recComDocImg=findViewById(R.id.recComDocImg);
       recPanImg=findViewById(R.id.recPanImg);
       recAadharImg=findViewById(R.id.recAadharImg);
       recGstImg=findViewById(R.id.recGstImg);
       /*----------------Arraylist---------------------*/
       listAdharImg=new ArrayList<>();
       listComImg=new ArrayList<>();
       listGstImg=new ArrayList<>();
       listPanImg=new ArrayList<>();
   }
    private void showChooser( int code) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), code);
    }
    public  void showPdfChooser(int code) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("application/pdf");
        startActivityForResult(intent,code);
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void askPermission() {
        if (ContextCompat.checkSelfPermission(ClientRegistration.this, Manifest.permission.READ_EXTERNAL_STORAGE )!= PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED
                ||ContextCompat.checkSelfPermission(ClientRegistration.this,Manifest.permission.CAMERA)!=PackageManager.PERMISSION_GRANTED)
        {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA},100);
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case 100:
                int length= grantResults.length;
                if (grantResults.length>0)
                {
                    for (int i=0;i<length;i++)
                    {
                        if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                            // Granted. Start getting the location information
                        }
                        else if(grantResults[i]==PackageManager.PERMISSION_DENIED) {
                            {
                                final AlertDialog.Builder builder=new AlertDialog.Builder(ClientRegistration.this,R.style.AlertDialogTheme);
                                builder.setTitle("Notice");
                                builder.setMessage("You Had Denied the Necessary Permissions.Please Go to app Setting and give All the permissions");
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent=new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                                        intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                                        startActivity(intent);
                                    }
                                });
                                builder.show();
                                break;
                            }

                        }

                    }

                }

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (resultCode == RESULT_OK) {
            if (requestCode == COM_DOC_IMG) {
                if (resultData != null) {
                    if (resultData.getClipData() != null) {
                        int count = resultData.getClipData().getItemCount();
                        int currentItem = 0;
                        while (currentItem <count) {
                            Uri imageUri = resultData.getClipData().getItemAt(currentItem).getUri();
                            currentItem = currentItem + 1;
                            Log.d("Uri Selected", imageUri.toString());

                            try {
                                listComImg.add(imageUri);
                                recComDocImg.setLayoutManager(new LinearLayoutManager(ClientRegistration.this,LinearLayoutManager.HORIZONTAL,false));
                                recComDocImg.setAdapter(new MyImageAdapter(ClientRegistration.this,listComImg));

                            } catch (Exception e) {
                                Log.e("TAG", "File select error", e);
                            }
                        }
                    } else if (resultData.getData() != null) {

                        final Uri uri = resultData.getData();
                        Log.i("TAG", "Uri = " + uri.toString());

                        try {
                            listComImg.add(uri);
                            recComDocImg.setLayoutManager(new LinearLayoutManager(ClientRegistration.this,LinearLayoutManager.HORIZONTAL,false));
                            recComDocImg.setAdapter(new MyImageAdapter(ClientRegistration.this,listComImg));

                        } catch (Exception e) {
                            Log.e("TAG", "File select error", e);
                        }
                    }
                }
            }
          else if (requestCode == PAN_IMG) {
                listPanImg.clear();
                recPanImg.removeAllViews();
                if (resultData != null) {
                    if (resultData.getClipData() != null) {
                        int count = resultData.getClipData().getItemCount();
                        int currentItem = 0;
                        while (currentItem <1) {
                            Uri imageUri = resultData.getClipData().getItemAt(currentItem).getUri();
                            currentItem = currentItem + 1;

                            Log.d("Uri Selected", imageUri.toString());

                            try {
                                listPanImg.add(imageUri);
                                recPanImg.setLayoutManager(new LinearLayoutManager(ClientRegistration.this,LinearLayoutManager.HORIZONTAL,false));
                                recPanImg.setAdapter(new MyImageAdapter(ClientRegistration.this,listPanImg));

                            } catch (Exception e) {
                                Log.e("TAG", "File select error", e);
                            }
                        }
                    } else if (resultData.getData() != null) {

                        final Uri uri = resultData.getData();
                        Log.i("TAG", "Uri = " + uri.toString());

                        try {
                            listPanImg.add(uri);
                            recPanImg.setLayoutManager(new LinearLayoutManager(ClientRegistration.this,LinearLayoutManager.HORIZONTAL,false));
                            recPanImg.setAdapter(new MyImageAdapter(ClientRegistration.this,listPanImg));

                        } catch (Exception e) {
                            Log.e("TAG", "File select error", e);
                        }
                    }
                }
            }
            else if (requestCode == AADHAR_IMG) {
                listAdharImg.clear();
                recAadharImg.removeAllViews();
                if (resultData != null) {
                    if (resultData.getClipData() != null) {
                        int count = resultData.getClipData().getItemCount();
                        int currentItem = 0;
                        while (currentItem < 2) {
                            Uri imageUri = resultData.getClipData().getItemAt(currentItem).getUri();
                            currentItem = currentItem + 1;

                            Log.d("Uri Selected", imageUri.toString());

                            try {
                                listAdharImg.add(imageUri);
                                recAadharImg.setLayoutManager(new LinearLayoutManager(ClientRegistration.this,LinearLayoutManager.HORIZONTAL,false));
                                recAadharImg.setAdapter(new MyImageAdapter(ClientRegistration.this,listAdharImg));

                            } catch (Exception e) {
                                Log.e("TAG", "File select error", e);
                            }
                        }
                    } else if (resultData.getData() != null) {

                        final Uri uri = resultData.getData();
                        Log.i("TAG", "Uri = " + uri.toString());

                        try {
                            listAdharImg.add(uri);
                            recAadharImg.setLayoutManager(new LinearLayoutManager(ClientRegistration.this,LinearLayoutManager.HORIZONTAL,false));
                            recAadharImg.setAdapter(new MyImageAdapter(ClientRegistration.this,listAdharImg));

                        } catch (Exception e) {
                            Log.e("TAG", "File select error", e);
                        }
                    }
                }
            }
            else if (requestCode == GST_IMG) {
                if (resultData != null) {
                    listGstImg.clear();
                    recPanImg.removeAllViews();
                    if (resultData.getClipData() != null) {
                        int count = resultData.getClipData().getItemCount();
                        int currentItem = 0;
                        while (currentItem < 2) {
                            Uri imageUri = resultData.getClipData().getItemAt(currentItem).getUri();
                            currentItem = currentItem + 1;

                            Log.d("Uri Selected", imageUri.toString());

                            try {
                                listGstImg.add(imageUri);
                                recGstImg.setLayoutManager(new LinearLayoutManager(ClientRegistration.this,LinearLayoutManager.HORIZONTAL,false));
                                recGstImg.setAdapter(new MyImageAdapter(ClientRegistration.this,listGstImg));

                            } catch (Exception e) {
                                Log.e("TAG", "File select error", e);
                            }
                        }
                    } else if (resultData.getData() != null) {

                        final Uri uri = resultData.getData();
                        Log.i("TAG", "Uri = " + uri.toString());

                        try {
                            listGstImg.add(uri);
                            recGstImg.setLayoutManager(new LinearLayoutManager(ClientRegistration.this,LinearLayoutManager.HORIZONTAL,false));
                            recGstImg.setAdapter(new MyImageAdapter(ClientRegistration.this,listGstImg));

                        } catch (Exception e) {
                            Log.e("TAG", "File select error", e);
                        }
                    }
                }
            }
            else if (requestCode == COM_DOC_PDF) {
                // Get the Uri of the selected file
                Uri uri = resultData.getData();
                String uriString = uri.toString();
                File myFile = new File(uriString);
                comPdfUri=uri;
                String path = uri.getPath();
                companyDocFile=path;
                comDocPath.setText(path);
//                getFilePathFromURI(MainActivity.this,uri);
                Log.d("ioooo",path);
//                uploadPDF(path);
            }
            else if (requestCode == PAN_PDF) {
                // Get the Uri of the selected file
                Uri uri = resultData.getData();
                String uriString = uri.toString();
                File myFile = new File(uriString);
                String path = uri.getPath();
                panPdfUri=uri;
                pancardFile=path;
                panPath.setText(path);
//                getFilePathFromURI(MainActivity.this,uri);
                Log.d("ioooo",path);
//                uploadPDF(path);
            }
            else if (requestCode == AADHAR_PDF) {
                // Get the Uri of the selected file
                Uri uri = resultData.getData();
                String uriString = uri.toString();
                File myFile = new File(uriString);
                String path = uri.getPath();
                aadharPdfUri=uri;
                aadharFile=path;
                aadharPath.setText(path);
//                getFilePathFromURI(MainActivity.this,uri);
                Log.d("ioooo",path);
//                uploadPDF(path);
            }
            else if (requestCode == GST_PDF) {
                // Get the Uri of the selected file
                Uri uri = resultData.getData();
                String uriString = uri.toString();
                gstPdfUri=uri;
                File myFile = new File(uriString);
                String path = uri.getPath();
                gstFile=path;
                gstPath.setText(path);
//                getFilePathFromURI(MainActivity.this,uri);
                Log.d("ioooo",path);
//                uploadPDF(path);
            }
            else if (requestCode == 100  && resultData != null) {

//the image URI
                Uri selectedImage = resultData.getData();

                //     imagepath=selectedImage.getPath();


                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                picturePath = cursor.getString(columnIndex);
                cursor.close();
                if (picturePath != null && !picturePath.equals("")) {
                    image = new File(picturePath);
                }

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    converetdImage = getResizedBitmap(bitmap, 500);
                    companyLogo.setImageBitmap(converetdImage);
                    companyLogo.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else if (requestCode == 101 ) {
                Bitmap converetdImage = (Bitmap) resultData.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                converetdImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                 companyLogo.setImageBitmap(converetdImage);
                 companyLogo.setVisibility(View.VISIBLE);
                image = new File(Environment.getExternalStorageDirectory(), "driverPic.jpg");
                FileOutputStream fo;
                try {
                    fo = new FileOutputStream(image);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }
    }
    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        // use the FileUtils to get the actual file by uri
//        File file = FileUtils.getFile(this, fileUri);
        String path = RealPathUtil.getRealPath(ClientRegistration.this,fileUri);
        // "/mnt/sdcard/FileName.mp3"
        File file = new File(path);
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create (MediaType.parse("multipart/form-data"), file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {

        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
}
