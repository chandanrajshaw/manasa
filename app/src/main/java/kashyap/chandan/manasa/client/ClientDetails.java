package kashyap.chandan.manasa.client;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.R;

public class ClientDetails extends AppCompatActivity {
TextView toolEdit,tvClientName,tvClientPhone,tvClientEmail,tvClientAddress,tvPanNumber,tvAadharNumber,tvGstNumber,tvDocuments;
    private TextView tvToolHeader;
ImageView goBack,clientLogo,showPanPdf,showAadharPdf,showGstPdf,showRegistrationPdf;
ClientListResponse.DataBean client;
RecyclerView recPanImage,recAadharImage,recGstImage,recDocumentImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_client_details);
        init();
        Intent intent=getIntent();
        Bundle bundle=intent.getBundleExtra("bundle");
        client= (ClientListResponse.DataBean) bundle.getSerializable("client");
        Picasso.get().load(ApiClient.IMAGEURL +client.getLogo()).placeholder(R.drawable.loading).error(R.drawable.terms)
        .into(clientLogo);
        tvToolHeader.setText(" Client Details");
        tvClientName.setText(client.getClient_name());
        tvClientPhone.setText(client.getPhone_no());
        tvClientEmail.setText(client.getEmail());
        tvClientAddress.setText(client.getAddress());
        tvPanNumber.setText(client.getPan_no());
        tvAadharNumber.setText(client.getAadhar_no());
        tvGstNumber.setText(client.getGst_no());
        String pdfRegistrationdocument=client.getCompany_doc();
        String imgRegistraionDocument=client.getCompanydoc_img();
        if (pdfRegistrationdocument.isEmpty()&&imgRegistraionDocument.isEmpty())
            tvDocuments.setText("Not Available");
        else
        {
            tvDocuments.setText("Available");
            if (pdfRegistrationdocument.isEmpty())
            {

                String panImages=client.getPanimage();

                    String[] imgPan= TextUtils.split(panImages,",");
                    recPanImage.setVisibility(View.VISIBLE);
                    recPanImage.setLayoutManager(new LinearLayoutManager(ClientDetails.this,LinearLayoutManager.HORIZONTAL,false));
                  recPanImage.setAdapter(new ClientPanImageAdapter(ClientDetails.this,imgPan));

               String aadharImages=client.getAimage();
                   String[]imgAadhar=TextUtils.split(aadharImages,",");
                   recAadharImage.setVisibility(View.VISIBLE);
                   recAadharImage.setLayoutManager(new LinearLayoutManager(ClientDetails.this,LinearLayoutManager.HORIZONTAL,false));
                recAadharImage.setAdapter(new ClientAadharImageAdapter(ClientDetails.this,imgAadhar));


                String gstImages=client.getGstimage();
                    String[]imgGst=TextUtils.split(gstImages,",");
                    recGstImage.setVisibility(View.VISIBLE);
                    recGstImage.setLayoutManager(new LinearLayoutManager(ClientDetails.this,LinearLayoutManager.HORIZONTAL,false));
                recGstImage.setAdapter(new ClientGstImageAdapter(ClientDetails.this,imgGst));

                String comDocImages=client.getCompanydoc_img();
                    String[]imgComDoc=TextUtils.split(comDocImages,",");
                    recDocumentImage.setVisibility(View.VISIBLE);
                    recDocumentImage.setLayoutManager(new LinearLayoutManager(ClientDetails.this,LinearLayoutManager.HORIZONTAL,false));
                    recDocumentImage.setAdapter(new ClientCompanyDocAdapter(ClientDetails.this,imgComDoc));

                showPanPdf.setVisibility(View.GONE);
                showAadharPdf.setVisibility(View.GONE);
                showGstPdf.setVisibility(View.GONE);
                showRegistrationPdf.setVisibility(View.GONE);
            }
            else if (imgRegistraionDocument.isEmpty())
            {
                recPanImage.setVisibility(View.GONE);
                recAadharImage.setVisibility(View.GONE);
                recGstImage.setVisibility(View.GONE);
                recDocumentImage.setVisibility(View.GONE);
                showPanPdf.setVisibility(View.VISIBLE);
                showPanPdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + "http://www.igranddeveloper.xyz/mansamover/"+client.getPan_doc()), "text/html");
                        startActivity(intent);
                    }
                });
                showAadharPdf.setVisibility(View.VISIBLE);
                showAadharPdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + "http://www.igranddeveloper.xyz/mansamover/"+client.getAadhar_doc()), "text/html");
                        startActivity(intent);
                    }
                });
                showGstPdf.setVisibility(View.VISIBLE);
                showGstPdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + "http://www.igranddeveloper.xyz/mansamover/"+client.getGst_doc()), "text/html");
                        startActivity(intent);
                    }
                });
                showRegistrationPdf.setVisibility(View.VISIBLE);
                showRegistrationPdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.parse( "http://docs.google.com/viewer?url=" + "http://www.igranddeveloper.xyz/mansamover/"+client.getCompany_doc()), "text/html");
                        startActivity(intent);
                    }
                });
            }
        }

        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });

        toolEdit.setText("Edit");
        toolEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ClientDetails.this,EditClient.class);
                Bundle bundle1=new Bundle();
                bundle1.putSerializable("client",client);
                intent.putExtra("bundle",bundle1);
                startActivity(intent);

            }
        });
    }

   private void init()
    {
        showPanPdf=findViewById(R.id.showPanPdf);
        showAadharPdf=findViewById(R.id.showAadharImage);
        showGstPdf=findViewById(R.id.showGstPdf);
        showRegistrationPdf=findViewById(R.id.showRegistrationPdf);

        recPanImage=findViewById(R.id.recPanImage);
        recAadharImage=findViewById(R.id.recAadharImage);
        recGstImage=findViewById(R.id.recGstImage);
        recDocumentImage=findViewById(R.id.recDocumentImage);
        clientLogo=findViewById(R.id.clientLogo);
        tvDocuments=findViewById(R.id.tvDocuments);
        tvGstNumber=findViewById(R.id.tvGstNumber);
        tvAadharNumber=findViewById(R.id.tvAadharNumber);
        tvPanNumber=findViewById(R.id.tvPanNumber);
        tvClientAddress=findViewById(R.id.tvClientAddress);
        tvClientPhone=findViewById(R.id.tvClientPhone);
        tvClientName=findViewById(R.id.tvClientName);
        tvClientEmail=findViewById(R.id.tvClientEmail);
        tvToolHeader=findViewById(R.id.toolgenheader);
        goBack=findViewById(R.id.toolgoback);
        toolEdit=findViewById(R.id.toolsubmit);
    }
}
