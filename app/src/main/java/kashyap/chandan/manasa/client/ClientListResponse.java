package kashyap.chandan.manasa.client;

import java.io.Serializable;
import java.util.List;

public class ClientListResponse implements Serializable {


    /**
     * status : {"code":200,"message":"Added client list"}
     * data : [{"id":"1","client_name":"vishallll77777","contact_pname":"sharma","phone_no":"9709615446","email":"vishal@gmail.com","logo":"upload/1027989719images.jpg","address":"sindur hazaribagh","company_doc":"upload/298646218html2pdf_(1).pdf","companydoc_img":"","pan_no":"9890989098","panimage":"upload/1189460770i.jpg,upload/1811919555m.jpg,upload/2145272703a.jpg,upload/755648654o.jpg","pan_doc":"upload/1942465113399280173h.pdf","aadhar_doc":"upload/1836098082399280173h.pdf","gst_doc":"upload/9708308231328279108C.pdf","aadhar_no":"8746282738273987","aimage":"upload/1189460770i.jpg,upload/1811919555m.jpg,upload/414484212d.jpg,upload/923291323o.png","gst_no":"88989dh8d9snd","gstimage":"upload/1189460770i.jpg,upload/1811919555m.jpg,upload/208455064a.jpg,upload/1791478678a.jpg","comment":"hgvdghsfdgshdhsdjdjh\nfd\\ghsd\ndgskhd","status":"0"},{"id":"2","client_name":"mohit","contact_pname":"rana","phone_no":"34343434","email":"bihar@gmail.com","logo":"upload/2119612837IMG-20180713-WA0015.jpg","address":"bihar","company_doc":"upload/607340612html2pdf_(6).pdf","companydoc_img":"","pan_no":"8789657898","panimage":"upload/1484347048IMG-20190321-WA0024.jpg","pan_doc":"","aadhar_doc":"","gst_doc":"","aadhar_no":"767865456789","aimage":"upload/1165628056IMG-20190321-WA0026.jpg","gst_no":"9890989089","gstimage":"upload/1297865510IMG-20190226-WA0024.jpg","comment":"hai","status":"1"},{"id":"3","client_name":"vishal sharma","contact_pname":"kumar","phone_no":"9709615446","email":"vishalsharma@gmail.com","logo":"upload/826039132IMG_20141222_143624.jpg","address":"<p>sindur hazaribagh<\/p>\r\n","company_doc":"upload/2164047851516835082drdo_payment.docx","companydoc_img":"","pan_no":"9890989098","panimage":"upload/791772286IMG_20141222_143829.jpg","pan_doc":"","aadhar_doc":"","gst_doc":"","aadhar_no":"8746282738273987","aimage":"upload/570335808IMG_20150214_151814.jpg","gst_no":"88989dh8d9snd","gstimage":"upload/535090499IMG_20150423_070559.jpg","comment":"<p>hgvdghsfdgshdhsdjd<\/p>\r\n\r\n<p>jh fd\\ghsd dgskhd<\/p>\r\n","status":"1"},{"id":"4","client_name":"vishal","contact_pname":"sharma","phone_no":"9709615446","email":"vishal@gmail.com","logo":"upload/758895054download_(1).jpg","address":"sindur hazaribagh","company_doc":"upload/132473627CaseReffereddetail.pdf","companydoc_img":"","pan_no":"9890989098","panimage":"upload/1283749587android_logo_low_quality.jpg","pan_doc":"","aadhar_doc":"","gst_doc":"","aadhar_no":"8746282738273987","aimage":"upload/653780545download.png","gst_no":"88989dh8d9snd","gstimage":"upload/1011179971maxresdefault.jpg","comment":"hgvdghsfdgshdhsdjdjh\nfd\\ghsd\ndgskhd","status":"0"},{"id":"6","client_name":"vishal123","contact_pname":"sharma","phone_no":"9709615446","email":"vishal@gmail.com","logo":"upload/1034954388download.png","address":"sindur hazaribagh","company_doc":"upload/14544799861328279108C.pdf","companydoc_img":"","pan_no":"9890989098","panimage":"upload/189062530d.jpg,upload/1997675755o.png,upload/1740596915a.jpg,upload/972824002o.jpg","pan_doc":"upload/1356846209399280173h.pdf","aadhar_doc":"upload/1979024851399280173h.pdf","gst_doc":"upload/16720115911328279108C.pdf","aadhar_no":"8746282738273987","aimage":"upload/1189460770i.jpg,upload/1811919555m.jpg,upload/840972228d.jpg,upload/561791182o.png","gst_no":"88989dh8d9snd","gstimage":"upload/1692742457d.jpg,upload/408632975a.jpg,upload/2044874100a.jpg,upload/1868760106a.jpg","comment":"hgvdghsfdgshdhsdjdjh\nfd\\ghsd\ndgskhd","status":"0"},{"id":"7","client_name":"vishallll77777","contact_pname":"sharma","phone_no":"9709615446","email":"vishal@gmail.com","logo":"upload/1482317428download.png","address":"sindur hazaribagh","company_doc":"upload/12471599771328279108C.pdf","companydoc_img":"","pan_no":"9890989098","panimage":"upload/711491352a.jpg,upload/1106829136o.jpg","pan_doc":"upload/758167366399280173h.pdf","aadhar_doc":"upload/1622240802399280173h.pdf","gst_doc":"upload/1010408761328279108C.pdf","aadhar_no":"8746282738273987","aimage":"upload/56820988d.jpg,upload/742979935o.png","gst_no":"88989dh8d9snd","gstimage":"upload/974822701a.jpg,upload/170032626a.jpg","comment":"hgvdghsfdgshdhsdjdjh\nfd\\ghsd\ndgskhd","status":"0"},{"id":"8","client_name":"vishallll88767777777","contact_pname":"sharma","phone_no":"9709615446","email":"vishal@gmail.com","logo":"upload/97154271420171205_211831.jpg","address":"sindur hazaribagh","company_doc":"upload/403775314VOLUNTEER-REGISTRATION-APP.PDF","companydoc_img":"","pan_no":"9890989098","panimage":"","pan_doc":"upload/1757385192VOLUNTEER-REGISTRATION-APP.PDF","aadhar_doc":"upload/2119650921VOLUNTEER-REGISTRATION-APP.PDF","gst_doc":"upload/842268741VOLUNTEER-REGISTRATION-APP.PDF","aadhar_no":"8746282738273987","aimage":"","gst_no":"88989dh8d9snd","gstimage":"","comment":"hgvdghsfdgshdhsdjdjh\nfd\\ghsd\ndgskhd","status":"0"},{"id":"9","client_name":"chandan company","contact_pname":"chandan","phone_no":"8789160492","email":"chandanrajshaw@gmail.com","logo":"upload/172580539120171205_211831.jpg","address":"Sindur","company_doc":"","companydoc_img":"upload/7001336722.jpg","pan_no":"12344448790","panimage":"upload/18562524812.jpg","pan_doc":"","aadhar_doc":"","gst_doc":"","aadhar_no":"3245879000075","aimage":"upload/2633970492.jpg","gst_no":"43879999954","gstimage":"upload/189973972.jpg","comment":"abcd","status":"1"},{"id":"10","client_name":"vishallll88","contact_pname":"sharma","phone_no":"9709615446","email":"vishal@gmail.com","logo":"upload/797948611images_(9).jpg","address":"sindur hazaribagh","company_doc":"upload/1837602387html2pdf.pdf","companydoc_img":"upload/1786186602i.jpg,upload/645324002m.jpg","pan_no":"9890989098","panimage":"upload/1308532030i.jpg,upload/387575554m.jpg","pan_doc":"upload/149014444CaseReffereddetail.pdf","aadhar_doc":"upload/1974502266html2pdf_(4).pdf","gst_doc":"upload/358079626html2pdf_(1).pdf","aadhar_no":"8746282738273987","aimage":"upload/622715277i.jpg,upload/1767773881m.jpg","gst_no":"88989dh8d9snd","gstimage":"upload/218920235d.jpg,upload/489912562o.jpg","comment":"hgvdghsfdgshdhsdjdjh\nfd\\ghsd\ndgskhd","status":"0"},{"id":"11","client_name":"vishallll88767777777","contact_pname":"sharma","phone_no":"9709615446","email":"vishal@gmail.com","logo":"upload/838346776images_(9).jpg","address":"sindur hazaribagh","company_doc":"upload/726608328html2pdf.pdf","companydoc_img":"upload/1669865225i.jpg,upload/2112128619m.jpg","pan_no":"9890989098","panimage":"upload/492917884i.jpg,upload/1002025528m.jpg","pan_doc":"upload/1567006777CaseReffereddetail.pdf","aadhar_doc":"upload/1433876270html2pdf_(4).pdf","gst_doc":"upload/1150739858html2pdf_(1).pdf","aadhar_no":"8746282738273987","aimage":"upload/110351657i.jpg,upload/858222913m.jpg","gst_no":"88989dh8d9snd","gstimage":"upload/234317775d.jpg,upload/1558674589o.jpg","comment":"hgvdghsfdgshdhsdjdjh\nfd\\ghsd\ndgskhd","status":"0"},{"id":"12","client_name":"hdjsj","contact_pname":"hdjdj","phone_no":"1234567890","email":"gdhsj","logo":"upload/1204100893driverPic.jpg","address":"hdjdj","company_doc":"upload/1321764200JharkhandHighCourt.pdf","companydoc_img":"","pan_no":"dhjsjs","panimage":"","pan_doc":"upload/332865780JharkhandHighCourt.pdf","aadhar_doc":"upload/1919501084JharkhandHighCourt.pdf","gst_doc":"upload/1639785995JharkhandHighCourt.pdf","aadhar_no":"1234567890123456","aimage":"","gst_no":"hzjsjs","gstimage":"","comment":"hsjs","status":"1"},{"id":"13","client_name":"hdjsj","contact_pname":"hdjdj","phone_no":"1234567890","email":"gdhsj","logo":"upload/1393918917driverPic.jpg","address":"hdjdj","company_doc":"","companydoc_img":"upload/297288114u.jpg,upload/274844507m.jpg,upload/666954612l.jpg","pan_no":"hdjdjd","panimage":"upload/1670862553u.jpg","pan_doc":"","aadhar_doc":"","gst_doc":"","aadhar_no":"1234567890000000","aimage":"upload/226053576u.jpg,upload/1716289377l.jpg","gst_no":"hdjdi","gstimage":"upload/1388164249u.jpg,upload/159289763m.jpg","comment":"hsjs","status":"1"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable {
        /**
         * code : 200
         * message : Added client list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * client_name : vishallll77777
         * contact_pname : sharma
         * phone_no : 9709615446
         * email : vishal@gmail.com
         * logo : upload/1027989719images.jpg
         * address : sindur hazaribagh
         * company_doc : upload/298646218html2pdf_(1).pdf
         * companydoc_img :
         * pan_no : 9890989098
         * panimage : upload/1189460770i.jpg,upload/1811919555m.jpg,upload/2145272703a.jpg,upload/755648654o.jpg
         * pan_doc : upload/1942465113399280173h.pdf
         * aadhar_doc : upload/1836098082399280173h.pdf
         * gst_doc : upload/9708308231328279108C.pdf
         * aadhar_no : 8746282738273987
         * aimage : upload/1189460770i.jpg,upload/1811919555m.jpg,upload/414484212d.jpg,upload/923291323o.png
         * gst_no : 88989dh8d9snd
         * gstimage : upload/1189460770i.jpg,upload/1811919555m.jpg,upload/208455064a.jpg,upload/1791478678a.jpg
         * comment : hgvdghsfdgshdhsdjdjh
         fd\ghsd
         dgskhd
         * status : 0
         */

        private String id;
        private String client_name;
        private String contact_pname;
        private String phone_no;
        private String email;
        private String logo;
        private String address;
        private String company_doc;
        private String companydoc_img;
        private String pan_no;
        private String panimage;
        private String pan_doc;
        private String aadhar_doc;
        private String gst_doc;
        private String aadhar_no;
        private String aimage;
        private String gst_no;
        private String gstimage;
        private String comment;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getClient_name() {
            return client_name;
        }

        public void setClient_name(String client_name) {
            this.client_name = client_name;
        }

        public String getContact_pname() {
            return contact_pname;
        }

        public void setContact_pname(String contact_pname) {
            this.contact_pname = contact_pname;
        }

        public String getPhone_no() {
            return phone_no;
        }

        public void setPhone_no(String phone_no) {
            this.phone_no = phone_no;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCompany_doc() {
            return company_doc;
        }

        public void setCompany_doc(String company_doc) {
            this.company_doc = company_doc;
        }

        public String getCompanydoc_img() {
            return companydoc_img;
        }

        public void setCompanydoc_img(String companydoc_img) {
            this.companydoc_img = companydoc_img;
        }

        public String getPan_no() {
            return pan_no;
        }

        public void setPan_no(String pan_no) {
            this.pan_no = pan_no;
        }

        public String getPanimage() {
            return panimage;
        }

        public void setPanimage(String panimage) {
            this.panimage = panimage;
        }

        public String getPan_doc() {
            return pan_doc;
        }

        public void setPan_doc(String pan_doc) {
            this.pan_doc = pan_doc;
        }

        public String getAadhar_doc() {
            return aadhar_doc;
        }

        public void setAadhar_doc(String aadhar_doc) {
            this.aadhar_doc = aadhar_doc;
        }

        public String getGst_doc() {
            return gst_doc;
        }

        public void setGst_doc(String gst_doc) {
            this.gst_doc = gst_doc;
        }

        public String getAadhar_no() {
            return aadhar_no;
        }

        public void setAadhar_no(String aadhar_no) {
            this.aadhar_no = aadhar_no;
        }

        public String getAimage() {
            return aimage;
        }

        public void setAimage(String aimage) {
            this.aimage = aimage;
        }

        public String getGst_no() {
            return gst_no;
        }

        public void setGst_no(String gst_no) {
            this.gst_no = gst_no;
        }

        public String getGstimage() {
            return gstimage;
        }

        public void setGstimage(String gstimage) {
            this.gstimage = gstimage;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
