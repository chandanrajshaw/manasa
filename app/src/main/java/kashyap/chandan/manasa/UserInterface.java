package kashyap.chandan.manasa;

import java.util.List;

import kashyap.chandan.manasa.branch.AddBranchResponse;
import kashyap.chandan.manasa.branch.BranchListResponse;
import kashyap.chandan.manasa.changepin.ChangePasswordResponse;
import kashyap.chandan.manasa.client.AddClientResponse;
import kashyap.chandan.manasa.client.ClientListResponse;
import kashyap.chandan.manasa.client.EditClientResponse;
import kashyap.chandan.manasa.driver.AddDriverResponse;
import kashyap.chandan.manasa.driver.DriverListResponse;
import kashyap.chandan.manasa.driver.EditDriverResponse;
import kashyap.chandan.manasa.expenses.AddExpenseResponse;
import kashyap.chandan.manasa.expenses.ExpenseListResponse;
import kashyap.chandan.manasa.forgetpassword.ForgotPasswordChangePasswordResponse;
import kashyap.chandan.manasa.forgetpassword.SendOtpResponse;
import kashyap.chandan.manasa.invoice.AddParticularResponse;
import kashyap.chandan.manasa.invoice.ClientInvoicesResponse;
import kashyap.chandan.manasa.invoice.CreateInvoiceResponse;
import kashyap.chandan.manasa.invoice.DeleteInvoiceResponse;
import kashyap.chandan.manasa.invoice.InvoiceBillDetailsResponse;
import kashyap.chandan.manasa.invoice.PrintResponse;
import kashyap.chandan.manasa.login.LoginResponse;
import kashyap.chandan.manasa.lrgenerator.AddLRResponse;
import kashyap.chandan.manasa.lrgenerator.EditLrResponse;
import kashyap.chandan.manasa.lrgenerator.LRListResponse;
import kashyap.chandan.manasa.updateprofile.UpdateProfileResponse;
import kashyap.chandan.manasa.valuespart.DropDownBranchResponse;
import kashyap.chandan.manasa.valuespart.DropDownExpenseTypeResponse;
import kashyap.chandan.manasa.valuespart.DropDownVehicleCompanyResponse;
import kashyap.chandan.manasa.valuespart.DropDownVehicleTypeResponse;
import kashyap.chandan.manasa.miscellaneous.VehicleCompanyListResponse;
import kashyap.chandan.manasa.miscellaneous.VehicleTypeListResponse;
import kashyap.chandan.manasa.vehicle.AddVehicleResponse;
import kashyap.chandan.manasa.vehicle.EditVehicleResponse;
import kashyap.chandan.manasa.vehicle.VehicleListResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface UserInterface {
    /*-----------------update Profile------------------------*/

    @Multipart
    @Headers("x-api-key:mansa@123")
    @POST("Loginservice/updateprofile")
    Call<UpdateProfileResponse>updateProfile(@Part("name") RequestBody name,
                                             @Part("email") RequestBody email,
                                             @Part("mobile") RequestBody mobile,
                                             @Part MultipartBody.Part profileImage);
    /*-------------------------forgetPassword--------------------*/
    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Loginservice/sendotp")
    Call<SendOtpResponse>sendOtp(@Field("email") String username);

    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Loginservice/forpass")
    Call<ForgotPasswordChangePasswordResponse>resetPassword(@Field("otp_no") String otp,@Field("newpass") String newPassword);
    /*------------------Login-----------------------*/
    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Loginservice/changepass")
    Call<ChangePasswordResponse>changePassword(@Field("email") String username, @Field("oldpass") String oldPassword, @Field("newpass") String newPassword);

    /*------------------------change Password---------------------*/
    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Loginservice/login")
    Call<LoginResponse>login(@Field("email") String username, @Field("password") String password);
/*---------------------------Driver Service--------------------------*/
    @Headers("x-api-key:mansa@123")
    @GET("Driversservice/driverslist")
    Call<DriverListResponse> driverList();

    @Multipart
    @Headers("x-api-key:mansa@123")
    @POST("Driversservice/add_drivers")
    Call<AddDriverResponse>addDriver(@Part(" driver_name") RequestBody fname,//  driver_name, , , , , , , , , , , ,
                                     @Part("account_no") RequestBody account_no,//account_no
                                     @Part("phone_no") RequestBody phone,//phone_no
                                     @Part(" ifsc_code") RequestBody  ifsc_code,//ifsc_code
//                                     @Part("address") RequestBody address,
                                     @Part("join_date") RequestBody date,//join_date
                                     @Part("branch_id") RequestBody branch,//branch_id
                                     @Part("comment") RequestBody comment,//comment
                                     @Part("status") RequestBody status,//status
                                     @Part MultipartBody.Part image,//pimage
//                                     @Part MultipartBody.Part aadharpdffile,//aadhar_doc
//                                     @Part MultipartBody.Part liscensepdffile,//liscence_doc
                                     @Part MultipartBody.Part aadharimagefile,//aimage
                                     @Part MultipartBody.Part liscenseimagefile);//limage



    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Driversservice/edit_drivers")
    Call<EditDriverResponse>editDriver(@Field("id") String id, @Field("status") String status);
    /*---------------------------------------------------------*/
    /*------------------------------Vehicle List--------------------------*/
    @Headers("x-api-key:mansa@123")
    @GET("Vehiclemangservice/vehiclemanglist")
    Call<VehicleListResponse> vehicleList();

    @Multipart
    @Headers("x-api-key:mansa@123")
    @POST("Vehiclemangservice/add_vehiclemang")
    Call<AddVehicleResponse>addVehicle(@Part("vehicle_number") RequestBody number,
                                       @Part("vehicle_code") RequestBody code,
                                       @Part("capacity") RequestBody capacity,
                                       @Part("vehicletype_id") RequestBody type,
                                       @Part("branch_id") RequestBody branch,
                                       @Part("purchased_date") RequestBody date,
                                       @Part("status") RequestBody status,
                                       @Part("vehiclecompany_id") RequestBody company,
                                       @Part MultipartBody.Part pdffile,
                                       @Part List<MultipartBody.Part> imagefile);
    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Vehiclemangservice/edit_vehiclemang")
    Call<EditVehicleResponse>editVehicle(@Field("id") String id, @Field("status") String status);

    /*------------------------------------Client---------------------------*/
    @Headers("x-api-key:mansa@123")
    @GET("Clientsservice/clientslist")
    Call<ClientListResponse> clientList();

    @Multipart
    @Headers("x-api-key:mansa@123")
    @POST("Clientsservice/add_clients")
    Call<AddClientResponse>addClient(@Part("client_name") RequestBody clientName,
                                      @Part("contact_pname") RequestBody name,
                                      @Part("phone_no") RequestBody phone,
                                      @Part("email") RequestBody email,
                                      @Part("address") RequestBody address,
                                      @Part MultipartBody.Part logo,
                                      @Part MultipartBody.Part companydocPdf,
                                     @Part MultipartBody.Part panCardPdf,
                                     @Part MultipartBody.Part aadharCardPdf,
                                     @Part MultipartBody.Part gstDocumentPdf,
                                      @Part List<MultipartBody.Part> companyDoc,
                                     @Part List<MultipartBody.Part> panDoc,
                                     @Part List<MultipartBody.Part> aadharDoc,
                                     @Part List<MultipartBody.Part> gstDoc,
                                     @Part("pan_no") RequestBody pan_no,
                                     @Part("aadhar_no") RequestBody aadhar_no,
                                     @Part("gst_no") RequestBody gst_no,
                                     @Part("comment") RequestBody comment,
                                     @Part("status") RequestBody status);

    @Multipart
    @Headers("x-api-key:mansa@123")
    @POST("Clientsservice/edit_clients")
    Call<EditClientResponse> updateClient(@Part("id") RequestBody id,
                                          @Part("client_name") RequestBody clientName,
                                          @Part("contact_pname") RequestBody name,
                                          @Part("phone_no") RequestBody phone,
                                          @Part("email") RequestBody email,
                                          @Part("address") RequestBody address,
                                          @Part MultipartBody.Part logo,
                                          @Part MultipartBody.Part companydocPdf,
                                          @Part MultipartBody.Part panCardPdf,
                                          @Part MultipartBody.Part aadharCardPdf,
                                          @Part MultipartBody.Part gstDocumentPdf,
                                          @Part List<MultipartBody.Part> companyDoc,
                                          @Part List<MultipartBody.Part> panDoc,
                                          @Part List<MultipartBody.Part> aadharDoc,
                                          @Part List<MultipartBody.Part> gstDoc,
                                          @Part("pan_no") RequestBody pan_no,
                                          @Part("aadhar_no") RequestBody aadhar_no,
                                          @Part("gst_no") RequestBody gst_no,
                                          @Part("comment") RequestBody comment,
                                          @Part("status") RequestBody status);
    /*------------------------------------Branch---------------------------*/
    @Headers("x-api-key:mansa@123")
    @GET("Branchservice/branchlist")
    Call<BranchListResponse> branchList();

    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Branchservice/add_branch")
    Call<AddBranchResponse>addBranch(@Field("branch_id") String branchId, @Field("branch_aname") String adminName,
                                     @Field("phone_no") String phone,
                                     @Field("email") String email,
                                     @Field("address") String address,@Field("bpassword") String bpassword,
                                     @Field("cpassword") String cpassword);
    /*--------------------------------Expense----------------------------*/
    @Headers("x-api-key:mansa@123")
    @GET("Expenseservice/expenselist")
    Call<ExpenseListResponse> expenseList();

    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Expenseservice/add_expense")
    Call<AddExpenseResponse>addExpense(@Field("branch_id") String branchId,
                                       @Field("expensetype_id") String expenseId,
                                       @Field("edate") String date,
                                       @Field("remark") String remark,
                                       @Field("expense_ammount") String amount);
    /*----------------------L-R---------------------------*/
    @Headers("x-api-key:mansa@123")
    @GET("Lrgeneratorservice/lrgeneratorlist")
    Call<LRListResponse> lrList();

    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Lrgeneratorservice/add_lrgenerator")
    Call<AddLRResponse>addLR(@Field("lr_no") String lr_no,
            @Field("lrgen_date") String date,
                             @Field("vehicletype_id") String vehicleId,
                             @Field("consignor") String consignor,
                             @Field("consignee") String consignee,
                             @Field("lr_from") String from,
                             @Field("lr_to") String to,
                             @Field("material_desc") String matrialDesc,
                             @Field("package") String materialType,
                             @Field("party_inv_no") String invoice,
                             @Field("actual_wt") String weight,
                             @Field("charge_wt") String charge,
                             @Field("rate") String rate,
                             @Field("value_good") String value,
                             @Field("payment_mode") String payment,
                             @Field("remark") String remark,
                             @Field("vehicle_no") String vehicle_no);
    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Lrgeneratorservice/edit_lrgenerator")
    Call<EditLrResponse>editLR(@Field("lr_no") String lr_no,
            @Field("id") String id,
                                @Field("lrgen_date") String date,
                               @Field("vehicletype_id") String vehicleId,
                               @Field("consignor") String consignor,
                               @Field("consignee") String consignee,
                               @Field("lr_from") String from,
                               @Field("lr_to") String to,
                               @Field("material_desc") String matrialDesc,
                               @Field("package") String materialType,
                               @Field("party_inv_no") String invoice,
                               @Field("actual_wt") String weight,
                               @Field("charge_wt") String charge,
                               @Field("rate") String rate,
                               @Field("value_good") String value,
                               @Field("payment_mode") String payment,
                               @Field("remark") String remark,
                               @Field("vehicle_no") String vehicle_no);
    /*------------------------Invoice---------------------------------------------*/
    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Invoiceservice/add_invoice")
    Call<CreateInvoiceResponse>createInvoice(@Field("invoice_date") String date,
                                          @Field("client_id") String clientId);
    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Invoiceservice/clientinvoice")
    Call<ClientInvoicesResponse>clientInvoices(@Field("client_id") String clientId);

    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Invoicebillservice/invoicebilllist")
    Call<InvoiceBillDetailsResponse>invoiceParticular(@Field("id") String invoiceId);

    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Invoiceservice/delete_invoice")
    Call<DeleteInvoiceResponse>deleteInvoice(@Field("id") String invoiceId);

    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Invoicebillservice/add_invoicebill")
    Call<AddParticularResponse>addBill(@Field("inv_id") String invoice_Id,
                                       @Field("particulars[]") List<String> particulars,
                                       @Field("weight[]") List<String> weight,
                                       @Field("rate[]") List<String> rate,
                                       @Field("price[]") List<String> price);

    @FormUrlEncoded
    @Headers("x-api-key:mansa@123")
    @POST("Invoiceservice/invoicebillpdf")
    Call<PrintResponse>printInvoice(@Field("id") String invoice_Id);

    /*---------------------------Miscellaneous---------------------------------------*/
    @Headers("x-api-key:mansa@123")
    @GET("Vehicletypeservice/vehicletypelist")
    Call<VehicleTypeListResponse> vehicleTypeList();

    @Headers("x-api-key:mansa@123")
    @GET("Vehiclecompanyservice/vehiclecompanylist")
    Call<VehicleCompanyListResponse> vehicleCompanyList();
    /*---------------------------values-----------------------------*/
    @Headers("x-api-key:mansa@123")
    @GET("Branchesservice/branchlist")
    Call<DropDownBranchResponse> dropDownBranchList();

    @Headers("x-api-key:mansa@123")
    @GET("Vehicletypeservice/vehicletypelist")
    Call<DropDownVehicleTypeResponse> dropDownvehicleTypeList();

    @Headers("x-api-key:mansa@123")
    @GET("Vehiclecompanyservice/vehiclecompanylist")
    Call<DropDownVehicleCompanyResponse> dropDownvehicleCompanyList();

    @Headers("x-api-key:mansa@123")
    @GET("Expensetypeservice/expensetypelist")
    Call<DropDownExpenseTypeResponse> dropDownExpenseTypeList();
}
