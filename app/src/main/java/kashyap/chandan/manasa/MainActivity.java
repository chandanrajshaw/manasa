package kashyap.chandan.manasa;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import kashyap.chandan.manasa.branch.BranchList;
import kashyap.chandan.manasa.changepin.ChangePassword;
import kashyap.chandan.manasa.client.ClientList;
import kashyap.chandan.manasa.client.ClientListAdapter;
import kashyap.chandan.manasa.client.ClientListResponse;
import kashyap.chandan.manasa.driver.DriverList;
import kashyap.chandan.manasa.expenses.ExpenseList;
import kashyap.chandan.manasa.invoice.ClientInvoiceList;
import kashyap.chandan.manasa.login.Login;
import kashyap.chandan.manasa.lrgenerator.LrList;
import kashyap.chandan.manasa.updateprofile.UpdateProfile;
import kashyap.chandan.manasa.vehicle.VehicleList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    boolean doubleBackToExitPressedOnce = false;
Toolbar toolbar;
DrawerLayout drawerlayout;
RecyclerView clientsList;
    RelativeLayout editProfile,home,changePassword,vehicleManagement,driverManagement,clientManagement,invoiceManagement,lrManagement,branchmanagement,expenseManagement;
TextView tvTitle,logout,adminMobile,adminName;
SharedPreferences sharedPreferences;
CircleImageView profileImage,ivProfile;
String image,name,mobile;
    List<ClientListResponse.DataBean> clients=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        init();
        sharedPreferences=getSharedPreferences("Login",MODE_PRIVATE);
        image=sharedPreferences.getString("image",null);
        mobile=sharedPreferences.getString("mobile",null);
        name=sharedPreferences.getString("name",null);
        Picasso.get().load("http://www.igranddeveloper.xyz/mansamover/"+image)
                .placeholder(R.drawable.loading).error(R.drawable.terms).into(profileImage);
        Picasso.get().load("http://www.igranddeveloper.xyz/mansamover/"+image)
                .placeholder(R.drawable.loading).error(R.drawable.terms).into(ivProfile);
        adminMobile.setText(mobile);
        adminName.setText(name);
        tvTitle.setText("Manasa Packers & Movers");
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.list_view);
        actionBar.setDisplayHomeAsUpEnabled(true);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle
                (this, drawerlayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerlayout.addDrawerListener(toggle);
        toggle.syncState();
        vehicleManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, VehicleList.class);
                startActivity(intent);
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });

        driverManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, DriverList.class);
                startActivity(intent);
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        clientManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, ClientList.class);
                startActivity(intent);
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        invoiceManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, ClientInvoiceList.class);
                startActivity(intent);
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        lrManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, LrList.class);
                startActivity(intent);
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        branchmanagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, BranchList.class);
                startActivity(intent);
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        expenseManagement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, ExpenseList.class);
                startActivity(intent);
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharedPreferences=getSharedPreferences("Login",MODE_PRIVATE);
                SharedPreferences.Editor editor=sharedPreferences.edit();
                editor.clear();
                editor.commit();
                Intent intent=new Intent(MainActivity.this, Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, ChangePassword.class);
                startActivity(intent);
                drawerlayout.closeDrawer(GravityCompat.START);
            }
        });
        final Dialog progressDialog=new Dialog(MainActivity.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<ClientListResponse> call=userInterface.clientList();
        call.enqueue(new Callback<ClientListResponse>() {
            @Override
            public void onResponse(Call<ClientListResponse> call, Response<ClientListResponse> response) {
                if (response.code()==200)
                {
                    clients=response.body().getData();
                    clientsList.setLayoutManager(new LinearLayoutManager(MainActivity.this,LinearLayoutManager.VERTICAL,false));
                    clientsList.setAdapter(new ClientListAdapter(MainActivity.this,clients));
                    progressDialog.dismiss();
                }
                else
                {
                    progressDialog.dismiss();
                    Snackbar.make(MainActivity.this.getWindow().getDecorView().findViewById(android.R.id.content),"Client List Not available",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ClientListResponse> call, Throwable t) {
progressDialog.dismiss();
                Toast.makeText(MainActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
editProfile.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent intent=new Intent(MainActivity.this, UpdateProfile.class);
        startActivity(intent);
        drawerlayout.closeDrawer(GravityCompat.START);
    }
});
    }
    private void init()
    {
        ivProfile=findViewById(R.id.ivProfile);
        profileImage=findViewById(R.id.adminProfilePicture);
        adminMobile=findViewById(R.id.profilePhone);
        adminName=findViewById(R.id.profileName);
        clientsList=findViewById(R.id.clientsList);
        changePassword=findViewById(R.id.changePin);
        expenseManagement=findViewById(R.id.nav_expense_management);
        branchmanagement=findViewById(R.id.nav_branches_management);
        lrManagement=findViewById(R.id.nav_LR_management);
        driverManagement=findViewById(R.id.nav_driver_management);
        vehicleManagement=findViewById(R.id.nav_vehicle_management);
        clientManagement=findViewById(R.id.nav_client);
        invoiceManagement=findViewById(R.id.nav_Invoice_management);
        toolbar = findViewById(R.id.toolbar);
        tvTitle=findViewById(R.id.tvTitle);
        drawerlayout=findViewById(R.id.drawerlayout);
        logout=findViewById(R.id.logout);
        home=findViewById(R.id.nav_home_layout);
        editProfile=findViewById(R.id.editProfile);
    }
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;

        Snackbar.make(this.getWindow().getDecorView().findViewById(android.R.id.content), "Please click BACK again to exit", Snackbar.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);

    }
//    @Override
//    public void onWindowFocusChanged(boolean hasFocus) {
//        super.onWindowFocusChanged(hasFocus);
//
//        if (hasFocus) {
//            hideSystemUI();
//        }
//        else if (!hasFocus)
//        {
//            showSystemUI();
//        }
//
//
//    }
//    private void hideSystemUI() {
//        // Enables regular immersive mode.
//        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
//        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
//        View decorView = getWindow().getDecorView();
//        decorView.setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_IMMERSIVE
//                        // Set the content to appear under the system bars so that the
//                        // content doesn't resize when the system bars hide and show.
//                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                        // Hide the nav bar and status bar
//                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
////                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
//    }
//
//    // Shows the system bars by removing all the flags
//// except for the ones that make the content appear under the system bars.
//    private void showSystemUI() {
//        View decorView = getWindow().getDecorView();
//        decorView.setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//    }

}
