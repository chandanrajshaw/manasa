package kashyap.chandan.manasa;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import kashyap.chandan.manasa.vehicle.EditVehicle;

public class StatusAdapter extends RecyclerView.Adapter<StatusAdapter.MyViewHolder> {
    Context context;
    ArrayList<String> status;
    CustomItemClickListener listener;
    Dialog statusDialog;
    private int mSelectedItem = -1;
    public StatusAdapter(Context context, ArrayList<String> status, Dialog statusDialog, CustomItemClickListener listener) {
        this.context=context;
        this.listener=listener;
        this.status=status;
        this.statusDialog=statusDialog;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.option,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == mSelectedItem);
        holder.tvStatus.setText(status.get(position));

        TextView ok= statusDialog.findViewById(R.id.ok);
        TextView dialogheader=statusDialog.findViewById(R.id.dialogheader);
        dialogheader.setText("Status");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedItem==-1)
                { Toast.makeText(context, "Select Status", Toast.LENGTH_SHORT).show(); }
                else
                { statusDialog.dismiss(); }
            }
        });
    }

    @Override
    public int getItemCount() {
        return status.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvStatus;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvStatus=itemView.findViewById(R.id.item);
            radioselect=itemView.findViewById(R.id.selected);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    listener.onItemClick(v,mSelectedItem,status.get(mSelectedItem));
                }

            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}
