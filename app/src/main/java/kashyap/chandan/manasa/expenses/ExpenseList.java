package kashyap.chandan.manasa.expenses;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.client.ClientList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExpenseList extends AppCompatActivity {
    RecyclerView expenseList;
    TextView toolHeader,submit;
    ImageView goBack;
    FloatingActionButton addExpense;
    List<ExpenseListResponse.DataBean> expenses=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_expense_list);
        init();
        final Dialog progressDialog=new Dialog(ExpenseList.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        submit.setVisibility(View.GONE);
        toolHeader.setText("Expense List");
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        addExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ExpenseList.this, ExpenseManagement.class);
                startActivity(intent);
            }
        });
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<ExpenseListResponse> call=userInterface.expenseList();
        call.enqueue(new Callback<ExpenseListResponse>() {
            @Override
            public void onResponse(Call<ExpenseListResponse> call, Response<ExpenseListResponse> response) {
                if (response.code()==200)
                {
                    expenses=response.body().getData();
                    expenseList.setLayoutManager(new LinearLayoutManager(ExpenseList.this,LinearLayoutManager.VERTICAL,false));
                    expenseList.setAdapter(new ExpenseListAdapter(ExpenseList.this,expenses));
                    progressDialog.dismiss();
                }
                else
                {
                    progressDialog.dismiss();
                    Snackbar.make(ExpenseList.this.getWindow().getDecorView().findViewById(android.R.id.content)," No Expense available",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ExpenseListResponse> call, Throwable t) {
                progressDialog.dismiss();
                Snackbar.make(ExpenseList.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();

            }
        });

    }
    void init()
    {
        expenseList =findViewById(R.id.expenseList);
        goBack=findViewById(R.id.toolgoback);
        addExpense =findViewById(R.id.addExpense);
        toolHeader=findViewById(R.id.toolgenheader);
        submit=findViewById(R.id.toolsubmit);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Dialog progressDialog=new Dialog(ExpenseList.this);
        progressDialog.setContentView(R.layout.loadingdialog);
        progressDialog.setCancelable(false);
        progressDialog.show();
        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
        Call<ExpenseListResponse> call=userInterface.expenseList();
        call.enqueue(new Callback<ExpenseListResponse>()
        {
            @Override
            public void onResponse(Call<ExpenseListResponse> call, Response<ExpenseListResponse> response) {
                if (response.code()==200)
                {
                    expenses=response.body().getData();
                    expenseList.setLayoutManager(new LinearLayoutManager(ExpenseList.this,LinearLayoutManager.VERTICAL,false));
                    expenseList.setAdapter(new ExpenseListAdapter(ExpenseList.this,expenses));
                    progressDialog.dismiss();
                }
                else
                {
                    progressDialog.dismiss();
                    Snackbar.make(ExpenseList.this.getWindow().getDecorView().findViewById(android.R.id.content),"Driver List Not available",Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ExpenseListResponse> call, Throwable t) {
                progressDialog.dismiss();
                Snackbar.make(ExpenseList.this.getWindow().getDecorView().findViewById(android.R.id.content),""+t.getMessage(),Snackbar.LENGTH_LONG).show();

            }
        });
    }
}
