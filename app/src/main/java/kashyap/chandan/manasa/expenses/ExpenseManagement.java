package kashyap.chandan.manasa.expenses;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.adapters.BranchAdapter;
import kashyap.chandan.manasa.adapters.ExpenseAdapter;
import kashyap.chandan.manasa.lrgenerator.RLManagement;
import kashyap.chandan.manasa.valuespart.DropDownBranchResponse;
import kashyap.chandan.manasa.valuespart.DropDownExpenseTypeResponse;
import kashyap.chandan.manasa.vehicle.VehicleManagement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExpenseManagement extends AppCompatActivity {
    ImageView goBack;
    DatePickerDialog.OnDateSetListener dateDialog;
    Calendar myCalendar;
    RelativeLayout branchlayout,datelayout,expenselayout;
    TextView tvdate,tvbranch,tvtoolheader,tvSubmit,tvExpense;
    RecyclerView branchRecycler,expenseRecycler;
    String branchcode,expenseCode;
    TextInputEditText etExpense,etRemark;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_expense_management);
        init();
        tvtoolheader.setText(" Add Expense");
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        datelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(ExpenseManagement.this,R.style.TimePickerTheme,dateDialog, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        dateDialog=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        branchlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final  Dialog progress=new Dialog(ExpenseManagement.this);
                progress.setContentView(R.layout.loadingdialog);
                progress.setCancelable(false);
                progress.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<DropDownBranchResponse> call=userInterface.dropDownBranchList();
                call.enqueue(new Callback<DropDownBranchResponse>() {
                    @Override
                    public void onResponse(Call<DropDownBranchResponse> call, Response<DropDownBranchResponse> response) {
                        if (response.code()==200)
                        {
                            progress.dismiss();
                            final Dialog branchDialog=new Dialog(ExpenseManagement.this);
                            branchDialog.setContentView(R.layout.recyclerdialog);
                            branchDialog.setCancelable(true);
                            DisplayMetrics metrics = getResources().getDisplayMetrics();
                            int width = metrics.widthPixels;
                            branchRecycler=branchDialog.findViewById(R.id.recycleroption);
                            branchDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                            List<DropDownBranchResponse.DataBean> branch=new ArrayList<>();
                            branch=response.body().getData();
                            branchRecycler.setLayoutManager(new LinearLayoutManager(ExpenseManagement.this,LinearLayoutManager.VERTICAL,false));
                            branchRecycler.setAdapter(new BranchAdapter(ExpenseManagement.this,branch,branchDialog,new CustomItemClickListener() {
                                @Override
                                public void onItemClick(View v, int position, String value) {
                                    tvbranch.setText(value);
                                    branchcode=String.valueOf(position);
                                }
                            }));
                            branchDialog.show();
                        }
                        else
                        {
                            progress.dismiss();
                            Snackbar.make(ExpenseManagement.this.getWindow().getDecorView().findViewById(android.R.id.content),"Branch Not available",Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DropDownBranchResponse> call, Throwable t) {
                        Toast.makeText(ExpenseManagement.this, ""+t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        expenselayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final  Dialog progress=new Dialog(ExpenseManagement.this);
                progress.setContentView(R.layout.loadingdialog);
                progress.setCancelable(false);
                progress.show();
                UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                Call<DropDownExpenseTypeResponse>call=userInterface.dropDownExpenseTypeList();
                call.enqueue(new Callback<DropDownExpenseTypeResponse>() {
                    @Override
                    public void onResponse(Call<DropDownExpenseTypeResponse> call, Response<DropDownExpenseTypeResponse> response) {
                       if (response.code()==200)
                       {
                           progress.dismiss();
                           final Dialog expenseDialog=new Dialog(ExpenseManagement.this);
                           expenseDialog.setContentView(R.layout.recyclerdialog);
                           expenseDialog.setCancelable(true);
                           DisplayMetrics metrics = getResources().getDisplayMetrics();
                           int width = metrics.widthPixels;
                           expenseRecycler=expenseDialog.findViewById(R.id.recycleroption);
                           expenseDialog.getWindow().setLayout(width, LinearLayout.LayoutParams.MATCH_PARENT);
                           List<DropDownExpenseTypeResponse.DataBean> expense=new ArrayList<>();
                           expense=response.body().getData();
                           expenseRecycler.setLayoutManager(new LinearLayoutManager(ExpenseManagement.this,LinearLayoutManager.VERTICAL,false));
                           expenseRecycler.setAdapter(new ExpenseAdapter(ExpenseManagement.this,expense,expenseDialog,new CustomItemClickListener() {
                               @Override
                               public void onItemClick(View v, int position, String value) {
                                   tvExpense.setText(value);
                                   expenseCode=String.valueOf(position);
                               }
                           }));
                           expenseDialog.show();
                       }
                    else {
                        progress.dismiss();
                           Toast.makeText(ExpenseManagement.this, "List Not Available", Toast.LENGTH_SHORT).show();
                       }
                    }

                    @Override
                    public void onFailure(Call<DropDownExpenseTypeResponse> call, Throwable t) {
progress.dismiss();
                        Toast.makeText(ExpenseManagement.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String branch=tvbranch.getText().toString();
                String exptype=tvExpense.getText().toString();
                String date=tvdate.getText().toString();
                String expense=etExpense.getText().toString();
                String remark=etRemark.getText().toString();
                if (branch.equalsIgnoreCase("Branch")&&date.equalsIgnoreCase("Date")&&
                exptype.equalsIgnoreCase("Expense Type")&&expense.isEmpty())
                    Toast.makeText(ExpenseManagement.this, "Fill All Fields", Toast.LENGTH_SHORT).show();
                else if (branch.equalsIgnoreCase("Branch"))
                    Toast.makeText(ExpenseManagement.this, "Select Branch", Toast.LENGTH_SHORT).show();
                else if (date.equalsIgnoreCase("Date"))
                    Toast.makeText(ExpenseManagement.this, "Select Expense Type", Toast.LENGTH_SHORT).show();
                else if (expense.isEmpty())
                    Toast.makeText(ExpenseManagement.this, "Enter Amount", Toast.LENGTH_SHORT).show();
                else {
                    final Dialog progress=new Dialog(ExpenseManagement.this);
                    progress.setContentView(R.layout.loadingdialog);
                    progress.setCancelable(false);
                    progress.show();
                    UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
Call<AddExpenseResponse>call=userInterface.addExpense(branchcode,expenseCode,date,remark,expense);
call.enqueue(new Callback<AddExpenseResponse>() {
    @Override
    public void onResponse(Call<AddExpenseResponse> call, Response<AddExpenseResponse> response) {
        if (response.code()==200)
        {
            progress.dismiss();
            Toast.makeText(ExpenseManagement.this, "Expense Added", Toast.LENGTH_SHORT).show();
            Intent intent=new Intent(ExpenseManagement.this,ExpenseList.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
        else {
            progress.dismiss();
            Toast.makeText(ExpenseManagement.this, "Expense Not Added", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Call<AddExpenseResponse> call, Throwable t) {
        progress.dismiss();
        Toast.makeText(ExpenseManagement.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
    }
});
                }
            }
        });
    }
    void init()
    {
        etExpense=findViewById(R.id.etExpense);
        etRemark=findViewById(R.id.etRemark);
        branchlayout=findViewById(R.id.branchlay);
        tvbranch=findViewById(R.id.tvBranch);
        expenselayout=findViewById(R.id.expenselay);
        tvExpense=findViewById(R.id.tvExpense);
        myCalendar=Calendar.getInstance();
        tvtoolheader=findViewById(R.id.toolgenheader);
        goBack=findViewById(R.id.toolgoback);
        tvdate=findViewById(R.id.tvDate);
        datelayout=findViewById(R.id.datelay);
        tvSubmit=findViewById(R.id.toolsubmit);
    }
    private void updateLabel() {

        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        tvdate.setText(sdf.format(myCalendar.getTime()));
    }
}
