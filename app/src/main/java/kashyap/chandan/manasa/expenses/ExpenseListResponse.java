package kashyap.chandan.manasa.expenses;

import java.io.Serializable;
import java.util.List;

public class ExpenseListResponse implements Serializable {

    /**
     * status : {"code":200,"message":"expense list"}
     * data : [{"id":"1","branch_id":"1","expensetype_id":"3","edate":"02-02-2010","remark":"<p>gdhjgdj<\/p>\r\n\r\n<p>dfshdgsa<\/p>\r\n\r\n<p>&nbsp;<\/p>\r\n","expense_ammount":"2000","branches_name":"New-delhi","expensetype_name":"train and plane tickets"},{"id":"2","branch_id":"2","expensetype_id":"2","edate":"12-12-1998","remark":"cvjcdhjjgdfhjbgjhgd\nfghgd\nfvdsj","expense_ammount":"1000 Rupees","branches_name":"Hydrabad","expensetype_name":"taxi receipts"}]
     */

    private StatusBean status;
    private List<DataBean> data;

    public StatusBean getStatus() {
        return status;
    }

    public void setStatus(StatusBean status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class StatusBean implements Serializable{
        /**
         * code : 200
         * message : expense list
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1
         * branch_id : 1
         * expensetype_id : 3
         * edate : 02-02-2010
         * remark : <p>gdhjgdj</p>

         <p>dfshdgsa</p>

         <p>&nbsp;</p>
         * expense_ammount : 2000
         * branches_name : New-delhi
         * expensetype_name : train and plane tickets
         */

        private String id;
        private String branch_id;
        private String expensetype_id;
        private String edate;
        private String remark;
        private String expense_ammount;
        private String branches_name;
        private String expensetype_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBranch_id() {
            return branch_id;
        }

        public void setBranch_id(String branch_id) {
            this.branch_id = branch_id;
        }

        public String getExpensetype_id() {
            return expensetype_id;
        }

        public void setExpensetype_id(String expensetype_id) {
            this.expensetype_id = expensetype_id;
        }

        public String getEdate() {
            return edate;
        }

        public void setEdate(String edate) {
            this.edate = edate;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getExpense_ammount() {
            return expense_ammount;
        }

        public void setExpense_ammount(String expense_ammount) {
            this.expense_ammount = expense_ammount;
        }

        public String getBranches_name() {
            return branches_name;
        }

        public void setBranches_name(String branches_name) {
            this.branches_name = branches_name;
        }

        public String getExpensetype_name() {
            return expensetype_name;
        }

        public void setExpensetype_name(String expensetype_name) {
            this.expensetype_name = expensetype_name;
        }
    }
}
