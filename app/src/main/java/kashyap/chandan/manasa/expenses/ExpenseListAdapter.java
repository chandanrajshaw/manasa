package kashyap.chandan.manasa.expenses;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.manasa.R;

class ExpenseListAdapter extends RecyclerView.Adapter<ExpenseListAdapter.MyViewHolder> {
    Context context;
    List<ExpenseListResponse.DataBean> expenses;
    public ExpenseListAdapter(Context context, List<ExpenseListResponse.DataBean> expenses) {
        this.context=context;
        this.expenses=expenses;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.expenselist,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
holder.branch.setText(expenses.get(position).getBranches_name());
holder.date.setText(expenses.get(position).getEdate());
holder.amount.setText(expenses.get(position).getExpense_ammount());
holder.type.setText(expenses.get(position).getExpensetype_name());
    }

    @Override
    public int getItemCount() {
        return expenses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView branch,date,amount,type;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            branch=itemView.findViewById(R.id.branch);
            date=itemView.findViewById(R.id.date);
            amount=itemView.findViewById(R.id.amount);
            type=itemView.findViewById(R.id.expensetype);
        }
    }
}
