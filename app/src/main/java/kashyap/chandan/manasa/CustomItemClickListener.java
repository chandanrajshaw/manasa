package kashyap.chandan.manasa;

import android.view.View;

public interface CustomItemClickListener {
    public void onItemClick(View v, int position, String value);
}
