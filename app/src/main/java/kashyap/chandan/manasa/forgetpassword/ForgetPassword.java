package kashyap.chandan.manasa.forgetpassword;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.lang.annotation.Annotation;

import kashyap.chandan.manasa.ApiClient;
import kashyap.chandan.manasa.ApiError;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.UserInterface;
import kashyap.chandan.manasa.changepin.ChangePassword;
import kashyap.chandan.manasa.login.Login;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;

public class ForgetPassword extends AppCompatActivity {
    TextView changePassword,toolSave,toolHeader;
    TextInputEditText etOtp,etNewPassword,etConfirmPassword;
    ImageView goBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        init();
        toolHeader.setText("Change Password");
        toolSave.setVisibility(View.GONE);
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                overridePendingTransition(R.anim.transitionlefttoright,R.anim.transitionlefttoright);
                finish();
            }
        });
        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String otp=etOtp.getText().toString().trim();
                String newPassword=etNewPassword.getText().toString().trim();
                String confirmPassword=etConfirmPassword.getText().toString().trim();
                if (otp.isEmpty()&&newPassword.isEmpty()&&confirmPassword.isEmpty())
                    Toast.makeText(ForgetPassword.this, "Enter All Fields", Toast.LENGTH_SHORT).show();
                else if (otp.isEmpty())
                    Toast.makeText(ForgetPassword.this, "Enter OTP", Toast.LENGTH_SHORT).show();
                else if (newPassword.isEmpty())
                    Toast.makeText(ForgetPassword.this, "Enter New Password", Toast.LENGTH_SHORT).show();
                else if (confirmPassword.isEmpty())
                    Toast.makeText(ForgetPassword.this, "Confirm Password", Toast.LENGTH_SHORT).show();
                else if (!newPassword.equals(confirmPassword))
                    Toast.makeText(ForgetPassword.this, "Confirm Password Mismatch", Toast.LENGTH_SHORT).show();
                else
                    {
                        final Dialog progressdialog=new Dialog(ForgetPassword.this);
                        progressdialog.setContentView(R.layout.loadingdialog);
                        progressdialog.setCancelable(false);
                        progressdialog.show();
                        UserInterface userInterface= ApiClient.getClient().create(UserInterface.class);
                        Call<ForgotPasswordChangePasswordResponse>call=userInterface.resetPassword(otp,confirmPassword);
                        call.enqueue(new Callback<ForgotPasswordChangePasswordResponse>() {
                            @Override
                            public void onResponse(Call<ForgotPasswordChangePasswordResponse> call, Response<ForgotPasswordChangePasswordResponse> response) {
                                if (response.code()==200)
                                {
                                    progressdialog.dismiss();
                                    Toast.makeText(ForgetPassword.this, "Password Changed Successfully", Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(ForgetPassword.this, Login.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                                else
                                {
                                    progressdialog.dismiss();
                                    Converter<ResponseBody, ApiError> converter =
                                            ApiClient.getClient().responseBodyConverter(ApiError.class,new Annotation[0]);
                                    ApiError error;
                                    try {
                                        error = converter.convert(response.errorBody());
                                        ApiError.StatusBean status=error.getStatus();
                                        Toast.makeText(ForgetPassword.this, ""+status.getMessage(), Toast.LENGTH_LONG).show();
                                    } catch (IOException e) { e.printStackTrace(); }
                                }
                            }

                            @Override
                            public void onFailure(Call<ForgotPasswordChangePasswordResponse> call, Throwable t) {
progressdialog.dismiss();
                                Toast.makeText(ForgetPassword.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
            }
        });
    }
   private void init()
    {
        toolHeader=findViewById(R.id.toolgenheader);
        toolSave=findViewById(R.id.toolsubmit);
        goBack=findViewById(R.id.toolgoback);
        changePassword=findViewById(R.id.changePin);
        etConfirmPassword=findViewById(R.id.etConfirmPassword);
        etOtp=findViewById(R.id.etotp);
        etNewPassword=findViewById(R.id.etNewPassword);
    }
}
