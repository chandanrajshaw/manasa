package kashyap.chandan.manasa.adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.valuespart.DropDownBranchResponse;

public class BranchAdapter extends RecyclerView.Adapter<BranchAdapter.MyViewHolder> {
    Context context;
    List<DropDownBranchResponse.DataBean> branch;
    Dialog branchDialog;
    CustomItemClickListener listener;
    private int mSelectedItem = -1;
    public BranchAdapter(Context context, List<DropDownBranchResponse.DataBean> branch, Dialog branchDialog, CustomItemClickListener listener) {
    this.context=context;
    this.branch=branch;
    this.branchDialog=branchDialog;
    this.listener=listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.option,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == mSelectedItem);
        holder.tvBranch.setText(branch.get(position).getBranches_name());
        TextView ok= branchDialog.findViewById(R.id.ok);
        TextView dialogheader=branchDialog.findViewById(R.id.dialogheader);
        dialogheader.setText("Branch");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedItem==-1)
                { Toast.makeText(context, "Select Branch", Toast.LENGTH_SHORT).show(); }
                else
                { branchDialog.dismiss(); }
            }
        });
    }

    @Override
    public int getItemCount() {
        return branch.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvBranch;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvBranch=itemView.findViewById(R.id.item);
            radioselect=itemView.findViewById(R.id.selected);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    listener.onItemClick(v, Integer.parseInt(branch.get(mSelectedItem).getId()),branch.get(mSelectedItem).getBranches_name());
                }

            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}
