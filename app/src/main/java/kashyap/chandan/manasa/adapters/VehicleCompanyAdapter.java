package kashyap.chandan.manasa.adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.valuespart.DropDownVehicleCompanyResponse;

public class VehicleCompanyAdapter extends RecyclerView.Adapter<VehicleCompanyAdapter.MyViewHolder> {
    Context context;
    List<DropDownVehicleCompanyResponse.DataBean> vehicleCompany;
    Dialog vehicleCompanyDialog;
    CustomItemClickListener listener;
    private int mSelectedItem = -1;
    public VehicleCompanyAdapter(Context context, List<DropDownVehicleCompanyResponse.DataBean> vehicleCompany, Dialog vehicleCompanyDialog, CustomItemClickListener listener) {
   this.context=context;
   this.listener=listener;
   this.vehicleCompany=vehicleCompany;
   this.vehicleCompanyDialog=vehicleCompanyDialog;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.option,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.radioselect.setChecked(position == mSelectedItem);
        holder.tvVehicleCompany.setText(vehicleCompany.get(position).getVehiclecompany_name());
        TextView ok= vehicleCompanyDialog.findViewById(R.id.ok);
        TextView dialogheader=vehicleCompanyDialog.findViewById(R.id.dialogheader);
        dialogheader.setText("Vehicle Company");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedItem==-1)
                { Toast.makeText(context, "Select Vehicle Company", Toast.LENGTH_SHORT).show(); }
                else
                { vehicleCompanyDialog.dismiss(); }
            }
        });
    }

    @Override
    public int getItemCount() {
        return vehicleCompany.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvVehicleCompany;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvVehicleCompany=itemView.findViewById(R.id.item);
            radioselect=itemView.findViewById(R.id.selected);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    listener.onItemClick(v, Integer.parseInt(vehicleCompany.get(mSelectedItem).getId()),vehicleCompany.get(mSelectedItem).getVehiclecompany_name());
                }

            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}
