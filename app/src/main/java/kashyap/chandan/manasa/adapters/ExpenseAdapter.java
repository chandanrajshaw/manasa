package kashyap.chandan.manasa.adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.expenses.ExpenseManagement;
import kashyap.chandan.manasa.valuespart.DropDownExpenseTypeResponse;

public class ExpenseAdapter extends RecyclerView.Adapter<ExpenseAdapter.MyViewHolder> {
    Context context;
    List<DropDownExpenseTypeResponse.DataBean> expense;
    Dialog expenseDialog;
    CustomItemClickListener listener;
    private int mSelectedItem = -1;
    public ExpenseAdapter(Context context, List<DropDownExpenseTypeResponse.DataBean> expense, Dialog expenseDialog, CustomItemClickListener listener) {
   this.context=context;
   this.expenseDialog=expenseDialog;
   this.listener=listener;
   this.expense=expense;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.option,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == mSelectedItem);
        holder.tvExpense.setText(expense.get(position).getExpensetype_name());
        TextView ok= expenseDialog.findViewById(R.id.ok);
        TextView dialogheader=expenseDialog.findViewById(R.id.dialogheader);
        dialogheader.setText("Expense Type");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedItem==-1)
                { Toast.makeText(context, "Select Expense Type", Toast.LENGTH_SHORT).show(); }
                else
                { expenseDialog.dismiss(); }
            }
        });

    }

    @Override
    public int getItemCount() {
        return expense.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvExpense;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvExpense=itemView.findViewById(R.id.item);
            radioselect=itemView.findViewById(R.id.selected);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    listener.onItemClick(v, Integer.parseInt(expense.get(mSelectedItem).getId()),expense.get(mSelectedItem).getExpensetype_name());
                }

            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}
