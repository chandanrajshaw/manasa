package kashyap.chandan.manasa.adapters;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import kashyap.chandan.manasa.CustomItemClickListener;
import kashyap.chandan.manasa.R;
import kashyap.chandan.manasa.valuespart.DropDownVehicleTypeResponse;

public class VehicleTypeAdapter extends RecyclerView.Adapter<VehicleTypeAdapter.MyViewHolder> {
    Context context;
    List<DropDownVehicleTypeResponse.DataBean> vehicleType;
    CustomItemClickListener listener;
    Dialog vehicleTypeDialog;
    private int mSelectedItem = -1;
    public VehicleTypeAdapter(Context context, List<DropDownVehicleTypeResponse.DataBean> vehicleType, Dialog vehicleTypeDialog, CustomItemClickListener listener) {
   this.context=context;
   this.vehicleType=vehicleType;
   this.vehicleTypeDialog=vehicleTypeDialog;
   this.listener =listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.option,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.radioselect.setChecked(position == mSelectedItem);
        holder.tvVehicle.setText(vehicleType.get(position).getVehicletype_name());
        TextView ok= vehicleTypeDialog.findViewById(R.id.ok);
        TextView dialogheader=vehicleTypeDialog.findViewById(R.id.dialogheader);
        dialogheader.setText("Vehicle Type");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedItem==-1)
                { Toast.makeText(context, "Select Vehicle Type", Toast.LENGTH_SHORT).show(); }
                else
                { vehicleTypeDialog.dismiss(); }
            }
        });
    }

    @Override
    public int getItemCount() {
        return vehicleType.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvVehicle;
        RadioButton radioselect;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvVehicle=itemView.findViewById(R.id.item);
            radioselect=itemView.findViewById(R.id.selected);
            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();
                    listener.onItemClick(v, Integer.parseInt(vehicleType.get(mSelectedItem).getId()),vehicleType.get(mSelectedItem).getVehicletype_name());
                }

            };
            itemView.setOnClickListener(clickListener);
            radioselect.setOnClickListener(clickListener);
        }
    }
}
